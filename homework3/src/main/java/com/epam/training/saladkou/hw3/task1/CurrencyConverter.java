package com.epam.training.saladkou.hw3.task1;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Utility class to convert card balance to different currencies.
 */
public final class CurrencyConverter {

    private CurrencyConverter() {

    }

    /**
     * Constant for USD exchange rate.
     */
    public static final double USD_RATE = 2.36;
    /**
     * Constant for EUR exchange rate.
     */
    public static final double EUR_RATE = 2.63;
    /**
     * Constant for RUB exchange rate.
     */
    public static final double RUB_RATE = 0.0317;

    /**
     * Converts sum to other currency by dividing it by exchange rate.
     * Throws IllegalArgumentException if sum parameter is less than 0.
     * Throws IllegalArgumentException if rate parameter is less than 0.
     * Throws ArithmeticException if rate parameter is 0.
     *
     * @param sum  - Sum we need to convert.
     * @param rate - Exchange rate of the currency into which we need to convert.
     * @return Method returns sum in other currency.
     */
    public static double convert(double sum, double rate) {
        if (sum < 0) {
            throw new IllegalArgumentException("Sum can't be negative");
        } else if (rate == 0) {
            throw new ArithmeticException("Division by zero");
        } else if (rate < 0) {
            throw new IllegalArgumentException("Rate can't be negative");
        } else {
            return sum / rate;
        }
    }
}
