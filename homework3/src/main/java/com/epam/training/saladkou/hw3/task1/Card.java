package com.epam.training.saladkou.hw3.task1;

/**
 * Class to simulate a bank card.
 */
public class Card {
    /**
     * Card holder name.
     */
    private String ownerName;
    /**
     * Current balance of the card.
     */
    private double balance;

    /**
     * Constructor allows to create instance of this class.
     *
     * @param ownerName - Card owner name.
     * @param balance   - Balance of the card.
     */
    public Card(String ownerName, double balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    /**
     * Overloaded constructor.
     * Provides creating instance of the class.
     * Sets balance field as 0.
     *
     * @param ownerName - Card owner name.
     */
    public Card(String ownerName) {
        this(ownerName, 0);
    }

    /**
     * @return Method returns current balance of the card.
     */
    public double getBalance() {
        return balance;
    }

    /**
     * Method to increase balance by sum.
     * If sum parameter is less than 0 method throws exception.
     *
     * @param sum - Sum you need to put on card.
     * @return Returns true if operation completed successfully.
     */
    public boolean putMoneyOn(double sum) {
        if (sum >= 0) {
            balance += sum;
            return true;
        } else {
            throw new IllegalArgumentException("Sum can't be negative");
        }
    }

    /**
     * Method to reduce balance by sum.
     * Throws "NoCashException" when sum is bigger then balance.
     * Throws IllegalArgumentException  if sum parameter is less than 0.
     *
     * @param sum Sum you need to withdraw.
     * @return Returns true if operation completed successfully.
     */


    public boolean withdraw(double sum) {
        if (sum < 0) {
            throw new IllegalArgumentException("Sum can't be negative");
        } else if (balance < sum) {
            throw new NoCashException("Attention, insufficient funds!");
        } else {
            balance -= sum;
            return true;
        }
    }
}