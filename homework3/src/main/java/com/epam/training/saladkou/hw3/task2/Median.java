package com.epam.training.saladkou.hw3.task2;

import java.util.Arrays;

/**
 * Utility class to calculate median of array.
 * Consists 2  methods for integer and double array.
 */
public final class Median {

    private Median() {

    }

    /**
     * @param array - input integer array.
     * @return returns median of input integer array.
     */
    public static float median(int[] array) {
        int[] arrayCopy = array.clone();
        Arrays.sort(arrayCopy);
        if (array.length % 2 == 0) {
            int left = arrayCopy[array.length / 2];
            int right = arrayCopy[array.length / 2 - 1];
            return (float) (right + left) / 2;
        } else {
            return arrayCopy[array.length / 2];
        }
    }

    /**
     * @param array - input double array.
     * @return returns median of input double array.
     */
    public static double median(double[] array) {
        double[] arrayCopy = array.clone();
        Arrays.sort(arrayCopy);
        if (array.length % 2 == 0) {
            double right = arrayCopy[array.length / 2];
            double left = arrayCopy[array.length / 2 - 1];
            return (left + right) / 2;
        } else {
            return arrayCopy[array.length / 2];
        }
    }


}
