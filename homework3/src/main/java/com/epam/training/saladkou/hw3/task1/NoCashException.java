package com.epam.training.saladkou.hw3.task1;

/**
 * Class of exception that occurs when
 * user tries to withdraw more money than is on the card.
 */
public class NoCashException extends RuntimeException {

    public NoCashException(String message) {
        super(message);
    }
}
