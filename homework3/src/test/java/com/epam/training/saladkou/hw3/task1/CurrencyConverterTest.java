package com.epam.training.saladkou.hw3.task1;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.*;

public class CurrencyConverterTest {

    private final double DEFAULT_SUM = 500.0;
    private final double DEFAULT_RATE = 2.20;
    private final double NEGATIVE_SUM = -500.0;
    private final double NEGATIVE_RATE = -2.20;
    private final double ZERO_RATE = 0.0;

    @Test
    public void testConvert() {
        double divided = DEFAULT_SUM / DEFAULT_RATE;
        double converted = CurrencyConverter.convert(DEFAULT_SUM, DEFAULT_RATE);
        Assert.assertEquals(divided, converted, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertNegativeValue() throws IllegalArgumentException {
        CurrencyConverter.convert(NEGATIVE_SUM, DEFAULT_RATE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertNegativeRate() throws IllegalArgumentException {
        CurrencyConverter.convert(DEFAULT_SUM, NEGATIVE_RATE);
    }

    @Test(expected = ArithmeticException.class)
    public void testConvertNullRate() throws ArithmeticException {
        CurrencyConverter.convert(DEFAULT_SUM, ZERO_RATE);
    }

}