package com.epam.training.saladkou.hw3.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CardTest {

    private final double DEFAULT_BALANCE = 500.0;
    private final double DEFAULT_SUM = 200.0;
    private final double NEGATIVE_SUM = -200.0;
    private Card card;

    @Before
    public void init() {
        card = new Card("Ivan Saladkou", DEFAULT_BALANCE);
    }


    @Test
    public void testPutMoneyOn() {
        double balanceBefore = card.getBalance();
        double total = balanceBefore + DEFAULT_SUM;
        card.putMoneyOn(DEFAULT_SUM);
        double balanceAfter = card.getBalance();
        Assert.assertEquals(total, balanceAfter, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPutMoneyOnNegativeSum() throws IllegalArgumentException {
        card.putMoneyOn(NEGATIVE_SUM);
    }

    @Test
    public void testWithdraw() {
        double balanceBefore = card.getBalance();
        double residue = balanceBefore - DEFAULT_SUM;
        card.withdraw(DEFAULT_SUM);
        double balanceAfter = card.getBalance();
        Assert.assertEquals(residue, balanceAfter, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawNegativeSum() throws IllegalArgumentException {
        card.withdraw(NEGATIVE_SUM);
    }

    @Test(expected = NoCashException.class)
    public void testWithdrawSumBiggerThanBalance() throws NoCashException {
        double bigger = card.getBalance() + 10.0;
        card.withdraw(bigger);
    }
}