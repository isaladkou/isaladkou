<%@page import="java.util.Map,
                com.epam.training.saladkou.hw17.Product"
%>
<html>
<head>
    <title>Order page</title>
</head>
<body>
<p>Dear,&nbsp<%= session.getAttribute("userName") %>
    your order is</p>

<%
    Map<Product, Integer> order = (Map<Product, Integer>) session.getAttribute("orderMap");
    int index = 1;
    if (order != null) {
        for (Product product : order.keySet()) {
            int amount = order.get(product);
            out.println(index + ") " + product.getTitle() + " x" + amount + " " + amount * product.getCost() + "$<br>");
            index++;
        }
    }
%>
<p>Total:&nbsp<%=session.getAttribute("totalSum")%>$</p>
</body>
</html>