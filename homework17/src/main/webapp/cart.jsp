<%@page import="java.util.Map,
                com.epam.training.saladkou.hw17.Product,
                com.epam.training.saladkou.hw17.ProductsCatalog"
%>
<%@ page import="java.util.List" %>

<html>
<head>
    <title>Cart page</title>
</head>
<body>
<p>Hello&nbsp<%= session.getAttribute("userName") %>
    !</p>
<p>You have already chosen:</p>
<%
    Map<Product, Integer> order = (Map<Product, Integer>) session.getAttribute("orderMap");
    int index = 1;
    if (order != null) {
        for (Product product : order.keySet()) {
            int amount = order.get(product);
            out.println(index + ") " + product.getTitle() + " x" + amount + " " + amount * product.getCost() + "$<br>");
            index++;
        }
    }
%>

<form name="orderForm" method="post" action="cart.jsp">
    <p>Make your order <br></p>
    <select size="1" name="orderedItem">
        <%
            List<Product> catalog = ProductsCatalog.getInstance().getCatalog();
            for (Product product : catalog) {
                out.println("<option value =" + product.getId() + ">" + product.toString() + "</option>");
            }
        %>
    </select><br><br>
    <input name="act" type="submit" value="Add Item">
    <input name="act" type="submit" value="Submit">
</form>
</body>
</html>