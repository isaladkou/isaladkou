package com.epam.training.saladkou.hw17;

import org.apache.log4j.Logger;

import java.io.IOException;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class AccessFilter implements Filter {

    private final Logger logger = Logger.getLogger(AccessFilter.class);
    private final String ACCESS_PARAM = "termsCheckbox";
    private final String USERNAME_PARAM = "name";
    private final String ACCESS_ATTRIBUTE = "isAccessAllowed";
    private final String USERNAME_ATTRIBUTE = "userName";
    private final String ERROR_PAGE_URL = "error.jsp";
    private final String REFERER_HEADER = "referer";
    private final String AUTH_PAGE_URL = "http://localhost:8888/online-shop/auth.jsp";

    private void handleRequestFromAuthPage(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (!session.isNew()) {
            session.invalidate();
            session = request.getSession(true);
        }
        String accessParam = request.getParameter(ACCESS_PARAM);
        String nameParam = request.getParameter(USERNAME_PARAM);

        boolean isAccessAllowed = (accessParam != null);
        session.setAttribute(ACCESS_ATTRIBUTE, isAccessAllowed);
        session.setAttribute(USERNAME_ATTRIBUTE, nameParam);
    }


    private void sendRedirectToErrorPage(HttpServletResponse response) {
        try {
            response.sendRedirect(ERROR_PAGE_URL);
        } catch (IOException exception) {
            logger.warn(exception.getMessage());
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String referer = request.getHeader(REFERER_HEADER);
        logger.info(REFERER_HEADER + ": " + referer);
        if (referer != null && referer.equals(AUTH_PAGE_URL)) {
            handleRequestFromAuthPage(request);
        }
        Boolean isAccessAllowed = (Boolean) request.getSession().getAttribute(ACCESS_ATTRIBUTE);
        logger.info(ACCESS_ATTRIBUTE + ": " + isAccessAllowed);
        if (isAccessAllowed == null || !isAccessAllowed) {
            sendRedirectToErrorPage(response);
            return;
        }
        RequestResender resender = new RequestResender();
        resender.putToFilterChain(request, response, filterChain);
    }

    @Override
    public void destroy() {

    }
}
