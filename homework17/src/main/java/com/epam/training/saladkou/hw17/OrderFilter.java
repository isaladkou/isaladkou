package com.epam.training.saladkou.hw17;


import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



public class OrderFilter implements Filter {

    private final String CART_PAGE_URL = "http://localhost:8888/online-shop/cart.jsp";
    private final String ORDER_ATTRIBUTE = "orderMap";
    private final String ACTION_PARAMETER = "act";
    private final String ORDERED_ITEM_PARAMETER = "orderedItem";
    private final String ADD_ITEM_ACTION = "Add Item";
    private final String REFERER_HEADER = "referer";
    private final String ORDER_PAGE_URL = "order";

    private final Logger logger = Logger.getLogger(OrderFilter.class);

    private final ProductsCatalog catalog = ProductsCatalog.getInstance();

    private boolean checkIsAddButtonClicked(HttpServletRequest request) {
        return request.getParameter(ACTION_PARAMETER).equals(ADD_ITEM_ACTION);
    }

    private Map<Product, Integer> getOrderMap(HttpSession session) {
        if (session.getAttribute(ORDER_ATTRIBUTE) == null) {
            session.setAttribute(ORDER_ATTRIBUTE, new HashMap<Product, Integer>());
        }
        return (Map<Product, Integer>) session.getAttribute(ORDER_ATTRIBUTE);
    }

    private boolean putItemToOrder(Map<Product, Integer> order, Product item) {
        Product product = getProductFromOrderById(order, item.getId());
        if (product == null) {
            order.put(item, 1);
        } else {
            int amount = order.get(product);
            order.put(product, ++amount);
        }
        return product == null;
    }

    private Product getProductFromOrderById(Map<Product, Integer> order, Integer id) {
        for (Product product : order.keySet()) {
            if (product.getId() == id) {
                return product;
            }
        }
        return null;
    }

    private boolean refreshOrder(HttpServletRequest request) {
        HttpSession session = request.getSession();
        int itemId = Integer.parseInt(request.getParameter(ORDERED_ITEM_PARAMETER));
        Product item = catalog.getProductById(itemId);
        Map<Product, Integer> order = getOrderMap(session);
        return putItemToOrder(order, item);
    }


    private void handleRequest(HttpServletRequest request,
                               ServletResponse servletResponse, FilterChain filterChain) {

        RequestResender resender = new RequestResender();
        if (checkIsAddButtonClicked(request)) {
            if (request.getParameter(ORDERED_ITEM_PARAMETER) != null) {
                refreshOrder(request);
            }
            resender.putToFilterChain(request, servletResponse, filterChain);

        } else {
            resender.forwardRequest(ORDER_PAGE_URL, request, servletResponse);
        }
    }


    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        if (request.getHeader(REFERER_HEADER).equals(CART_PAGE_URL)) {
            logger.info(REFERER_HEADER + " :" + request.getHeader(REFERER_HEADER));
            handleRequest(request, servletResponse, filterChain);
            return;
        }
        RequestResender resender = new RequestResender();
        resender.putToFilterChain(request, servletResponse, filterChain);
    }


    @Override
    public void destroy() {

    }
}
