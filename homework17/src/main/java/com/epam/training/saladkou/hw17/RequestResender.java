package com.epam.training.saladkou.hw17;

import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


public class RequestResender {

    private final Logger logger = Logger.getLogger(RequestResender.class);

    public void forwardRequest(String url, ServletRequest request,
                               ServletResponse response) {
        RequestDispatcher dispatcher = request.getRequestDispatcher(url);
        try {
            dispatcher.forward(request, response);
        } catch (ServletException | IOException exception) {
            logger.warn(exception.getMessage());
        }
    }

    public void putToFilterChain(ServletRequest request, ServletResponse response,
                                 FilterChain filterChain) {
        try {
            filterChain.doFilter(request, response);
        } catch (IOException | ServletException exception) {
            logger.warn(exception.getMessage());
        }
    }
}
