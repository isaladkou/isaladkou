package com.epam.training.saladkou.hw17;

import org.apache.log4j.Logger;

import java.util.Map;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



@WebServlet(name = "Order", urlPatterns = "/order")
public class OrderServlet extends HttpServlet {

    private final Logger logger = Logger.getLogger(OrderServlet.class);

    private final String ORDER_PAGE_URL = "order.jsp";
    private final String ORDER_ATTRIBUTE = "orderMap";
    private final String TOTAL_SUM_ATTRIBUTE = "totalSum";


    private double getTotalSum(Map<Product, Integer> products) {
        double sum = 0;
        for (Product product : products.keySet()) {
            int amount = products.get(product);
            sum += product.getCost() * amount;
        }
        return sum;
    }


    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        Map<Product, Integer> order = (Map<Product, Integer>) session.getAttribute(ORDER_ATTRIBUTE);
        session.setAttribute(TOTAL_SUM_ATTRIBUTE, getTotalSum(order));
        logger.info(ORDER_ATTRIBUTE + " :" + order);

        RequestResender resender = new RequestResender();
        resender.forwardRequest(ORDER_PAGE_URL, request, response);
    }
}
