package com.epam.training.saladkou.hw11;

import org.junit.Test;

public class FileTest {

    private final String WRONG_FILE_NAME = " a";

    @Test(expected = IllegalArgumentException.class)
    public void testCreateFileNullName() throws IllegalArgumentException {
        new File(null);
    }

    @Test(expected = WrongFileNameException.class)
    public void testCreateFileWrongName() throws WrongFileNameException {
        new File(WRONG_FILE_NAME);
    }
}