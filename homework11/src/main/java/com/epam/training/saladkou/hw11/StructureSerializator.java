package com.epam.training.saladkou.hw11;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class StructureSerializator {


    public void serialize(Folder folder, String pathToFile) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(pathToFile);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(folder);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public Folder deserialize(String pathToFile) {
        Folder folder = new Folder("Empty Folder");
        try (FileInputStream fileInputStream = new FileInputStream(pathToFile);
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            return (Folder) objectInputStream.readObject();
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        throw new RuntimeException("Failed to load structure");
    }
}
