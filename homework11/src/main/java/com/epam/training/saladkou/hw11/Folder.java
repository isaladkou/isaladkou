package com.epam.training.saladkou.hw11;

import java.io.Serializable;
import java.util.ArrayList;

public class Folder implements Serializable {

    private static final String FOLDER_NAME_PATTERN = "[a-zA-Z0-9\\s_]+";

    private String name;

    public String getName() {
        return name;
    }

    private ArrayList<Folder> folders;
    private ArrayList<File> files;

    public Folder(String name) {
        if (checkFolderNameValid(name)) {
            this.name = name;
            files = new ArrayList<>();
            folders = new ArrayList<>();
        } else {
            throw new WrongFolderNameException("Folder name must match "
                    + "pattern \"[a-zA-Z0-9\\s_]+\"");
        }
    }

    public ArrayList<File> getFiles() {
        return files;
    }

    public ArrayList<Folder> getFolders() {
        return folders;
    }

    public boolean addFile(File file) {
        checkIsFileValid(file);
        files.add(file);
        return true;
    }

    public boolean addFolder(Folder folder) {
        if (folder == null) {
            throw new IllegalArgumentException("Folder can't be null");
        }
        folders.add(folder);
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean containsFiles() {
        return !files.isEmpty();
    }

    public boolean containsFolders() {
        return !folders.isEmpty();
    }

    private boolean containsFolder(String folderName) {
        for (Folder folder : folders) {
            if (folder.getName().equals(folderName)) {
                return true;
            }
        }
        return false;
    }

    private boolean containsFile(String fileName) {
        for (File file : files) {
            if (file.getFullName().equals(fileName)) {
                return true;
            }
        }
        return false;
    }

    public Folder getFolderByName(String folderName) {
        if (containsFolder(folderName)) {
            for (Folder folder : folders) {
                if (folder.getName().equals(folderName)) {
                    return folder;
                }
            }
        }
        throw new FolderNotFoundException("Folder was not found");
    }

    private boolean checkFolderNameValid(String folderName) {
        if (folderName == null) {
            throw new IllegalArgumentException("Folder name can't be null");
        }
        return folderName.matches(FOLDER_NAME_PATTERN);
    }

    private boolean checkIsFileValid(File file) {
        if (file == null) {
            throw new IllegalArgumentException("File can't be null");
        } else if (this.containsFile(file.getFullName())) {
            throw new FileAlreadyExistException("File with such name already exists");
        }
        return true;
    }


}


