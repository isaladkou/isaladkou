package com.epam.training.saladkou.hw11;

public class WrongFileNameException extends RuntimeException {
    public WrongFileNameException(String message) {
        super(message);
    }
}
