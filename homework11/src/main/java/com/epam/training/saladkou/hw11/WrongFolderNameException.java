package com.epam.training.saladkou.hw11;

public class WrongFolderNameException extends RuntimeException {
    public WrongFolderNameException(String message) {
        super(message);
    }
}
