package com.epam.training.saladkou.hw11;

import java.io.File;
import java.util.Scanner;


public class Program {

    private static final String INSTRUCTIONS = "To select action, please type\n"
            + "root/path - to add new structure\n"
            + "print - to print structure\n"
            + "save - to save structure to file\n"
            + "exit - to exit";

    private static final String LOAD_STRUCTURE_MESSAGE =
            "Do you want to load structure (y/n)?\n";


    private static final String DEFAULT_PATH = "serializedStructure.ser";

    private static final String EXIT = "exit";
    private static final String ROOT = "root";
    private static final String PRINT = "print";
    private static final String SAVE = "save";
    private static final String YES = "Y";

    public static void main(String[] args) {

        PathParser parser = new PathParser();
        StructureSerializator serializator = new StructureSerializator();
        Scanner scanner = new Scanner(System.in);
        File file = new File(DEFAULT_PATH);
        Folder root = new Folder("root");
        String inputLine = "";
        if (file.exists()) {
            System.out.println(LOAD_STRUCTURE_MESSAGE);
            inputLine = scanner.nextLine();
            if (inputLine.equalsIgnoreCase(YES)) {
                try {
                    root = serializator.deserialize(DEFAULT_PATH);
                } catch (Exception exception) {
                    System.out.println(exception.getMessage());
                }
            }
        }
        Printer printer = new Printer();
        StructureCreator creator = new StructureCreator();
        System.out.println(INSTRUCTIONS);
        while (!inputLine.equalsIgnoreCase(EXIT)) {
            scanner = new Scanner(System.in);
            inputLine = scanner.nextLine();
            if (inputLine.equalsIgnoreCase(PRINT)) {
                printer.print(root);
            } else if (inputLine.startsWith(ROOT)) {
                buildStructure(parser, creator, inputLine, root);
            } else if (inputLine.startsWith(SAVE)) {
                serializator.serialize(root, DEFAULT_PATH);
            } else if (inputLine.equalsIgnoreCase(EXIT)) {
                break;
            } else {
                System.out.println("Please, check your input\n" + INSTRUCTIONS);
            }
        }
    }

    private static void buildStructure(PathParser parser, StructureCreator creator,
                                       String path, Folder root) {
        parser.setPath(path);
        try {
            creator.createStructure(root, parser.getFoldersNames(), parser.getFileName());
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }


}
