package com.epam.training.saladkou.hw10.exceptions;

public class CrowdedAtmException extends RuntimeException {

    public CrowdedAtmException(String message) {
        super(message);
    }
}
