package com.epam.training.saladkou.hw10.runnables;

import com.epam.training.saladkou.hw10.CashMachine;
import com.epam.training.saladkou.hw10.cards.AbstractCard;


import java.math.BigDecimal;

public class ReplenishmentRunnable extends AbstractAtmRunnable {

    private final int SLEEP_TIME = 2000;
    private final BigDecimal REPLENISHMENT_SUM = new BigDecimal(10);

    public ReplenishmentRunnable(CashMachine atm, AbstractCard card, String atmName) {
        super(atm, card, atmName);
    }

    @Override
    protected void printChangeBalanceMessage() {
        System.out.println("Balance was replenished by " + getAtmName()
                + ", balance = " + getCard().getBalance() + "$");
    }


    @Override
    public void run() {
        while (checkIsBalanceBetweenLimits()) {
            getAtm().replenishAccount(getCard(), REPLENISHMENT_SUM);
            printChangeBalanceMessage();
            putThreadToSleep(SLEEP_TIME);
        }
        printFinishMessage();

    }
}
