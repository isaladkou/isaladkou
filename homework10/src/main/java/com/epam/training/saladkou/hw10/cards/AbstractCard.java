package com.epam.training.saladkou.hw10.cards;

import java.math.BigDecimal;
import java.math.RoundingMode;

public abstract class AbstractCard {

    public final BigDecimal USD_RATE = new BigDecimal("2.59");
    public final BigDecimal EUR_RATE = new BigDecimal("2.8287");
    public final BigDecimal RUB_RATE = new BigDecimal("0.329");

    private String ownerName;
    private BigDecimal balance;

    public BigDecimal getBalance() {
        return balance;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public AbstractCard(String ownerName, BigDecimal balance) {
        checkCardParametersIsValid(ownerName, balance);
        this.ownerName = ownerName;
        this.balance = balance;
    }

    public AbstractCard() {
        this("Owner Name", BigDecimal.ZERO);
    }


    public synchronized boolean putMoneyOn(BigDecimal sum) {
        checkSumIsValid(sum);
        this.balance = balance.add(sum);
        return true;

    }

    public BigDecimal convert(BigDecimal sum, BigDecimal rate) {
        checkSumIsValid(sum);
        checkConversionRateIsValid(rate);
        return sum.divide(rate, 2, RoundingMode.HALF_UP);
    }

    public abstract boolean withdraw(BigDecimal sum);

    protected boolean checkCardParametersIsValid(String ownerName, BigDecimal balance) {
        if (ownerName == null) {
            throw new IllegalArgumentException("Owner name can't be null");
        } else if (balance == null) {
            throw new IllegalArgumentException("Balance can't be null");
        }
        return true;
    }

    protected boolean checkSumIsValid(BigDecimal sum) {
        if (sum == null) {
            throw new IllegalArgumentException("Sum can't be null");
        } else if (sum.signum() < 0) {
            throw new IllegalArgumentException("Sum can't be negative");
        }
        return true;
    }

    private boolean checkConversionRateIsValid(BigDecimal rate) {
        if (rate == null) {
            throw new IllegalArgumentException("Rate can't be null");
        } else if (rate.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("Rate can't non positive");
        }
        return true;
    }

}