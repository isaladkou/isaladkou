package com.epam.training.saladkou.hw10.runnables;

import com.epam.training.saladkou.hw10.CashMachine;
import com.epam.training.saladkou.hw10.cards.AbstractCard;


import java.math.BigDecimal;

public abstract class AbstractAtmRunnable implements Runnable {

    private final BigDecimal WITHDRAWAL_LIMIT = new BigDecimal(0);
    private final BigDecimal REPLENISHMENT_LIMIT = new BigDecimal(1000);

    private CashMachine atm;
    private AbstractCard card;
    private String atmName;

    public CashMachine getAtm() {
        return atm;
    }

    public AbstractCard getCard() {
        return card;
    }

    public String getAtmName() {
        return atmName;
    }

    public AbstractAtmRunnable(CashMachine atm, AbstractCard card, String atmName) {
        this.atm = atm;
        this.card = card;
        this.atmName = atmName;
    }

    protected boolean checkIsBalanceBetweenLimits() {
        return this.card.getBalance().compareTo(WITHDRAWAL_LIMIT) > 0
                && this.card.getBalance().compareTo(REPLENISHMENT_LIMIT) < 0;
    }

    protected abstract void printChangeBalanceMessage();

    protected void printFinishMessage() {
        System.out.println("Balance = " + card.getBalance()
                + "! " + atmName + " finished it's work");
    }

    protected void putThreadToSleep(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void run() {

    }
}
