package com.epam.training.saladkou.hw10;

import com.epam.training.saladkou.hw10.cards.AbstractCard;
import com.epam.training.saladkou.hw10.cards.CreditCard;
import com.epam.training.saladkou.hw10.runnables.AbstractAtmRunnable;
import com.epam.training.saladkou.hw10.runnables.ReplenishmentRunnable;
import com.epam.training.saladkou.hw10.runnables.WithdrawalRunnable;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Program {
    private static final String OWNER_NAME = "Ivan Saladkou";
    private static final int PRODUCERS_AMOUNT = 5;
    private static final int CONSUMER_AMOUNT = 3;
    private static final BigDecimal BALANCE = BigDecimal.valueOf(500);


    public static void main(String[] args) {
        AbstractCard card = new CreditCard(OWNER_NAME, BALANCE);
        ArrayList<CashMachine> producers = createCashMachines(PRODUCERS_AMOUNT);
        ArrayList<CashMachine> consumers = createCashMachines(CONSUMER_AMOUNT);
        ArrayList<ReplenishmentRunnable> replensihmentRunnables =
                createReplenishmentRunnables(producers, card);
        ArrayList<WithdrawalRunnable> withdrawalRunnables =
                createWithdrawalRunnables(consumers, card);
        ArrayList<Thread> replenishmentThreads = initializeThreads(replensihmentRunnables);
        ArrayList<Thread> withdrawalThreads = initializeThreads(withdrawalRunnables);
        ArrayList<Thread> threads = new ArrayList<>(replenishmentThreads);
        threads.addAll(withdrawalThreads);
        startThreads(threads);
    }

    private static ArrayList<CashMachine> createCashMachines(int amount) {
        ArrayList<CashMachine> cashMachines = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            cashMachines.add(new CashMachine());
        }
        return cashMachines;
    }

    private static ArrayList<ReplenishmentRunnable> createReplenishmentRunnables(
            ArrayList<CashMachine> atmList, AbstractCard card) {
        ArrayList<ReplenishmentRunnable> runnables = new ArrayList<>();
        int counter = 0;
        for (CashMachine cashMachine : atmList) {
            runnables.add(new ReplenishmentRunnable(cashMachine, card,
                    "moneyProducer" + (++counter)));
        }
        return runnables;
    }

    private static ArrayList<WithdrawalRunnable> createWithdrawalRunnables(
            ArrayList<CashMachine> atmList, AbstractCard card) {
        ArrayList<WithdrawalRunnable> runnables = new ArrayList<>();
        int counter = 0;
        for (CashMachine cashMachine : atmList) {
            runnables.add(new WithdrawalRunnable(cashMachine, card,
                    "moneyConsumer" + (++counter)));
        }
        return runnables;
    }


    private static ArrayList<Thread> initializeThreads(
            ArrayList<? extends AbstractAtmRunnable> runnables) {
        ArrayList<Thread> threads = new ArrayList<>();
        for (Runnable runnable : runnables) {
            threads.add(new Thread(runnable));
        }
        return threads;
    }

    private static boolean startThreads(ArrayList<Thread> threads) {
        for (Thread thread : threads) {
            thread.start();
        }
        return true;
    }


}
