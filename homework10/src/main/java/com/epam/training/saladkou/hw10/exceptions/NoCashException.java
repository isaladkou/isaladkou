package com.epam.training.saladkou.hw10.exceptions;

public class NoCashException extends RuntimeException {

    public NoCashException(String message) {
        super(message);
    }
}
