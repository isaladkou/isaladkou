package com.epam.training.saladkou.hw10.runnables;

import com.epam.training.saladkou.hw10.CashMachine;
import com.epam.training.saladkou.hw10.cards.AbstractCard;

import java.math.BigDecimal;

public class WithdrawalRunnable extends AbstractAtmRunnable {

    private final BigDecimal WITHDRAWAL_SUM = new BigDecimal(10);
    private final int SLEEP_TIME = 2000;

    public WithdrawalRunnable(CashMachine atm, AbstractCard card, String atmName) {
        super(atm, card, atmName);
    }

    @Override
    protected void printChangeBalanceMessage() {
        System.out.println("Money was withdrawn by " + getAtmName()
                + ", balance = " + getCard().getBalance() + "$");
    }


    @Override
    public void run() {
        while (checkIsBalanceBetweenLimits()) {
            getAtm().withdrawMoney(getCard(), WITHDRAWAL_SUM);
            printChangeBalanceMessage();
            putThreadToSleep(SLEEP_TIME);
        }
        printFinishMessage();
    }
}

