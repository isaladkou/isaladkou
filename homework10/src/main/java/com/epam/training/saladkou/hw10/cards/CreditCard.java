package com.epam.training.saladkou.hw10.cards;

import java.math.BigDecimal;

public class CreditCard extends AbstractCard {
    public CreditCard(String ownerName, BigDecimal balance) {
        super(ownerName, balance);
    }

    @Override
    public synchronized boolean withdraw(BigDecimal sum) {
        checkSumIsValid(sum);
        setBalance(getBalance().subtract(sum));
        return true;
    }
}
