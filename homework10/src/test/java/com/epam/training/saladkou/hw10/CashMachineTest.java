package com.epam.training.saladkou.hw10;

import com.epam.training.saladkou.hw10.cards.DebitCard;
import com.epam.training.saladkou.hw10.exceptions.CrowdedAtmException;
import com.epam.training.saladkou.hw10.exceptions.NoCashException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class CashMachineTest {
    private final String DEFAULT_OWNER_NAME = "Ivan Saladkou";
    private BigDecimal defaultBalance;
    private BigDecimal defaultCapacity;
    private BigDecimal negativeCapacity;
    private BigDecimal defaultMoneyAmount;
    private BigDecimal negativeMoneyAmount;
    private BigDecimal bigMoneyAmount;
    private BigDecimal defaultSum;
    private BigDecimal negativeSum;
    private DebitCard card;
    private CashMachine cashMachine;

    @Before
    public void initialize() {
        defaultBalance = new BigDecimal(100);
        defaultCapacity = new BigDecimal(2000);
        negativeCapacity = new BigDecimal(-2000);
        defaultMoneyAmount = new BigDecimal(1000);
        negativeMoneyAmount = new BigDecimal(-1000);
        bigMoneyAmount = new BigDecimal(5000);
        defaultSum = new BigDecimal(50);
        negativeSum = new BigDecimal(-50);
        cashMachine = new CashMachine(defaultCapacity, defaultMoneyAmount);
        card = new DebitCard(DEFAULT_OWNER_NAME, defaultBalance);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetNegativeCapacity() throws IllegalArgumentException {
        cashMachine = new CashMachine(negativeCapacity, defaultMoneyAmount);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetMoneyAmountMoreThanCapacity() throws IllegalArgumentException {
        cashMachine = new CashMachine(defaultCapacity, bigMoneyAmount);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetNegativeMoneyAmount() throws IllegalArgumentException {
        cashMachine = new CashMachine(defaultCapacity, negativeMoneyAmount);
    }

    @Test
    public void testReplenishAccount() {
        Assert.assertTrue(cashMachine.replenishAccount(card, defaultSum));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReplenishAccountNullCard() throws IllegalArgumentException {
        cashMachine.replenishAccount(null, defaultSum);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReplenishAccountNullSum() throws IllegalArgumentException {
        cashMachine.replenishAccount(card, null);
    }

    @Test(expected = CrowdedAtmException.class)
    public void testReplenishAccountSumMoreThanDifference() throws CrowdedAtmException {
        cashMachine.replenishAccount(card, bigMoneyAmount);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReplenishAccountNegativeSum() throws IllegalArgumentException {
        cashMachine.replenishAccount(card, negativeSum);
    }

    @Test
    public void testWithdrawMoney() {
        Assert.assertTrue(cashMachine.withdrawMoney(card, defaultSum));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawMoneyNullSum() throws IllegalArgumentException {
        cashMachine.withdrawMoney(card, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawMoneyNullCard() throws IllegalArgumentException {
        cashMachine.withdrawMoney(null, defaultSum);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawMoneyNegativeSum() throws IllegalArgumentException {
        cashMachine.withdrawMoney(card, negativeSum);
    }

    @Test(expected = NoCashException.class)
    public void testWithdrawMoneySumMoreThanMoneyAmount() throws NoCashException {
        cashMachine.withdrawMoney(card, bigMoneyAmount);
    }
}