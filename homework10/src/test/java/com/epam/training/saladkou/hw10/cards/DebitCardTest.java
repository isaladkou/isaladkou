package com.epam.training.saladkou.hw10.cards;

import com.epam.training.saladkou.hw10.exceptions.InsufficientFundsException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DebitCardTest {
    private final String DEFAULT_OWNER_NAME = "Ivan Saladkou";
    private BigDecimal bigSum;
    private BigDecimal defaultBalance;
    private BigDecimal negativeBalance;
    private BigDecimal defaultSum;
    private BigDecimal negativeSum;
    private DebitCard debitCard;
    private BigDecimal defaultRate;
    private BigDecimal negativeRate;
    private BigDecimal conversionResult;

    @Before
    public void initialize() {
        bigSum = new BigDecimal(5000);
        defaultBalance = new BigDecimal(100);
        negativeBalance = new BigDecimal(-100);
        defaultSum = new BigDecimal(10);
        negativeSum = new BigDecimal(-10);
        defaultRate = new BigDecimal(2);
        negativeRate = new BigDecimal(-2);
        conversionResult = new BigDecimal("5.00");
        debitCard = new DebitCard(DEFAULT_OWNER_NAME, defaultBalance);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateDebitCardNullOwnerName() throws IllegalArgumentException {
        debitCard = new DebitCard(null, defaultBalance);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateDebitCardNullBalance() throws IllegalArgumentException {
        debitCard = new DebitCard(DEFAULT_OWNER_NAME, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateDebitCardNegativeBalance() throws IllegalArgumentException {
        debitCard = new DebitCard(DEFAULT_OWNER_NAME, negativeBalance);
    }

    @Test
    public void testPutMoneyOn() {
        assertTrue(debitCard.putMoneyOn(defaultSum));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPutMoneyOnNegativeSum() throws IllegalArgumentException {
        debitCard.putMoneyOn(negativeSum);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPutMoneyOnNullSum() throws IllegalArgumentException {
        debitCard.putMoneyOn(null);
    }

    @Test
    public void testWithdraw() {
        assertTrue(debitCard.withdraw(defaultSum));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawNegativeSum() throws IllegalArgumentException {
        debitCard.withdraw(negativeSum);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawNullSum() throws IllegalArgumentException {
        debitCard.withdraw(null);
    }

    @Test(expected = InsufficientFundsException.class)
    public void testWithdrawSumMoreThanBalance() throws InsufficientFundsException {
        debitCard.withdraw(bigSum);
    }

    @Test
    public void testConvert() {
        assertEquals(conversionResult, debitCard.convert(defaultSum, defaultRate));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertNullSum() throws IllegalArgumentException {
        debitCard.convert(null, defaultRate);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertNullRate() throws IllegalArgumentException {
        debitCard.convert(defaultSum, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertNegativeSum() throws IllegalArgumentException {
        debitCard.convert(negativeSum, defaultRate);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertNegativeRate() throws IllegalArgumentException {
        debitCard.convert(defaultSum, negativeRate);
    }

}