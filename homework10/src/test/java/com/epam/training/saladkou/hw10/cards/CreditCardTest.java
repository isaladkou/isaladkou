package com.epam.training.saladkou.hw10.cards;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class CreditCardTest {

    private CreditCard creditCard;
    private final String DEFAULT_OWNER_NAME = "Ivan Saladkou";
    private BigDecimal defaultBalance;
    private BigDecimal defaultSum;
    private BigDecimal negativeSum;

    @Before
    public void initialize() {
        defaultBalance = new BigDecimal(100);
        defaultSum = new BigDecimal(10);
        negativeSum = new BigDecimal(-10);
        creditCard = new CreditCard(DEFAULT_OWNER_NAME, defaultBalance);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCreditCardNullOwnerName() throws IllegalArgumentException {
        creditCard = new CreditCard(null, defaultBalance);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCreditCardNullBalance() throws IllegalArgumentException {
        creditCard = new CreditCard(DEFAULT_OWNER_NAME, null);
    }

    @Test
    public void testPutMoneyOn() {
        Assert.assertTrue(creditCard.putMoneyOn(defaultSum));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPutMoneyOnNegativeSum() throws IllegalArgumentException {
        creditCard.putMoneyOn(negativeSum);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPutMoneyOnNullSum() throws IllegalArgumentException {
        creditCard.putMoneyOn(null);
    }

    @Test
    public void testWithdraw() {
        Assert.assertTrue(creditCard.withdraw(defaultSum));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawNegativeSum() throws IllegalArgumentException {
        creditCard.withdraw(negativeSum);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawOnNullSum() throws IllegalArgumentException {
        creditCard.withdraw(null);
    }
}