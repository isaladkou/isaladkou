package com.epam.training.saladkou.hw15;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "Order", urlPatterns = "/order")
public class OrderServlet extends HttpServlet {

    private Logger logger = Logger.getLogger(OrderServlet.class);


    private List<Product> mapParametersToProducts(String[] parameters) {
        ProductsCatalog productsCatalog = ProductsCatalog.getInstance();
        List<Product> products = new ArrayList<>();
        for (int i = 0; i < parameters.length; i++) {
            Product product = productsCatalog.getProductById(Integer.parseInt(parameters[i]));
            products.add(product);
        }
        return products;
    }

    private String getOrderTable(List<Product> products) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<table>");
        int index = 1;
        for (Product product : products) {
            stringBuilder.append("<tr><td>").append(index).append(") </td>");
            stringBuilder.append("<td>").append(product.getTitle()).append("</td>");
            stringBuilder.append("<td>").append(product.getCost()).append(" $</tr></td>");
            index++;
        }
        stringBuilder.append("</table>");
        return stringBuilder.toString();
    }

    private double getTotalSum(List<Product> products) {
        double sum = 0;
        for (Product product : products) {
            sum += product.getCost();
        }
        return sum;
    }

    private void printMarkup(HttpServletResponse response, String name, String table, Double sum) {
        try (PrintWriter writer = response.getWriter()) {
            writer.println("<html>"
                    + "<head>"
                    + "<title>Order page</title>"
                    + "</head>"
                    + "<body>"
                    + "<p>Dear " + name + ", your order:</p>"
                    + table
                    + "<p>Total: $" + sum + " </p>"
                    + "</body>"
                    + "</html>");
        } catch (IOException exception) {
            logger.warn(exception.getMessage());
        }
    }

    private void printEmptyOrderMarkup(HttpServletResponse response, String name) {
        try (PrintWriter writer = response.getWriter()) {
            writer.println("<html>"
                    + "<head>"
                    + "<title>Order page</title>"
                    + "</head>"
                    + "<body>"
                    + "<p>Dear " + name + ", your order is empty</p>"
                    + "</body>"
                    + "</html>");
        } catch (IOException exception) {
            logger.warn(exception.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("name");
        String[] items = request.getParameterValues("orderedItems");
        if (items != null) {
            List<Product> products = mapParametersToProducts(items);
            String table = getOrderTable(products);
            Double sum = getTotalSum(products);
            printMarkup(response, name, table, sum);
        } else {
            printEmptyOrderMarkup(response, name);
        }
    }
}
