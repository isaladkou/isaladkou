package com.epam.training.saladkou.hw15;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "Cart", urlPatterns = "/cart")
public class CartServlet extends HttpServlet {

    private final String DEFAULT_USER_NAME = "User";

    private Logger logger = Logger.getLogger(CartServlet.class);

    private String getOptions() {
        StringBuilder options = new StringBuilder();
        List<Product> catalog = ProductsCatalog.getInstance().getCatalog();
        for (Product product : catalog) {
            options.append("<option value =").append(product.getId()).append(">")
                    .append(product).append("</option>");
        }
        return options.toString();
    }

    private String getUserName(HttpServletRequest request) {
        String name = request.getParameter("name");
        if (name.equals("")) {
            name = DEFAULT_USER_NAME;
        }
        return name;
    }

    private void printMarkup(HttpServletResponse response, String name) {
        try (PrintWriter writer = response.getWriter()) {
            writer.println("<html>"
                    + "<head>"
                    + "<title>Cart page</title>"
                    + "</head>"
                    + "<body>"
                    + "<p>Hello " + name + "!</p>"
                    + "<form name=\"orderForm\" method=\"post\" action=\"order\"  >"
                    + "<p>Make your order <br></p>"
                    + "<select multiple size =\"3\" name =\"orderedItems\">"
                    + getOptions()
                    + "</select><br>"
                    + "<input type=\"hidden\"  name = \"name\" value =\"" + name + "\">"
                    + "<input type=\"submit\" value=\"Submit\">"
                    + "</form>"
                    + "</body>"
                    + "</html>");
        } catch (IOException exception) {
            logger.warn(exception.getMessage());
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String name = getUserName(request);
        printMarkup(response, name);
    }
}