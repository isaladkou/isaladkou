package com.epam.training.saladkou.hw12;

import java.io.File;
import java.util.Scanner;


public class Program {

    private static final String INSTRUCTIONS = "To select action, please type\n"
            + "root/path - to add new structure\n"
            + "print - to print structure\n"
            + "save - to save structure to file\n"
            + "exit - to exit";

    private static final String LOAD_STRUCTURE_MESSAGE =
            "Serialized structure file was detected\n"
                    + "Do you want to load structure (y/n)?\n";

    private static final String STRUCTURE_LOADING_SUCCESS_MESSAGE =
            "\nStructure successfully loaded\n";

    private static final String STRUCTURE_LOADING_FAILURE_MESSAGE =
            "\nFailed to load structure\n";

    private static final String DEFAULT_STRUCTURE_MESSAGE =
            "\nDefault structure created\n";

    private static final String CHECK_INPUT_MESSAGE =
            "\nPlease, check your input\n";

    private static final String STRUCTURE_CREATION_SUCCESS_MESSAGE =
            "\nStructure successfully created\n";

    private static final String STRUCTURE_CREATION_FAILURE_MESSAGE =
            "\nFailed to create structure\n";

    private static final String STRUCTURE_SAVED_MESSAGE =
            "\nStructure successfully saved\n";


    private static final String DEFAULT_PATH = "serializedStructure.ser";
    private static final String ROOT_FOLDER_NAME = "root";

    private static final String EXIT = "exit";
    private static final String ROOT = "root";
    private static final String PRINT = "print";
    private static final String SAVE = "save";
    private static final String YES = "Y";
    private static final String NO = "N";

    public static void main(String[] args) {
        PathParser parser = new PathParser();
        Scanner scanner;
        String inputLine = "";
        File file = new File(DEFAULT_PATH);
        Folder root = new Folder(ROOT_FOLDER_NAME);
        if (file.exists()) {
            System.out.println(LOAD_STRUCTURE_MESSAGE);
            root = loadStructure();
        }
        Printer printer = new Printer();
        StructureCreator creator = new StructureCreator();
        StructureSerializator serializator = new StructureSerializator();
        System.out.println(INSTRUCTIONS);
        while (!inputLine.equalsIgnoreCase(EXIT)) {
            scanner = new Scanner(System.in);
            inputLine = scanner.nextLine();
            if (inputLine.equalsIgnoreCase(PRINT)) {
                printer.print(root);
            } else if (inputLine.startsWith(ROOT)) {
                buildStructure(parser, creator, inputLine, root);
            } else if (inputLine.startsWith(SAVE)) {
                serializator.serialize(root, DEFAULT_PATH);
                System.out.println(STRUCTURE_SAVED_MESSAGE);
            } else if (inputLine.equalsIgnoreCase(EXIT)) {
                break;
            } else {
                System.out.println(CHECK_INPUT_MESSAGE + INSTRUCTIONS);
            }
        }
    }

    private static void buildStructure(PathParser parser, StructureCreator creator,
                                       String path, Folder root) {
        parser.setPath(path);
        try {
            creator.createStructure(root, parser.getFoldersNames(), parser.getFileName());
            System.out.println(STRUCTURE_CREATION_SUCCESS_MESSAGE);
        } catch (RuntimeException exception) {
            System.out.println(STRUCTURE_CREATION_FAILURE_MESSAGE
                    + exception.getMessage());
        }
    }

    private static Folder loadStructure() {
        Folder root = new Folder(ROOT_FOLDER_NAME);
        String inputLine = "";
        while (!inputLine.equalsIgnoreCase(YES) && !inputLine.equalsIgnoreCase(NO)) {
            Scanner scanner = new Scanner(System.in);
            inputLine = scanner.nextLine();
            if (inputLine.equalsIgnoreCase(YES)) {
                root = loadFromFile();
            } else if (inputLine.equalsIgnoreCase(NO)) {
                System.out.println(DEFAULT_STRUCTURE_MESSAGE);
            } else {
                System.out.println(CHECK_INPUT_MESSAGE);
            }
        }
        return root;
    }

    private static Folder loadFromFile() {
        Folder root = new Folder(ROOT_FOLDER_NAME);
        StructureSerializator serializator = new StructureSerializator();
        try {
            root = serializator.deserialize(DEFAULT_PATH);
            System.out.println(STRUCTURE_LOADING_SUCCESS_MESSAGE);
        } catch (Exception exception) {
            System.out.println(STRUCTURE_LOADING_FAILURE_MESSAGE
                    + DEFAULT_STRUCTURE_MESSAGE
                    + exception.getMessage());
        }
        return root;
    }

}
