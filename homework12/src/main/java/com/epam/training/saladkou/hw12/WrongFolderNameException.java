package com.epam.training.saladkou.hw12;

public class WrongFolderNameException extends RuntimeException {
    public WrongFolderNameException(String message) {
        super(message);
    }
}
