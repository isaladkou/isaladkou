package com.epam.training.saladkou.hw12;

import java.util.ArrayList;

public class StructureCreator {

    private void createFile(Folder parentFolder, String fullFileName) {
        if (!fullFileName.equals("")) {
            File file = new File(fullFileName);
            parentFolder.addFile(file);
        }
    }

    private boolean checkStructureParametersIsValid(Folder parentFolder,
                                                    ArrayList<String> foldersNames,
                                                    String fullFileName) {
        if (parentFolder != null
                && foldersNames != null
                && fullFileName != null) {
            return true;
        }
        throw new IllegalArgumentException("No one parameter could be null");
    }

    public void createStructure(Folder parentFolder, ArrayList<String> foldersNames,
                                String fullFileName) {
        checkStructureParametersIsValid(parentFolder, foldersNames, fullFileName);
        if (!foldersNames.isEmpty()) {
            String tempName = foldersNames.get(0);
            foldersNames.remove(0);
            if (parentFolder.containsFolder(tempName)) {
                Folder childFolder = parentFolder.getFolderByName(tempName);
                selectAction(childFolder, foldersNames, fullFileName);
            } else {
                parentFolder.addFolder(new Folder(tempName));
                selectAction(parentFolder.getFolderByName(tempName),
                        foldersNames, fullFileName);
            }
        } else {
            createFile(parentFolder, fullFileName);
        }
    }

    private void selectAction(Folder childFolder, ArrayList<String> foldersNames,
                              String fullFileName) {
        if (foldersNames.isEmpty()) {
            createFile(childFolder, fullFileName);
        } else {
            createStructure(childFolder, foldersNames, fullFileName);
        }
    }
}
