package com.epam.training.saladkou.hw12;

public class WrongFileNameException extends RuntimeException {
    public WrongFileNameException(String message) {
        super(message);
    }
}
