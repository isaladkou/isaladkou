package com.epam.training.saladkou.hw12;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class StructureCreatorTest {
    private StructureCreator creator = new StructureCreator();
    private Folder rootFolder = new Folder("root");
    private Folder inFolder = new Folder("inFolder");
    private ArrayList<String> folderNames = new ArrayList<>(
            Arrays.asList("folder1", "folder2"));
    private ArrayList<String> emptyFolderNames = new ArrayList<>();
    private ArrayList<String> existingFolderNames = new ArrayList<>(
            Arrays.asList("inFolder"));
    private final String FILE_NAME = "test.txt";


    @Before
    public void initialize() {
        rootFolder.addFolder(inFolder);
    }

    @Test
    public void testCreateStructure() {
        creator.createStructure(rootFolder, folderNames, FILE_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateStructureNullRootFolder() throws IllegalArgumentException {
        creator.createStructure(null, folderNames, FILE_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateStructureNullFolderNames() throws IllegalArgumentException {
        creator.createStructure(rootFolder, null, FILE_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateStructureNullFileName() throws IllegalArgumentException {
        creator.createStructure(rootFolder, folderNames, null);
    }

    @Test
    public void testCreateStructureNoFolders() {
        creator.createStructure(rootFolder, emptyFolderNames, FILE_NAME);
    }

    @Test
    public void testCreateStructureInExistingFolder() {
        creator.createStructure(rootFolder, existingFolderNames, FILE_NAME);
    }
}