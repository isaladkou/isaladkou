package com.epam.training.saladkou.hw12;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class PrinterTest {

    private Printer printer = new Printer();
    private Folder rootFolder = new Folder("root");
    private Folder inFolder = new Folder("inFolder");
    private File file = new File("test.txt");


    @Before
    public void initialize() {
        rootFolder.addFolder(inFolder);
        inFolder.addFile(file);
    }

    @Test
    public void testPrint() {
        printer.print(rootFolder);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPrintNullFolder() throws IllegalArgumentException {
        printer.print(null);
    }
}