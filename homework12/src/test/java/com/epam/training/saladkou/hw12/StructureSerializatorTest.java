package com.epam.training.saladkou.hw12;

import org.junit.Before;
import org.junit.Test;

public class StructureSerializatorTest {


    private final String PATH = "src/test/resources/serializedStructure.ser";
    private final String FOLDER_NAME = "folder";
    private StructureSerializator serializator;
    private Folder folder;

    @Before
    public void initialize() {
        serializator = new StructureSerializator();
        folder = new Folder(FOLDER_NAME);
    }


    @Test
    public void serialize() {
        serializator.serialize(folder, PATH);
    }

    @Test
    public void deserialize() {
        serializator.deserialize(PATH);
    }
}
