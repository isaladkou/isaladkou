package com.epam.training.saladkou.hw9.task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;

public class SetAssistantTest {
    private HashSet<String> set1;
    private HashSet<String> set2;

    private HashSet<String> union;
    private HashSet<String> intersection;
    private HashSet<String> minus;
    private HashSet<String> difference;

    private SetAssistant<String> setAssistant = new SetAssistant<>();

    @Before
    public void initialize() {
        setAssistant = new SetAssistant<>();
        set1 = new HashSet<>(Arrays.asList("A", "B"));
        set2 = new HashSet<>(Arrays.asList("B", "C"));
        union = new HashSet<>(Arrays.asList("A", "B", "C"));
        intersection = new HashSet<>(Arrays.asList("B"));
        minus = new HashSet<>(Arrays.asList("A"));
        difference = new HashSet<>(Arrays.asList("A", "C"));
    }


    @Test
    public void testUnion() {
        assertEquals(union, setAssistant.union(set1, set2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnionNull() throws IllegalArgumentException {
        assertEquals(union, setAssistant.union(null, set2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnionNull2() throws IllegalArgumentException {
        assertEquals(union, setAssistant.union(set1, null));
    }

    @Test
    public void testIntersection() {
        assertEquals(intersection, setAssistant.intersection(set1, set2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIntersectionNull() throws IllegalArgumentException {
        assertEquals(union, setAssistant.intersection(null, set2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIntersectionNull2() throws IllegalArgumentException {
        assertEquals(union, setAssistant.intersection(set1, null));
    }

    @Test
    public void testMinus() {
        assertEquals(minus, setAssistant.minus(set1, set2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMinusNull() throws IllegalArgumentException {
        assertEquals(union, setAssistant.minus(null, set2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMinusNull2() throws IllegalArgumentException {
        assertEquals(union, setAssistant.minus(set1, null));
    }


    @Test
    public void testDifference() {
        assertEquals(difference, setAssistant.difference(set1, set2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDifferenceNull() throws IllegalArgumentException {
        assertEquals(union, setAssistant.difference(null, set2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDifferenceNull2() throws IllegalArgumentException {
        assertEquals(union, setAssistant.difference(set1, null));
    }
}