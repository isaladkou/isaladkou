package com.epam.training.saladkou.hw9.task1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class DigitSumSorterTest {

    private DigitSumSorter digitSumSorter;
    private Integer[] ARRAY;
    private Integer[] SORTED_ARRAY;

    @Before
    public void initialize() {
        digitSumSorter = new DigitSumSorter();
        ARRAY = new Integer[]{712, 1703, 2911, 504};
        SORTED_ARRAY = new Integer[]{504, 712, 1703, 2911};
    }


    @Test
    public void testSort() {
        assertArrayEquals(SORTED_ARRAY, digitSumSorter.sort(ARRAY));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSortNull() throws IllegalArgumentException {
        assertArrayEquals(SORTED_ARRAY, digitSumSorter.sort(null));
    }
}
