package com.epam.training.saladkou.hw9.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DigitSumComparatorTest {

    private DigitSumComparator digitSumComparator;

    private final int ONE = 1;
    private final int MINUS_ONE = -1;
    private final int ZERO = 0;

    private final int TWELVE = 12;
    private final int MINUS_TWELVE = -12;
    private final int TWENTY_TWO = 22;

    @Before
    public void initialize() {
        digitSumComparator = new DigitSumComparator();
    }

    @Test
    public void testCompareLess() {
        assertEquals(MINUS_ONE, digitSumComparator.compare(TWELVE, TWENTY_TWO));
    }

    @Test
    public void testCompareGreater() {
        Assert.assertEquals(ONE, digitSumComparator.compare(TWENTY_TWO, TWELVE));
    }

    @Test
    public void testCompareEquals() {
        Assert.assertEquals(ZERO, digitSumComparator.compare(TWELVE, MINUS_TWELVE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCompareNull() throws IllegalArgumentException {
        digitSumComparator.compare(null,TWELVE);
    }
}