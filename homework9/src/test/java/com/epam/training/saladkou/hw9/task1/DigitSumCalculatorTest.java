package com.epam.training.saladkou.hw9.task1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DigitSumCalculatorTest {

    private DigitSumCalculator digitSumCalculator;
    private final int THREE = 3;
    private final int SIX = 6;
    private final int NEGATIVE_NUMBER = -123;
    private final int POSITIVE_NUMBER = 12;


    @Before
    public void initialize() {
        digitSumCalculator = new DigitSumCalculator();
    }

    @Test
    public void testGetDigits() {
        assertEquals(THREE, digitSumCalculator.getDigitsSum(POSITIVE_NUMBER));
    }

    @Test
    public void testGetDigitsNegativeNumber() {
        assertEquals((SIX), digitSumCalculator.getDigitsSum(NEGATIVE_NUMBER));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDigitsNull() throws IllegalArgumentException {
        digitSumCalculator.getDigitsSum(null);
    }
}