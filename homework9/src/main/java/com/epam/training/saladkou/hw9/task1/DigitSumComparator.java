package com.epam.training.saladkou.hw9.task1;

import java.util.Comparator;

public class DigitSumComparator implements Comparator<Integer> {

    public DigitSumComparator() {
        this.digitSumCalculator = new DigitSumCalculator();
    }

    private DigitSumCalculator digitSumCalculator;

    @Override
    public int compare(Integer number, Integer otherNUmber) {
        Integer digitSum1 = digitSumCalculator.getDigitsSum(number);
        Integer digitSum2 = digitSumCalculator.getDigitsSum((otherNUmber));
        return digitSum1.compareTo(digitSum2);
    }
}
