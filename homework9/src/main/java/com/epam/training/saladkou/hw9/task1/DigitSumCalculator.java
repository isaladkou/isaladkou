package com.epam.training.saladkou.hw9.task1;

public class DigitSumCalculator {

    public int getDigitsSum(Integer number) {
        checkArgumentIsNull(number);
        number = Math.abs(number);
        int sum = 0;
        String numberString = number.toString();
        char[] digitsCharArray = numberString.toCharArray();
        for (char digit : digitsCharArray) {
            sum += Character.getNumericValue(digit);
        }
        return sum;
    }

    private boolean checkArgumentIsNull(Integer number) {
        if (number == null) {
            throw new IllegalArgumentException("This argument can't be null");
        }
        return true;
    }
}
