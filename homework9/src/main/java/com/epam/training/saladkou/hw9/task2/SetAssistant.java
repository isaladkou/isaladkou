package com.epam.training.saladkou.hw9.task2;

import java.util.HashSet;

public class SetAssistant<T> {

    public HashSet<T> union(HashSet<T> set1, HashSet<T> set2) {
        checkAreSetsNull(set1, set2);
        HashSet<T> result = new HashSet<>(set1);
        result.addAll(set2);
        return result;
    }

    public HashSet<T> intersection(HashSet<T> set1, HashSet<T> set2) {
        checkAreSetsNull(set1, set2);
        HashSet<T> result = new HashSet<>(set1);
        result.retainAll(set2);
        return result;
    }

    public HashSet<T> minus(HashSet<T> set1, HashSet<T> set2) {
        checkAreSetsNull(set1, set2);
        HashSet<T> result = new HashSet<>(set1);
        result.removeAll(set2);
        return result;
    }

    public HashSet<T> difference(HashSet<T> set1, HashSet<T> set2) {
        checkAreSetsNull(set1, set2);
        return minus(union(set1, set2), intersection(set1, set2));
    }

    private boolean checkAreSetsNull(HashSet<T> set1, HashSet<T> set2) {
        if (set1 == null || set2 == null) {
            throw new IllegalArgumentException("No one set can't be null");
        }
        return true;
    }
}
