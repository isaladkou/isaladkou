package com.epam.training.saladkou.hw9.task1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class DigitSumSorter {


    public Integer[] sort(Integer[] array) {
        checkArgumentIsNull(array);
        ArrayList<Integer> sorted = new ArrayList<>(Arrays.asList(array));
        Collections.sort(sorted, new DigitSumComparator());
        return sorted.toArray(array);
    }

    private boolean checkArgumentIsNull(Integer[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Array can't be null");
        }
        return true;
    }

}
