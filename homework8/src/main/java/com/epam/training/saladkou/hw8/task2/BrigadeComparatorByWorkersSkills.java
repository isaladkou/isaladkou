package com.epam.training.saladkou.hw8.task2;

import java.util.Comparator;
import java.util.HashMap;

public class BrigadeComparatorByWorkersSkills implements Comparator<Brigade> {

    @Override
    public int compare(Brigade brigade1, Brigade brigade2) {
        HashMap<Skill, Integer> skillsMap = brigade1.getSkillAmountMap();
        HashMap<Skill, Integer> skillsMap2 = brigade2.getSkillAmountMap();
        if (!skillsMap.keySet().containsAll(skillsMap2.keySet())) {
            return -1;
        }
        int equalsCounter = 0;
        for (Skill skill : skillsMap2.keySet()) {
            if (skillsMap.get(skill) < skillsMap2.get(skill)) {
                return -1;
            } else if (skillsMap.get(skill).equals(skillsMap2.get(skill))) {
                equalsCounter++;
            }
        }
        return (equalsCounter == skillsMap.size()) ? 0 : 1;
    }
}
