package com.epam.training.saladkou.hw8.task2;

import java.util.Comparator;

public class BrigadeComparatorByFinancialOffer implements Comparator<Brigade> {
    @Override
    public int compare(Brigade brigade, Brigade brigade2) {
        return Integer.compare(brigade2.getFinancialOffer(), brigade.getFinancialOffer());
    }
}

