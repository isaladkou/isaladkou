package com.epam.training.saladkou.hw8.task1;


import java.util.*;

public class CustomArrayList<T> implements List<T> {
    private static final int DEFAULT_CAPACITY = 10;
    private final int MAX_CAPACITY;
    private Object[] values;

    public CustomArrayList() {
        this(DEFAULT_CAPACITY);
    }

    public CustomArrayList(int maxCapacity) {
        if (maxCapacity < 0) {
            throw new IllegalArgumentException("List capacity can't be negative!");
        }
        MAX_CAPACITY = maxCapacity;
        values = new Object[0];
    }

    public CustomArrayList(T[] array, int maxCapacity) {
        this(maxCapacity);
        copyArrayToValues(array, Math.min(maxCapacity, array.length));
    }

    @Override
    public boolean add(T element) {
        checkOverflow(size() + 1);
        resizeData(size() + 1);
        values[size() - 1] = element;
        return true;
    }

    @Override
    public void add(int index, Object element) {
    }

    @Override
    public boolean remove(Object object) {
        for (int index = 0; index < size(); index++) {
            if (object.equals(values[index])) {
                removeInData(index);
                return true;
            }
        }
        return false;
    }

    @Override
    public T remove(int index) {
        checkIndex(index);
        removeInData(index);
        return (T) values[0];
    }

    @Override
    public int indexOf(Object object) {
        for (int index = 0; index < values.length; index++) {
            if (object.equals(values[index])) {
                remove(index);
                return index;
            }
        }
        throw new NoSuchElementException("No such element");
    }

    @Override
    public boolean contains(Object object) {
        for (int i = 0; i < size(); i++) {
            if (values[i].equals(object)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        checkArgumentsIsNull(collection);
        Object[] arrayCollection = collection.toArray();
        checkOverflow(size() + arrayCollection.length);
        for (Object object : arrayCollection) {
            add((T) object);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection collection) {
        return false;
    }


    @Override
    public boolean removeAll(Collection collection) {
        return false;
    }

    @Override
    public int size() {
        return values.length;
    }

    @Override
    public boolean equals(Object otherList) {
        checkArgumentsIsNull(otherList);
        CustomArrayList<T> other = (CustomArrayList) otherList;
        if (this.size() == other.size() && this.MAX_CAPACITY == other.MAX_CAPACITY) {
            for (int i = 0; i < size(); i++) {
                if (!values[i].equals(other.values[i])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hashCode = (size() == 0) ? 7 : size();
        for (Object element : this.values) {
            hashCode = 29 * hashCode + (element == null ? 0 : element.hashCode() + 2);
        }
        System.out.println(hashCode);
        return hashCode;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }


    @Override
    public T get(int index) {
        checkIndex(index);
        return (T) values[index];
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean containsAll(Collection collection) {
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] array) {
        return null;
    }


    @Override
    public Object set(int index, Object element) {
        return null;
    }


    @Override
    public int lastIndexOf(Object object) {
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean retainAll(Collection collection) {
        return false;
    }


    public boolean isFull() {
        return size() == MAX_CAPACITY;
    }

    private void removeInData(int index) {
        Object[] newData = new Object[size() - 1];
        boolean isPassed = false;
        for (int i = 0; i < size(); i++) {
            if (i != index && !isPassed) {
                newData[i] = values[i];
            } else if (i != index && isPassed) {
                newData[i - 1] = values[i];
            } else if (i == index) {
                isPassed = true;
            }
        }
        values = newData;
    }

    private boolean checkIndex(int index) {
        if (index >= size() || index < 0) {
            throw new IndexOutOfBoundsException(
                    "The index was outside the bounds of the array!");
        }
        return true;
    }

    private boolean checkOverflow(int size) {
        if (size > MAX_CAPACITY) {
            throw new FullListException("The list is full");
        }
        return true;
    }

    private void resizeData(int size) {
        Object[] newData = new Object[size];
        for (int i = 0; i < size(); i++) {
            newData[i] = values[i];
        }
        values = newData;
    }

    private void copyArrayToValues(T[] array, int size) {
        values = new Object[size];
        System.arraycopy(array, 0, values, 0, size);
    }

    private boolean checkArgumentsIsNull(Object argument) {
        if (argument == null) {
            throw new IllegalArgumentException("Argument can't be null");
        }
        return true;
    }


}
