package com.epam.training.saladkou.hw8.task2;

public enum Skill {
    MASON,
    CARPENTER,
    ELECTRICIAN
}
