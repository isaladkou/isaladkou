package com.epam.training.saladkou.hw8.task2;

public class SuitableBrigadeNotFoundException extends RuntimeException {
    public SuitableBrigadeNotFoundException(String message) {
        super(message);
    }
}
