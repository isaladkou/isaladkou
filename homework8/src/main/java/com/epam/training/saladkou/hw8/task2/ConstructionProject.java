package com.epam.training.saladkou.hw8.task2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ConstructionProject {

    private Brigade requiredBrigade;

    public ConstructionProject(Brigade requiredBrigade) {
        this.requiredBrigade = requiredBrigade;
    }

    public Brigade getSuitableBrigade(ArrayList<Brigade> brigades) {
        BrigadeComparatorByWorkersSkills skillsComparator = new BrigadeComparatorByWorkersSkills();
        BrigadeComparatorByFinancialOffer offerComparator = new BrigadeComparatorByFinancialOffer();
        Comparator<Brigade> brigadeComparator = skillsComparator
                .thenComparing(offerComparator);
        Collections.sort(brigades, brigadeComparator);
        Brigade bestBrigade = brigades.get(brigades.size() - 1);
        if (skillsComparator.compare(bestBrigade, requiredBrigade) >= 0
                && offerComparator.compare(bestBrigade, requiredBrigade) >= 0) {
            return bestBrigade;
        } else {
            throw new SuitableBrigadeNotFoundException("No suitable brigades. Project is closed.");
        }
    }
}
