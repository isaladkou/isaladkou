package com.epam.training.saladkou.hw8.task2;

import java.util.HashSet;

public class Worker {

    private HashSet<Skill> skills;

    public HashSet<Skill> getSkills() {
        return skills;
    }

    public Worker(HashSet<Skill> skills) {
        this.skills = skills;
    }

}
