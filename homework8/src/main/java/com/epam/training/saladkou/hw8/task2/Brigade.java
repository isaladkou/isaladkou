package com.epam.training.saladkou.hw8.task2;

import java.util.ArrayList;
import java.util.HashMap;

public class Brigade {


    private ArrayList<Worker> workers;
    private int financialOffer;

    public int getFinancialOffer() {
        return financialOffer;
    }

    public Brigade(ArrayList<Worker> workers, int financialOffer) {
        this.workers = workers;
        this.financialOffer = financialOffer;
    }

    public HashMap<Skill, Integer> getSkillAmountMap() {
        HashMap<Skill, Integer> skillMap = new HashMap<>();
        for (Worker worker : workers) {
            for (Skill skill : worker.getSkills()) {
                if (skillMap.containsKey(skill)) {
                    Integer amount = skillMap.get(skill);
                    skillMap.put(skill, ++amount);
                } else {
                    skillMap.put(skill, 1);
                }
            }
        }
        return skillMap;
    }
}
