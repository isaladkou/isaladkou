package com.epam.training.saladkou.hw8.task1;

public class FullListException extends RuntimeException {
    public FullListException(final String message) {
        super(message);
    }
}
