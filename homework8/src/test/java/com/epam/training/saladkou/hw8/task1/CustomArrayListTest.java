package com.epam.training.saladkou.hw8.task1;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class CustomArrayListTest {

    private final int MINUS_ONE = -1;
    private final int ZERO = 0;
    private final int ONE = 1;
    private final int TWO = 2;
    private final int THREE = 3;
    private final int FOUR = 4;
    private final int FIVE = 5;
    private final int DEFAULT_CAPACITY = 10;
    private Integer[] DEFAULT_ARRAY;
    private final int CALCULATED_HASHCODE = 75811;

    private CustomArrayList<Integer> defaultList;
    private CustomArrayList<Integer> bigList;
    private ArrayList<Integer> collection;
    private CustomArrayList<Integer> fullList;
    private CustomArrayList<Integer> notFullList;
    private CustomArrayList<Integer> someList;
    private CustomArrayList<Integer> otherList;

    @Before
    public void initialize() {
        DEFAULT_ARRAY = new Integer[]{1, 2, 3};
        defaultList = new CustomArrayList<>();
        fullList = new CustomArrayList<>(DEFAULT_ARRAY, THREE);
        notFullList = new CustomArrayList<>(DEFAULT_ARRAY, DEFAULT_CAPACITY);
        collection = new ArrayList<>(Arrays.asList(6, 7));
        bigList = new CustomArrayList<>(new Integer[]{1, 2, 3, 6, 7}, DEFAULT_CAPACITY);
        someList = new CustomArrayList<>(DEFAULT_ARRAY, DEFAULT_CAPACITY);
        otherList = new CustomArrayList<>(new Integer[]{3, 4, 5}, DEFAULT_CAPACITY);
    }


    @Test
    public void testAddTrue() {
        assertTrue(defaultList.add(ONE));
    }

    @Test(expected = FullListException.class)
    public void testAddFullListException() throws FullListException {
        fullList.add(ONE);
    }

    @Test
    public void testRemove() {
        assertTrue(fullList.remove(new Integer(THREE)));
    }

    @Test
    public void testRemove2() {
        assertTrue(fullList.remove(new Integer(TWO)));
    }

    @Test
    public void testRemoveFalse() {
        assertFalse(fullList.remove(new Integer(FIVE)));
    }

    @Test
    public void testRemoveByIndex() {
        assertEquals(fullList.get(0), fullList.remove(ONE));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveByIndexException() throws IndexOutOfBoundsException {
        fullList.remove(MINUS_ONE);
    }

    @Test
    public void testIndexOf() {
        assertEquals(ZERO, fullList.indexOf(ONE));
    }

    @Test(expected = NoSuchElementException.class)
    public void testIndexOfNoSuchElement() throws NoSuchElementException {
        fullList.indexOf(FIVE);
    }

    @Test
    public void testContainsTrue() {
        assertTrue(fullList.contains(ONE));
    }

    @Test
    public void testContainsFalse() {
        assertFalse(fullList.contains(FOUR));
    }

    @Test
    public void testAddAll() {
        notFullList.addAll(collection);
        assertEquals(bigList, notFullList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddAllNull() throws IllegalArgumentException {
        notFullList.addAll(null);
    }

    @Test
    public void testSize() {
        assertEquals(THREE, fullList.size());
    }

    @Test
    public void testEqualsTrue() {
        assertEquals(notFullList, someList);
    }

    @Test
    public void testEqualsFalse() {
        assertNotEquals(notFullList, fullList);
    }

    @Test
    public void testEqualsFalse2() {
        assertNotEquals(notFullList, otherList);
    }

    @Test
    public void testHashCode() {
        assertEquals(CALCULATED_HASHCODE, notFullList.hashCode());
    }

    @Test
    public void testIsFullTrue() {
        assertTrue(fullList.isFull());
    }

    @Test
    public void testIsFullFalse() {
        assertFalse(notFullList.isFull());
    }

    @Test
    public void testIsEmptyTrue() {
        assertTrue(defaultList.isEmpty());
    }

    @Test
    public void testIsEmptyFalse() {
        assertFalse(fullList.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNegativeCapacity() {
        new CustomArrayList<>(MINUS_ONE);
    }


}