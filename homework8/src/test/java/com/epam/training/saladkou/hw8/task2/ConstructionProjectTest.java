package com.epam.training.saladkou.hw8.task2;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;

public class ConstructionProjectTest {
    private Worker carpenter;
    private Worker mason;
    private Worker electrician;
    private Worker carpenterElectrician;

    private Brigade requiredBrigade;
    private Brigade cheapestSuitableBrigade;
    private Brigade suitableBrigade;
    private Brigade unsuitableBrigade;
    private Brigade unsuitableBrigade2;
    private Brigade expensiveBrigade;

    private ArrayList<Brigade> suitableBrigades;
    private ArrayList<Brigade> unsuitableBrigades;


    private ArrayList<Brigade> brigades;
    private ConstructionProject constructionProject;

    @Before
    public void initialize() {
        carpenter = new Worker(new HashSet<>(Arrays.asList(Skill.CARPENTER)));
        mason = new Worker(new HashSet<>(Arrays.asList(Skill.MASON)));
        electrician = new Worker(new HashSet<>(Arrays.asList(Skill.ELECTRICIAN)));
        carpenterElectrician = new Worker(new HashSet<>(
                Arrays.asList(Skill.CARPENTER, Skill.ELECTRICIAN)));

        requiredBrigade = new Brigade(new ArrayList<>(Arrays.asList(carpenter, mason, electrician, electrician)),
                300);
        cheapestSuitableBrigade = new Brigade(new ArrayList(Arrays.asList(mason, electrician, carpenterElectrician)),
                210);
        suitableBrigade = new Brigade(new ArrayList(Arrays.asList(carpenter, mason, electrician)),
                250);
        unsuitableBrigade = new Brigade(new ArrayList(Arrays.asList(carpenter, mason)),
                50);
        unsuitableBrigade2 = new Brigade(new ArrayList(Arrays.asList(carpenter, mason, electrician)),
                50);
        expensiveBrigade = new Brigade(new ArrayList(Arrays.asList(carpenterElectrician, mason, electrician)),
                500);
        suitableBrigades = new ArrayList<>(Arrays.asList(cheapestSuitableBrigade, suitableBrigade));
        unsuitableBrigades = new ArrayList<>(
                Arrays.asList(unsuitableBrigade, unsuitableBrigade2, expensiveBrigade));
        brigades = new ArrayList<>(suitableBrigades);
        brigades.addAll(unsuitableBrigades);
        constructionProject = new ConstructionProject(requiredBrigade);
    }


    @Test
    public void testGetSuitableBrigade() {
        assertEquals(constructionProject.getSuitableBrigade(brigades), cheapestSuitableBrigade);
    }

    @Test(expected = SuitableBrigadeNotFoundException.class)
    public void testGetSuitableBrigadeNoBrigade() throws SuitableBrigadeNotFoundException {
        constructionProject.getSuitableBrigade(unsuitableBrigades);
    }
}