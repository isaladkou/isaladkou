package com.epam.training.saladkou.hw8.task2;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.*;

public class BrigadeComparatorByFinancialOfferTest {


    private BrigadeComparatorByFinancialOffer comparator;

    private final int SMALL_PRICE = 100;
    private final int BIG_PRICE = 500;


    private Brigade cheapBrigade;
    private Brigade expensiveBrigade;

    private final int ONE = 1;
    private final int MINUS_ONE = -1;
    private final int ZERO = 0;


    @Before
    public void initialize() {
        comparator = new BrigadeComparatorByFinancialOffer();
        cheapBrigade = new Brigade(null, SMALL_PRICE);
        expensiveBrigade = new Brigade(null, BIG_PRICE);
    }


    @Test
    public void testCompareGreater() {
        assertEquals(comparator.compare(cheapBrigade, expensiveBrigade), ONE);
    }

    @Test
    public void testCompareEquals() {
        assertEquals(comparator.compare(cheapBrigade, cheapBrigade), ZERO);
    }

    @Test
    public void testCompareLess() {
        assertEquals(comparator.compare(expensiveBrigade, cheapBrigade), MINUS_ONE);
    }
}