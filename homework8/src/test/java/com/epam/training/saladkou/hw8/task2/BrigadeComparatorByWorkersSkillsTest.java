package com.epam.training.saladkou.hw8.task2;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.*;

public class BrigadeComparatorByWorkersSkillsTest {

    private BrigadeComparatorByWorkersSkills comparator;

    private Worker carpenter;
    private Worker mason;
    private Worker electrician;

    private Brigade patternBrigade;
    private Brigade equivalentBrigade;
    private Brigade lessSkillsBrigade;
    private Brigade notEnoughWorkersBrigade;

    private final int ONE = 1;
    private final int MINUS_ONE = -1;
    private final int ZERO = 0;

    private final int DEFAULT_FINANCIAL_OFFER = 200;

    @Before
    public void initialize() {
        comparator = new BrigadeComparatorByWorkersSkills();

        carpenter = new Worker(new HashSet<>(Arrays.asList(Skill.CARPENTER)));
        mason = new Worker(new HashSet<>(Arrays.asList(Skill.MASON)));
        electrician = new Worker(new HashSet<>(Arrays.asList(Skill.ELECTRICIAN)));

        patternBrigade = new Brigade(new ArrayList<>(Arrays.asList(carpenter, mason, electrician, electrician)),
                DEFAULT_FINANCIAL_OFFER);
        equivalentBrigade = new Brigade(new ArrayList(Arrays.asList(carpenter, mason, electrician, electrician)),
                DEFAULT_FINANCIAL_OFFER);
        lessSkillsBrigade = new Brigade(new ArrayList(Arrays.asList(carpenter, mason)),
                DEFAULT_FINANCIAL_OFFER);
        notEnoughWorkersBrigade = new Brigade(new ArrayList(Arrays.asList(carpenter, mason, electrician)),
                DEFAULT_FINANCIAL_OFFER);
    }


    @Test
    public void testCompare() {
        assertEquals(ZERO, comparator.compare(patternBrigade, equivalentBrigade));
    }

    @Test
    public void testCompareGreater() {
        assertEquals(ONE, comparator.compare(patternBrigade, lessSkillsBrigade));
    }

    @Test
    public void testCompareGreater2() {
        assertEquals(ONE, comparator.compare(patternBrigade, notEnoughWorkersBrigade));
    }

    @Test
    public void testCompareLess() {
        assertEquals(MINUS_ONE, comparator.compare(notEnoughWorkersBrigade, patternBrigade));
    }

    @Test
    public void testCompareLess2() {
        assertEquals(MINUS_ONE, comparator.compare(lessSkillsBrigade, patternBrigade));
    }
}