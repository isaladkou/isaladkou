package com.epam.training.saladkou.hw8.task2;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.Assert.*;

public class BrigadeTest {

    private Brigade brigade;
    private Worker carpenter;
    private Worker electrician;
    private ArrayList<Worker> workers;
    private final int FINANCIAL_OFFER = 100;
    private HashMap<Skill, Integer> skillMap;


    @Before
    public void initialize() {
        skillMap = new HashMap<>();
        skillMap.put(Skill.CARPENTER, 2);
        skillMap.put(Skill.ELECTRICIAN, 1);
        carpenter = new Worker(new HashSet<>(Arrays.asList(Skill.CARPENTER)));
        electrician = new Worker(new HashSet<>(Arrays.asList(Skill.ELECTRICIAN)));
        workers = new ArrayList<>(Arrays.asList(carpenter, carpenter, electrician));
        brigade = new Brigade(workers, FINANCIAL_OFFER);
    }

    @Test
    public void testGetSkillAmountMap() {
        assertEquals(skillMap, brigade.getSkillAmountMap());
    }
}