package com.epam.training.saladkou.hw13.exceptions;

public class StreamException extends RuntimeException {
    public StreamException(String message, Throwable cause) {
        super(message, cause);
    }
}
