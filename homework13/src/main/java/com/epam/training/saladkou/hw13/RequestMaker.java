package com.epam.training.saladkou.hw13;

import com.epam.training.saladkou.hw13.exceptions.MappingFailedException;
import com.epam.training.saladkou.hw13.exceptions.ResponseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;

public interface RequestMaker {
    Post sendGet(int postId);

    Post sendPost(Post post);

    default Post mapStreamToObject(InputStream stream) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Post post = mapper.readValue(stream, Post.class);
            stream.close();
            return post;
        } catch (IOException exception) {
            throw new MappingFailedException(exception);
        }
    }

    default String getParametersString(Post post) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("userId=").append(post.getUserId()).append("&");
        stringBuilder.append("id=").append(post.getId()).append("&");
        stringBuilder.append("title=").append(post.getTitle()).append("&");
        stringBuilder.append("body=").append(post.getBody());
        return stringBuilder.toString();
    }

    default boolean checkStatusCodeIsSuccess(int statusCode) {
        if (statusCode > 199 && statusCode < 300) {
            return true;
        } else {
            throw new ResponseException("Response code was " + statusCode);
        }
    }
}
