package com.epam.training.saladkou.hw13.exceptions;

public class RequestException extends RuntimeException {
    public RequestException(Throwable cause) {
        super(cause);
    }

}
