package com.epam.training.saladkou.hw13;

import java.util.Map;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        Map<String, String> parameters;
        RequestMaker requestMaker;
        try {
            parameters = getRequestParameters();
            requestMaker = getRequestMaker(parameters.get("solution"));
            if (requestMaker != null) {
                sendRequest(requestMaker, parameters.get("method"),
                        Integer.parseInt(parameters.get("id")));
            }
        } catch (RuntimeException exception) {
            System.out.println(exception.getMessage());
            exception.printStackTrace();
        }
    }

    private static boolean sendRequest(RequestMaker requestMaker, String methodName, int id) {
        Post response = null;
        if (methodName.equals("GET")) {
            response = requestMaker.sendGet(id);
            printSuccessGetMessage(response);
        } else if (methodName.equals("POST")) {
            response = requestMaker.sendPost(new Post(id));
            printSuccessPostMessage(response);
        }
        return (response == null);
    }

    private static void printSuccessPostMessage(Post post) {
        if (post != null) {
            System.out.println("Article [" + post.getId() + "] has been created:"
                    + "User [" + post.getUserId() + "] Title [" + post.getTitle()
                    + "] Message [" + post.getBody() + "]");
        }
    }

    private static void printSuccessGetMessage(Post post) {
        if (post != null) {
            System.out.println(post);
        }
    }

    private static Map<String, String> getRequestParameters() {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        scanner.close();
        InputParser inputParser = new InputParser();
        Map<String, String> parameters = inputParser.getParametersMap(input);
        InputValidator inputValidator = new InputValidator();
        inputValidator.validate(parameters);
        return parameters;
    }

    private static RequestMaker getRequestMaker(String parameter) {
        int parameterInt = Integer.parseInt(parameter);
        if (parameterInt == 1) {
            return new StandardRequestMaker();
        } else if (parameterInt == 2) {
            return new ApacheRequestMaker();
        } else {
            return null;
        }
    }
}
