package com.epam.training.saladkou.hw13;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class InputParserTest {

    private final String DEFAULT_INPUT = "1 POST 1 1 title body";
    private final String NO_SEPARATOR_INPUT = "1POST_111/1,title-body";

    private InputParser inputParser;
    private Map<String, String> parametersMap;


    @Before
    public void initialize() {
        inputParser = new InputParser();
        parametersMap = new HashMap<>();
        parametersMap.put("id", "1");
        parametersMap.put("solution", "1");
        parametersMap.put("method", "POST");
    }


    @Test
    public void testGetParametersMap() {
        Assert.assertEquals(parametersMap, inputParser.getParametersMap(DEFAULT_INPUT));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetParametersMapNull() {
        inputParser.getParametersMap(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetParametersMapNoSeparator() {
        inputParser.getParametersMap(NO_SEPARATOR_INPUT);
    }
}