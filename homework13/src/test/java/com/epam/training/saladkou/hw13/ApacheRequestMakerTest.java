package com.epam.training.saladkou.hw13;

import com.epam.training.saladkou.hw13.exceptions.ResponseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ApacheRequestMakerTest {

    private final int ID = 1;
    private final int TO_SEND_ID = 101;
    private final int NOT_EXISTING_ID = 111;
    private final int USER_ID = 1;
    private final String TITLE = "sunt aut facere repellat provident "
            + "occaecati excepturi optio reprehenderit";
    private final String BODY = "quia et suscipit\n"
            + "suscipit recusandae consequuntur expedita et cum\n"
            + "reprehenderit molestiae ut ut quas totam\n"
            + "nostrum rerum est autem sunt rem eveniet architecto";


    private RequestMaker requestMaker;
    private Post defaultPost;
    private Post postToSend;

    @Before
    public void initialize() {
        requestMaker = new ApacheRequestMaker();
        defaultPost = new Post(USER_ID, ID, TITLE, BODY);
        postToSend = new Post(TO_SEND_ID);
    }

    @Test
    public void testSendGet() {
        Assert.assertEquals(defaultPost, requestMaker.sendGet(ID));
    }


    @Test
    public void testSendPost() {
        Assert.assertEquals(postToSend, requestMaker.sendPost(postToSend));
    }

    @Test(expected = ResponseException.class)
    public void testSendGetNotExisting() {
        requestMaker.sendGet(NOT_EXISTING_ID);
    }
}