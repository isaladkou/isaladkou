package com.epam.training.saladkou.hw18.mappers;

import com.epam.training.saladkou.hw18.entity.Good;
import com.epam.training.saladkou.hw18.utils.NullChecker;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GoodMapper {

    private final String NULL_MESSAGE = "Resultset can't be null";
    private final String ID_COLUMN = "ID";
    private final String TITLE_COLUMN = "TITLE";
    private final String PRICE_COLUMN = "PRICE";


    public Good mapResultSetToGoodEntity(ResultSet resultSet) throws SQLException {
        new NullChecker().checkIsArgumentNull(resultSet, NULL_MESSAGE);
        Good good = new Good();
        while (resultSet.next()) {
            good.setId(resultSet.getInt(ID_COLUMN));
            good.setTitle(resultSet.getString(TITLE_COLUMN));
            good.setPrice(resultSet.getDouble(PRICE_COLUMN));
        }
        return good;
    }

    public List<Good> mapResultSetToGoodList(ResultSet resultSet) throws SQLException {
        new NullChecker().checkIsArgumentNull(resultSet, NULL_MESSAGE);
        List<Good> goods = new ArrayList<>();
        while (resultSet.next()) {
            Good good = new Good();
            good.setId(resultSet.getInt(ID_COLUMN));
            good.setTitle(resultSet.getString(TITLE_COLUMN));
            good.setPrice(resultSet.getDouble(PRICE_COLUMN));
            goods.add(good);
        }
        return goods;
    }
}
