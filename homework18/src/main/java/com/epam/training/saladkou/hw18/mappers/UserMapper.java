package com.epam.training.saladkou.hw18.mappers;

import com.epam.training.saladkou.hw18.entity.User;
import com.epam.training.saladkou.hw18.utils.NullChecker;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper {

    private final String NULL_MESSAGE = "Resultset can't be null";
    private final String ID_COLUMN = "ID";
    private final String LOGIN_COLUMN = "LOGIN";
    private final String PASSWORD_COLUMN = "PASSWORD";

    public User mapResultSetToUserEntity(ResultSet resultSet) throws SQLException {
        new NullChecker().checkIsArgumentNull(resultSet, NULL_MESSAGE);
        User user = new User();
        while (resultSet.next()) {
            user.setId(resultSet.getInt(ID_COLUMN));
            user.setLogin(resultSet.getString(LOGIN_COLUMN));
            user.setPassword(resultSet.getString(PASSWORD_COLUMN));
        }
        return user;
    }
}

