package com.epam.training.saladkou.hw18.repository;

import com.epam.training.saladkou.hw18.entity.Order;
import com.epam.training.saladkou.hw18.mappers.OrderMapper;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class OrderRepository {

    private final String GET_ORDERS_BY_USER_ID_SQL_STATEMENT =
            "SELECT \"ORDER\".ID AS \"ORDER_ID\",\n"
                    + "       GOOD_ID    AS \"GOOD_ID\",\n"
                    + "       GOOD.title,\n"
                    + "       GOOD.PRICE,\n"
                    + "       ORDER_GOOD.amount\n"
                    + "FROM \"ORDER\",\n"
                    + "     ORDER_GOOD,\n"
                    + "     GOOD\n"
                    + "WHERE \"ORDER\".ID IN (\n"
                    + "    SELECT id FROM \"ORDER\" WHERE USER_ID = ?\n"
                    + "    )\n"
                    + "  AND ORDER_GOOD.ORDER_ID = \"ORDER\".ID\n"
                    + "  AND GOOD.ID = ORDER_GOOD.GOOD_ID\n"
                    + "ORDER BY \"ORDER\".ID;";


    private final String GET_ALL_USER_ORDERS_ID_SQL_STATEMENT =
            "SELECT \"ORDER\".ID FROM \"ORDER\" WHERE USER_ID=?";

    private final String GET_ALL_ORDERS_ID_SQL_STATEMENT =
            "SELECT \"ORDER\".ID FROM \"ORDER\"";


    private Logger logger = Logger.getLogger(OrderRepository.class);

    private final String DATABASE_URL =
            "jdbc:h2:mem:shop_database;INIT=RUNSCRIPT FROM"
                    + " './src/main/resources/sql/init.sql';DB_CLOSE_DELAY=-1";

    private final String ADD_ORDER_SQL_STATEMENT =
            "INSERT INTO \"ORDER\"(USER_ID, TOTAL_PRICE) VALUES (?,?)";


    public List<Order> getOrdersByUserId(int userId) {
        List<Order> orders = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement =
                     connection.prepareStatement(GET_ORDERS_BY_USER_ID_SQL_STATEMENT)) {

            preparedStatement.setInt(1, userId);
            logger.info("getting user " + userId + " orders...");
            ResultSet resultSet = preparedStatement.executeQuery();

            orders = new OrderMapper().mapResultSetToOrderList(resultSet);
            logger.info("user " + userId + " orders :" + orders);
            return orders;

        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return orders;
    }

    public boolean addOrderToUser(Order order, int userId) {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement =
                     connection.prepareStatement(ADD_ORDER_SQL_STATEMENT)) {

            preparedStatement.setInt(1, userId);
            preparedStatement.setDouble(2, order.getTotalSum());

            logger.info("adding order to user " + userId);

            int rowsAdded = preparedStatement.executeUpdate();

            new OrderGoodRepository().insertItems(order.getItems(), order.getId());
            return rowsAdded != 0;

        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return false;
    }


    public int getLastOrderId() {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement =
                     connection.prepareStatement(GET_ALL_ORDERS_ID_SQL_STATEMENT)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            int lastId = 0;
            while (resultSet.next()) {
                lastId = resultSet.getInt("ID");
            }
            logger.info("LAST ORDER ID: " + lastId);
            return lastId;
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return 0;
    }

}
