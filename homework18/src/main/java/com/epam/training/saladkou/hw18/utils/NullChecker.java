package com.epam.training.saladkou.hw18.utils;

public class NullChecker {

    private final String DEFAULT_MESSAGE = "Argument can't be null";

    public boolean checkIsArgumentNull(Object argument) {
        if (argument == null) {
            throw new IllegalArgumentException(DEFAULT_MESSAGE);
        }
        return true;
    }

    public boolean checkIsArgumentNull(Object argument, String message) {
        if (argument == null) {
            throw new IllegalArgumentException(message);
        }
        return true;
    }
}
