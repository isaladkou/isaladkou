package com.epam.training.saladkou.hw18.mappers;

import com.epam.training.saladkou.hw18.entity.Good;
import com.epam.training.saladkou.hw18.entity.Order;
import com.epam.training.saladkou.hw18.utils.NullChecker;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderMapper {

    private final String ORDER_ID_COLUMN = "ORDER_ID";
    private final String GOOD_ID_COLUMN = "GOOD_ID";
    private final String TITLE_COLUMN = "TITLE";
    private final String PRICE_COLUMN = "PRICE";
    private final String AMOUNT_COLUMN = "AMOUNT";


    public List<Order> mapResultSetToOrderList(ResultSet resultSet) throws SQLException {
        List<Order> orders = new ArrayList<>();
        new NullChecker().checkIsArgumentNull(resultSet);
        while (resultSet.next()) {
            int currentId = resultSet.getInt(ORDER_ID_COLUMN);

            Good good = new Good();
            good.setId(resultSet.getInt(GOOD_ID_COLUMN));
            good.setTitle(resultSet.getString(TITLE_COLUMN));
            good.setPrice(resultSet.getDouble(PRICE_COLUMN));

            Order order = getOrder(orders, currentId);
            order.addItem(good, resultSet.getInt(AMOUNT_COLUMN));
            orders.add(order);
        }
        return orders;
    }

    private Order getOrder(List<Order> orders, int id) {
        for (Order order : orders) {
            if (order.getId() == id) {
                orders.remove(order);
                return order;
            }
        }
        return new Order(id);
    }


}
