package com.epam.training.saladkou.hw18.repository;

import com.epam.training.saladkou.hw18.entity.Good;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

public class OrderGoodRepository {
    private final String DATABASE_URL =
            "jdbc:h2:mem:shop_database;INIT=RUNSCRIPT FROM"
                    + " './src/main/resources/sql/init.sql';DB_CLOSE_DELAY=-1";

    private Logger logger = Logger.getLogger(OrderGoodRepository.class);

    private final String SQL_STATEMENT =
            "INSERT INTO ORDER_GOOD (ORDER_ID,GOOD_ID, AMOUNT ) VALUES(?,?,?) ";


    public int insertItems(Map<Good, Integer> orderMap, int orderId) {
        int counter = 0;
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQL_STATEMENT)) {
            logger.info("inserting rows to ORDER_GODD table");
            for (Good good : orderMap.keySet()) {
                preparedStatement.setInt(1, orderId);
                preparedStatement.setInt(2, good.getId());
                preparedStatement.setInt(3, orderMap.get(good));
                counter += preparedStatement.executeUpdate();
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        logger.info("ITEMS ADDED: " + counter);
        return counter;
    }


}
