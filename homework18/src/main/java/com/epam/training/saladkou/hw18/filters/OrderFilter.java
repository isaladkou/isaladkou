package com.epam.training.saladkou.hw18.filters;


import com.epam.training.saladkou.hw18.entity.Good;
import com.epam.training.saladkou.hw18.entity.Order;
import com.epam.training.saladkou.hw18.repository.GoodRepository;
import com.epam.training.saladkou.hw18.repository.OrderRepository;
import com.epam.training.saladkou.hw18.utils.RequestResender;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class OrderFilter implements Filter {

    private final String CART_PAGE_URL = "http://localhost:8888/online-shop/cart";
    private final String ORDER_ATTRIBUTE = "order";

    private final String ACTION_PARAMETER = "act";
    private final String ORDERED_ITEM_PARAMETER = "orderedItem";
    private final String ADD_ITEM_ACTION = "Add Item";
    private final String REFERER_HEADER = "referer";
    private final String ORDER_PAGE_URL = "order";

    private final Logger logger = Logger.getLogger(OrderFilter.class);


    private boolean checkIsAddButtonClicked(HttpServletRequest request) {
        return request.getParameter(ACTION_PARAMETER).equals(ADD_ITEM_ACTION);
    }

    private Order getOrder(HttpSession session) {
        if (session.getAttribute(ORDER_ATTRIBUTE) == null) {
            int lastOrderId = new OrderRepository().getLastOrderId();
            session.setAttribute(ORDER_ATTRIBUTE, new Order(lastOrderId + 1));
        }
        return (Order) session.getAttribute(ORDER_ATTRIBUTE);
    }

    private boolean refreshOrder(HttpServletRequest request) {
        HttpSession session = request.getSession();
        int itemId = Integer.parseInt(request.getParameter(ORDERED_ITEM_PARAMETER));

        Good item = new GoodRepository().getGoodById(itemId);
        Order order = getOrder(session);
        return order.addItem(item);
    }


    private void handleRequest(HttpServletRequest request,
                               ServletResponse servletResponse, FilterChain filterChain) {
        RequestResender resender = new RequestResender();
        if (checkIsAddButtonClicked(request)) {
            if (request.getParameter(ORDERED_ITEM_PARAMETER) != null) {
                refreshOrder(request);
            }
            resender.putToFilterChain(request, servletResponse, filterChain);
        } else {
            resender.forwardRequest(ORDER_PAGE_URL, request, servletResponse);
        }
    }


    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException {
        logger.info("in OrderFilter");

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (request.getHeader(REFERER_HEADER).equals(CART_PAGE_URL)) {
            logger.info(REFERER_HEADER + " :" + request.getHeader(REFERER_HEADER));
            handleRequest(request, servletResponse, filterChain);
            return;
        }
        new RequestResender().putToFilterChain(request, servletResponse, filterChain);
    }


    @Override
    public void destroy() {

    }
}
