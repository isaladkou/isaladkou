package com.epam.training.saladkou.hw18.utils;

import com.epam.training.saladkou.hw18.entity.Good;
import com.epam.training.saladkou.hw18.entity.Order;

public class HtmlOrderPrinter {
    public String wrapOrderToTable(Order order) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("<table border=\"1\">");
        stringBuilder.append("<caption>Order #").append(order.getId()).append("</caption>");
        stringBuilder.append("<tr><td>Good</td><td>Price</td><td>Amount</td><td>Sum</td></tr>");
        for (Good good : order.getItems().keySet()) {
            int amount = order.getItems().get(good);
            stringBuilder.append("<tr><td>").append(good.getTitle()).append("</td><td>")
                    .append(good.getPrice()).append("</td><td>")
                    .append(amount).append("</td><td>")
                    .append(good.getPrice() * amount).append("</td></tr>");
        }
        stringBuilder.append("<tr><td>Total sum:</td><td></td><td></td><td>")
                .append(order.getTotalSum()).append("</td></tr>");
        stringBuilder.append("<br></table>");
        return stringBuilder.toString();
    }
}
