package com.epam.training.saladkou.hw18.servlets;

import com.epam.training.saladkou.hw18.entity.Order;
import com.epam.training.saladkou.hw18.entity.User;
import com.epam.training.saladkou.hw18.utils.RequestResender;
import com.epam.training.saladkou.hw18.repository.OrderRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "Order", urlPatterns = "/order")
public class OrderServlet extends HttpServlet {

    private final String ORDER_PAGE_URL = "order.jsp";
    private final String ORDER_ATTRIBUTE = "order";
    private final String USER_ATTRIBUTE = "user";


    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        Order order = (Order)session.getAttribute(ORDER_ATTRIBUTE);
        User user = (User) session.getAttribute(USER_ATTRIBUTE);

        new OrderRepository().addOrderToUser(order,user.getId());
        new RequestResender().forwardRequest(ORDER_PAGE_URL, request, response);
    }
}
