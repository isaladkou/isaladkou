package com.epam.training.saladkou.hw18.repository;

import com.epam.training.saladkou.hw18.entity.Good;
import com.epam.training.saladkou.hw18.mappers.GoodMapper;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.List;

public class GoodRepository {

    private final String DATABASE_URL =
            "jdbc:h2:mem:shop_database;INIT=RUNSCRIPT FROM "
                    + "'./src/main/resources/sql/init.sql';DB_CLOSE_DELAY=-1";

    private Logger logger = Logger.getLogger(GoodRepository.class);

    private final String GET_GOOD_BY_ID_SQL_STATEMENT =
            "SELECT id, title, price FROM GOOD WHERE id=?";

    private final String GET_ALL_GOODS_SQL_STATEMENT =
            "SELECT * FROM GOOD";


    public Good getGoodById(int id) {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement =
                     connection.prepareStatement(GET_GOOD_BY_ID_SQL_STATEMENT)) {

            preparedStatement.setInt(1, id);
            logger.info("getting good by id =" + id);

            ResultSet resultSet = preparedStatement.executeQuery();
            Good good = new GoodMapper().mapResultSetToGoodEntity(resultSet);

            logger.info("GOOD :" + good);
            return good;

        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return null;
    }

    public List<Good> getAll() {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement
                     = connection.prepareStatement(GET_ALL_GOODS_SQL_STATEMENT)) {
            logger.info("getting all goods...");
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Good> goods = new GoodMapper().mapResultSetToGoodList(resultSet);
            logger.info("Goods:" + goods);
            return goods;

        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return null;
    }
}
