package com.epam.training.saladkou.hw18.servlets;

import com.epam.training.saladkou.hw18.entity.Order;
import com.epam.training.saladkou.hw18.entity.User;
import com.epam.training.saladkou.hw18.utils.RequestResender;
import com.epam.training.saladkou.hw18.repository.OrderRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@WebServlet(urlPatterns = "/history")
public class HistoryServlet extends HttpServlet {

    private final String HISTORY_ATTRIBUTE = "history";
    private final String USER_ATTRIBUTE = "user";
    private final String HISTORY_PAGE_URL = "order_history.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)  {
        HttpSession session = request.getSession();
        OrderRepository orderRepository = new OrderRepository();
        User user = (User)session.getAttribute(USER_ATTRIBUTE);
        List<Order> orders = orderRepository.getOrdersByUserId(user.getId());
        session.setAttribute(HISTORY_ATTRIBUTE, orders);
        new RequestResender().forwardRequest(HISTORY_PAGE_URL, request, response);
    }
}
