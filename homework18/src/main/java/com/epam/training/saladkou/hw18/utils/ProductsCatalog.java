package com.epam.training.saladkou.hw18.utils;

import com.epam.training.saladkou.hw18.entity.Good;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductsCatalog {

    private ProductsCatalog() {
        this.catalog = new ArrayList<>(
                Arrays.asList(new Good(1, "Pants", 5),
                        new Good(2, "Black Hat", 3),
                        new Good(3, "Shoes", 12.3)));
    }

    private static ProductsCatalog instance;

    public static ProductsCatalog getInstance() {
        if (instance == null) {
            instance = new ProductsCatalog();
        }
        return instance;
    }


    public List<Good> getCatalog() {
        return catalog;
    }

    private final List<Good> catalog;

    public Good getProductById(int id) {
        Good requested = null;
        for (Good good : catalog) {
            if (good.getId() == id) {
                requested = good;
            }
        }
        return requested;
    }
}
