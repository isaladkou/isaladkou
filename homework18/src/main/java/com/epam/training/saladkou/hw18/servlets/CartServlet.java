package com.epam.training.saladkou.hw18.servlets;

import com.epam.training.saladkou.hw18.entity.Good;
import com.epam.training.saladkou.hw18.utils.RequestResender;
import com.epam.training.saladkou.hw18.repository.GoodRepository;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;


@WebServlet(urlPatterns = "/cart")
public class CartServlet extends HttpServlet {

    private final Logger logger = Logger.getLogger(CartServlet.class);

    private final String GOODS_ATTRIBUTE = "goods";
    private final String CART_PAGE_URL = "cart.jsp";


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        logger.info("in CartServlet doPost method");
        GoodRepository goodRepository = new GoodRepository();
        List<Good> goods = goodRepository.getAll();
        HttpSession session = request.getSession();
        session.setAttribute(GOODS_ATTRIBUTE, goods);
        logger.info("resending request to cart.jsp");
        new RequestResender().forwardRequest(CART_PAGE_URL, request, response);

    }
}
