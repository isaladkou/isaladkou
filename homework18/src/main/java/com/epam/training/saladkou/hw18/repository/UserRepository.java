package com.epam.training.saladkou.hw18.repository;

import com.epam.training.saladkou.hw18.entity.User;
import com.epam.training.saladkou.hw18.mappers.UserMapper;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;

public class UserRepository {
    private Logger logger = Logger.getLogger(UserRepository.class);


    private final String DATABASE_URL =
            "jdbc:h2:mem:shop_database;INIT=RUNSCRIPT FROM "
                    + "'./src/main/resources/sql/init.sql';DB_CLOSE_DELAY=-1";

    private final String SQL_STATEMENT = "SELECT id, login, password FROM USER WHERE login=?";

    private final String ADD_USER_SQL_STATEMENT =
            "INSERT INTO USER(LOGIN, PASSWORD ) VALUES ( ?,? )";


    public User getUserByLogin(String login) {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQL_STATEMENT)) {

            preparedStatement.setString(1, login);

            logger.info("getting user " + login + " from db...");
            ResultSet resultSet = preparedStatement.executeQuery();
            User user = getUserEntity(resultSet, login);
            logger.info("USER :" + user);
            return user;

        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return null;
    }

    public boolean addUser(User user) {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement =
                     connection.prepareStatement(ADD_USER_SQL_STATEMENT)) {

            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());

            logger.info("adding user " + user + " to db...");

            int rowsAdded = preparedStatement.executeUpdate();
            logger.info("Row added: " + rowsAdded);

            return rowsAdded != 0;

        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return false;
    }

    private User getUserEntity(ResultSet resultSet, String login) {
        try {
            if (!resultSet.isBeforeFirst()) {
                logger.info("adding new user...");
                return getUserByLogin(registerNewUser(login).getLogin());
            } else {
                logger.info("getting user from db...");
                return new UserMapper().mapResultSetToUserEntity(resultSet);
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return null;
    }

    private User registerNewUser(String login) {
        User user = new User();
        user.setLogin(login);
        addUser(user);
        return user;
    }
}
