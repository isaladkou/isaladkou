

CREATE TABLE IF NOT EXISTS User
(
    id       int NOT NULL AUTO_INCREMENT,
    login    varchar(255),
    password varchar(255),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS "ORDER"
(
    id       int NOT NULL AUTO_INCREMENT,
    user_id    int,
    total_price float,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES User(id)
);

CREATE TABLE IF NOT EXISTS Good
(
    id       int NOT NULL AUTO_INCREMENT,
    title    varchar(255),
    price float,
    PRIMARY KEY (id)
);


CREATE TABLE IF NOT EXISTS Order_Good
(
    id       int NOT NULL AUTO_INCREMENT,
    order_id    int,
    good_id int,
    amount int,
    PRIMARY KEY (id),
    FOREIGN KEY (order_id) REFERENCES "ORDER" (id),
    FOREIGN KEY (good_id) REFERENCES Good (id)
);

MERGE INTO User VALUES ( 1,'ISaladkou','' );
MERGE INTO User VALUES ( 2,'VKalesnikau','' );


MERGE INTO Good VALUES ( 1,'Black Hat', 2.3 );
MERGE INTO Good VALUES ( 2,'Pants', 5.5 );
MERGE INTO Good VALUES ( 3,'Shoes', 11.4 );

MERGE INTO "ORDER" VALUES ( 1,1,7.8 );
MERGE INTO "ORDER" VALUES ( 2,2,13.7 );
MERGE INTO "ORDER" VALUES ( 3,1,21.1 );


MERGE INTO Order_Good VALUES ( 1,1,1,1 );
MERGE INTO Order_Good VALUES ( 2,1,2,1 );
MERGE INTO Order_Good VALUES ( 3,2,1,1 );
MERGE INTO Order_Good VALUES ( 4,2,3,1 );
MERGE INTO Order_Good VALUES ( 5,3,1,2 );
MERGE INTO Order_Good VALUES ( 6,3,2,3 );





