<%@page import="com.epam.training.saladkou.hw18.entity.Order,
                com.epam.training.saladkou.hw18.entity.User"
%>
<%@ page import="com.epam.training.saladkou.hw18.utils.HtmlOrderPrinter" %>
<html>
<head>
    <title>Order page</title>
</head>
<body>

<p>Dear,&nbsp<%
    User user = (User) session.getAttribute("user");
    out.println(user.getLogin());
%>
    your order is</p>
<%
    Order order = (Order)session.getAttribute("order");
    out.println(new HtmlOrderPrinter().wrapOrderToTable(order));
%>
</body>
</html>