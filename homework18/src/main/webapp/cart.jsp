<%@ page import="com.epam.training.saladkou.hw18.entity.Good" %>
<%@ page import="com.epam.training.saladkou.hw18.entity.Order" %>
<%@ page import="com.epam.training.saladkou.hw18.entity.User" %>
<%@ page import="com.epam.training.saladkou.hw18.utils.HtmlOrderPrinter" %>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="java.util.List" %>

<html>
<head>
    <title>Cart page</title>
</head>
<body>
<%! Logger logger = Logger.getLogger("Cart_jsp"); %>

<p>Hello&nbsp<%
    User user =(User)session.getAttribute("user");
    out.println(user.getLogin()); %>
    !</p>

<%
    Order order = (Order)session.getAttribute("order");
    logger.info(order);

    if(order!=null){
        out.println("<p>You have already chosen:</p>");
        out.println(new HtmlOrderPrinter().wrapOrderToTable(order));
    }
%>

<form name="orderForm" method="post" action="cart">
    <p>Make your order <br></p>
    <select size="1" name="orderedItem">
        <%
            List<Good> catalog = (List<Good>) session.getAttribute("goods");
            for (Good good : catalog) {
                out.println("<option value =" + good.getId() + ">" + good.toString() + "</option>");
            }
        %>
    </select><br><br>
    <input name="act" type="submit" value="Add Item">
    <input name="act" type="submit" value="Submit">
</form>
<br><a href = "history">History</a>
</body>
</html>