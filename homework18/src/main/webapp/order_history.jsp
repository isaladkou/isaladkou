<%@ page import="com.epam.training.saladkou.hw18.entity.Order" %>
<%@ page import="java.util.List" %>
<%@ page import="com.epam.training.saladkou.hw18.utils.HtmlOrderPrinter" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order history</title>
</head>
<body>
<h3>Here is your order history</h3>
<%
    List<Order> history = (List<Order>)session.getAttribute("history");
    HtmlOrderPrinter printer = new HtmlOrderPrinter();
    for(Order order : history){
        out.println(printer.wrapOrderToTable(order));
    }
%>

</body>
</html>
