package com.epam.training.saladkou.hw6;

import java.util.ArrayList;

public class Distributor {

    public ArrayList<WordGroup> groupByFirstLetter(ArrayList<String> words) {
        if (words == null) {
            throw new IllegalArgumentException("Words list can't be null");
        }
        ArrayList<WordGroup> groups = new ArrayList<>();
        for (String currentWord : words) {
            String firstLetter = currentWord.substring(0, 1);
            try {
                WordGroup group = getGroupByLetter(groups, firstLetter);
                group.addOccurrence(currentWord);
            } catch (NoSuchGroupException e) {
                addWordToNewGroup(currentWord, groups);
            }
        }
        return groups;
    }


    private WordGroup getGroupByLetter(ArrayList<WordGroup> groups, String letter) {
        for (WordGroup group : groups) {
            if (group.getLetter().equalsIgnoreCase(letter)) {
                return group;
            }
        }
        throw new NoSuchGroupException("No group with such letter");
    }

    private void addWordToNewGroup(String word, ArrayList<WordGroup> groups) {
        String firstLetter = word.substring(0, 1);
        WordGroup newGroup = new WordGroup(firstLetter);
        newGroup.addOccurrence(word);
        groups.add(newGroup);
    }
}
