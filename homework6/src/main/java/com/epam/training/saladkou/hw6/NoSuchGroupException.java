package com.epam.training.saladkou.hw6;

public class NoSuchGroupException extends RuntimeException {

    public NoSuchGroupException(String message) {
        super(message);
    }
}
