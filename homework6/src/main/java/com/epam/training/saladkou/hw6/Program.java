package com.epam.training.saladkou.hw6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Program {

    private static final String BRACES_AND_COMMA_REGEX = "[\\[\\],]";

    public static void main(String[] args) {
        System.out.println("Please enter text in English");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();

        TextEditor textEditor = new TextEditor();
        String noPunctuationText = textEditor.deletePunctuation(text);
        ArrayList<String> words = textEditor.convertToList(noPunctuationText);

        Distributor distributor = new Distributor();
        ArrayList<WordGroup> groups = distributor.groupByFirstLetter(words);

        Collections.sort(groups);

        String string = Arrays.toString(groups.toArray());
        System.out.println(string.replaceAll(BRACES_AND_COMMA_REGEX, ""));
    }
}
