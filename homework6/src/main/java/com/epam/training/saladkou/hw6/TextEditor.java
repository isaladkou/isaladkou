package com.epam.training.saladkou.hw6;

import java.util.ArrayList;
import java.util.Arrays;

public class TextEditor {

    public ArrayList<String> convertToList(String text) {
        String[] array = text.split(" ");
        ArrayList<String> words = new ArrayList<>(Arrays.asList(array));
        return words;
    }

    public String deletePunctuation(String text) {
        String regex = "[^A-Za-z0-9 ]";
        return text.replaceAll(regex, "");
    }

}
