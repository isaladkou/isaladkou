package com.epam.training.saladkou.hw6;

import java.util.ArrayList;

public class WordGroup implements Comparable<WordGroup> {

    private String letter;
    private ArrayList<Occurrence> occurrences;

    public String getLetter() {
        return letter;
    }

    public WordGroup(String letter) {
        this.letter = letter.toUpperCase();
        occurrences = new ArrayList<>();
    }

    public boolean addOccurrence(String word) {
        if (checkAddingWordIsValid(word)) {
            try {
                Occurrence occurrence = getOccurrence(word);
                occurrence.setAmount(occurrence.getAmount() + 1);
                return true;
            } catch (NoSuchOccurrenceException e) {
                occurrences.add(new Occurrence(word));
                return true;
            }
        }
        return false;
    }

    private boolean checkAddingWordIsValid(String word) {
        if (word == null) {
            throw new IllegalArgumentException("Adding occurrence was null");
        }
        if (!word.toUpperCase().startsWith(letter)) {
            throw new IllegalArgumentException("First letter of the word is wrong");
        }
        return true;
    }


    private Occurrence getOccurrence(String word) {
        for (Occurrence occurrence : occurrences) {
            if (occurrence.getWord().equals(word)) {
                return occurrence;
            }
        }
        throw new NoSuchOccurrenceException("No occurrence of such word ");
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("\n" + letter + ":\n\t");
        for (Occurrence occurrence : occurrences) {
            string.append(occurrence.toString()).append("\n\t");
        }
        int size = string.length();
        return string.toString().substring(0, size - 1);
    }


    @Override
    public int compareTo(WordGroup other) {
        return this.letter.compareTo(other.getLetter());
    }
}
