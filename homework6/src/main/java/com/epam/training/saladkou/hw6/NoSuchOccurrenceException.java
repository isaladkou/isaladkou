package com.epam.training.saladkou.hw6;

public class NoSuchOccurrenceException extends RuntimeException {

    public NoSuchOccurrenceException(String message) {
        super(message);
    }
}
