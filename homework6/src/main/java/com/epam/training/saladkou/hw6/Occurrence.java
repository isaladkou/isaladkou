package com.epam.training.saladkou.hw6;

public class Occurrence {

    private String word;
    private int amount;

    public String getWord() {
        return word;
    }

    public int getAmount() {
        return amount;
    }


    public Occurrence(String word) {
        if (word == null) {
            throw new IllegalArgumentException("Word parameter can't be null");
        }
        this.word = word.toLowerCase();
        this.amount = 1;
    }

    public void setAmount(int amount) {
        if (amount >= 0) {
            this.amount = amount;
        } else {
            throw new IllegalArgumentException("Amount can't be negative");
        }
    }

    @Override
    public String toString() {
        return word + " " + amount;
    }

}
