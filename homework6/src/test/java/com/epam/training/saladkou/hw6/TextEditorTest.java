package com.epam.training.saladkou.hw6;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class TextEditorTest {

    private TextEditor textEditor = new TextEditor();

    private final String PUNCTUATION_STRING = "This, string;$% contains- no :(punctuation)!";
    private final String NO_PUNCTUATION_STRING = "This string contains no punctuation";
    private final String TEST_STRING = "This is a test string";
    private final ArrayList<String> STRING_LIST = new ArrayList<String>(
            Arrays.asList("This", "is", "a", "test", "string"));


    @Test
    public void testDeletePunctuation() {
        Assert.assertEquals(NO_PUNCTUATION_STRING,
                textEditor.deletePunctuation(PUNCTUATION_STRING));
    }

    @Test
    public void testConvertToList() {
        Assert.assertEquals(STRING_LIST, textEditor.convertToList(TEST_STRING));
    }


}