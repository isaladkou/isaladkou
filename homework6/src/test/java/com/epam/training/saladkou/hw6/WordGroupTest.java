package com.epam.training.saladkou.hw6;

import org.junit.Test;

public class WordGroupTest {

    private WordGroup wordGroup = new WordGroup("T");
    private final String RIGHT_WORD = "test";
    private final String WRONG_WORD = "wrong";


    @Test
    public void testAddOccurrence() {
        wordGroup.addOccurrence(RIGHT_WORD);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddOccurrenceNull() throws IllegalArgumentException {
        wordGroup.addOccurrence(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddOccurrenceWrongWord() throws IllegalArgumentException {
        wordGroup.addOccurrence(WRONG_WORD);
    }

    @Test
    public void testAddOccurrenceExistingWord() {
        wordGroup.addOccurrence(RIGHT_WORD);
        wordGroup.addOccurrence(RIGHT_WORD);
    }
}