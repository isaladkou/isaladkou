package com.epam.training.saladkou.hw6;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class DistributorTest {

    private ArrayList<WordGroup> groups = new ArrayList<>();
    private ArrayList<String> words = new ArrayList<>(
            Arrays.asList("This", "is", "a", "test", "test"));
    private Distributor distributor = new Distributor();

    @Before
    public void createExpectedGroups() {

        WordGroup group1 = new WordGroup("T");
        group1.addOccurrence("this");
        group1.addOccurrence("test");
        group1.addOccurrence("test");
        groups.add(group1);

        WordGroup group2 = new WordGroup("I");
        group2.addOccurrence("is");
        groups.add(group2);

        WordGroup group3 = new WordGroup("A");
        group3.addOccurrence("a");
        groups.add(group3);
    }

    @Test
    public void testGroupByFirstLetter() {
        Assert.assertEquals(groups, distributor.groupByFirstLetter(words));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGroupByFirstLetterNullParameter() throws IllegalArgumentException {
        distributor.groupByFirstLetter(null);
    }
}