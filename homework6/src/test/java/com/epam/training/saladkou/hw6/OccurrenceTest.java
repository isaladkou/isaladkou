package com.epam.training.saladkou.hw6;

import org.junit.Assert;
import org.junit.Test;


public class OccurrenceTest {

    private Occurrence occurrence = new Occurrence("test");

    @Test
    public void testSetAmount() {
        int expectedAmount = occurrence.getAmount() + 1;
        occurrence.setAmount(expectedAmount);
        Assert.assertEquals(expectedAmount, occurrence.getAmount());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetWrongAmount() throws IllegalArgumentException {
        occurrence.setAmount(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateOccurrenceNullWord() throws IllegalArgumentException {
        new Occurrence(null);
    }


}