package com.epam.training.saladkou.hw5;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FolderTest {

    private final String DEFAULT_FOLDER_NAME = "folder";
    private final String IN_FOLDER_NAME = "inFolder";
    private final String SOME_FOLDER_NAME = "someFolder";
    private final String NOT_EXISTING_FOLDER_NAME = "folder1234";
    private final String FILE_NAME = "file.txt";
    private final String WRONG_FOLDER_NAME = "&//%6";

    private Folder folder = new Folder(DEFAULT_FOLDER_NAME);
    private Folder inFolder = new Folder(IN_FOLDER_NAME);
    private File file = new File(FILE_NAME);

    @Before
    public void initialize() {
        folder.addFolder(inFolder);
    }

    @Test
    public void testAddFile() {
        Assert.assertTrue(folder.addFile(file));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddFileNull() throws IllegalArgumentException {
        folder.addFile(null);
    }

    @Test(expected = FileAlreadyExistException.class)
    public void testAddFileExisting() throws FileAlreadyExistException {
        folder.addFile(file);
        folder.addFile(file);
    }

    @Test
    public void testAddFolder() {
        Assert.assertTrue(folder.addFolder(new Folder(SOME_FOLDER_NAME)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddFolderNull() throws IllegalArgumentException {
        folder.addFolder(null);
    }

    @Test
    public void testGetFolderByName() {
        Assert.assertEquals(folder.getFolderByName(IN_FOLDER_NAME).getName(), IN_FOLDER_NAME);
    }

    @Test(expected = FolderNotFoundException.class)
    public void testGetFolderByNameFolderNotFound() throws FolderNotFoundException {
        folder.getFolderByName(NOT_EXISTING_FOLDER_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateFolderNullName() throws IllegalArgumentException {
        new Folder(null);
    }

    @Test(expected = WrongFolderNameException.class)
    public void testCreateFolderWrongName() throws WrongFolderNameException {
        new Folder(WRONG_FOLDER_NAME);
    }


}