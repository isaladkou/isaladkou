package com.epam.training.saladkou.hw5;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class PathParserTest {

    private PathParser parser = new PathParser();
    private final String DEFAULT_PATH = "root/folder1/folder2/test.txt";
    private final String PATH_WITHOUT_FILE = "root/folder1/folder2";
    private final String FILE_NAME = "test.txt";
    private final String EMPTY_PATH = "";

    private final ArrayList<String> FOLDER_NAMES = new ArrayList<>(Arrays.asList("folder1", "folder2"));


    @Before
    public void initialize() {
        parser.setPath(DEFAULT_PATH);
    }

    @Test
    public void testGetFoldersNamesFromPath() {
        assertEquals(FOLDER_NAMES, parser.getFoldersNames());
    }

    @Test
    public void testGetFileNameFromPath() {
        assertEquals(FILE_NAME, parser.getFileName());
    }


    @Test
    public void testGetFoldersNamesFromPathWithoutFile() {
        parser.setPath(PATH_WITHOUT_FILE);
        assertEquals(FOLDER_NAMES, parser.getFoldersNames());
    }

    @Test
    public void testGetFileNameFromPathWithoutFile() {
        parser.setPath(PATH_WITHOUT_FILE);
        assertEquals("", parser.getFileName());
    }

    @Test
    public void testGetFoldersNamesFromEmtyPath() {
        parser.setPath(EMPTY_PATH);
        parser.getFoldersNames();
    }
}