package com.epam.training.saladkou.hw5;

public class WrongFileNameException extends RuntimeException {
    public WrongFileNameException(String message) {
        super(message);
    }
}
