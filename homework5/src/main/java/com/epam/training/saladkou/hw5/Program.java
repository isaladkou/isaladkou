package com.epam.training.saladkou.hw5;

import java.util.Scanner;

public class Program {

    private static final String INSTRUCTIONS = "To select action, please type\n"
            + "root/path - to add new structure\n"
            + "print - to print structure\n"
            + "exit - to exit";

    private static final String EXIT = "exit";
    private static final String ROOT = "root";
    private static final String PRINT = "print";


    public static void main(String[] args) {

        System.out.println(INSTRUCTIONS);
        PathParser parser = new PathParser();
        Folder root = new Folder("root");
        Printer printer = new Printer();
        StructureCreator creator = new StructureCreator();
        String inputLine = "";
        while (!inputLine.equalsIgnoreCase(EXIT)) {
            Scanner scanner = new Scanner(System.in);
            inputLine = scanner.nextLine();
            if (inputLine.equalsIgnoreCase(PRINT)) {
                printer.print(root);
            } else if (inputLine.startsWith(ROOT)) {
                buildStructure(parser, creator, inputLine, root);
            } else if (inputLine.equalsIgnoreCase(EXIT)) {
                break;
            } else {
                System.out.println("Please, check your input\n" + INSTRUCTIONS);
            }
        }
    }

    private static void buildStructure(PathParser parser, StructureCreator creator,
                                       String path, Folder root) {
        parser.setPath(path);
        try {
            creator.createStructure(root, parser.getFoldersNames(), parser.getFileName());
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }
}
