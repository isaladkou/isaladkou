package com.epam.training.saladkou.hw5;

import java.util.ArrayList;
import java.util.Arrays;

public class PathParser {

    private String path;

    public void setPath(String path) {
        this.path = path;
    }


    /**
     * Method to get substring of file name from path.
     * File name goes after last slash.
     *
     * @return If path contains file name method returns it
     *         if no returns empty string.
     */
    public String getFileName() {
        int lastSlashIndex = path.lastIndexOf('/');
        String substring = path.substring(lastSlashIndex + 1);
        if (substring.contains(".")) {
            return substring;
        } else {
            return "";
        }
    }

    /**
     * Method cuts off file name from path.
     *
     * @param path - String path, contains folder and file names.
     * @return - Returns substring of path without file name.
     */
    private String cutFileName(String path) {
        String fileSubstring = getFileName();
        if (!fileSubstring.equals("")) {
            int fileNameIndex = path.indexOf(fileSubstring);
            return path.substring(0, fileNameIndex);
        } else {
            return path;
        }
    }

    /**
     * Method to get substring of path without root folder name.
     * Root folder name goes before first slash.
     *
     * @param path - String path.
     * @return Method returns substring of path without root folder name.
     */
    private String cutRoot(String path) {
        int firstSlashIndex = path.indexOf('/');
        return path.substring(firstSlashIndex + 1);
    }


    /**
     * Method to get substring of path
     * without root folder and file name.
     *
     * @return Method returns substring of path
     *         without root folder and file name
     */
    private String getFoldersString() {
        String str = cutFileName(path);
        return cutRoot(str);
    }

    /**
     * @return Method returns list of folder names from path.
     */
    public ArrayList<String> getFoldersNames() {
        String foldersString = getFoldersString();
        if (!foldersString.equals("")) {
            String[] foldersStringArray = foldersString.split("/");
            ArrayList<String> foldersList = new ArrayList<>(Arrays.asList(foldersStringArray));
            return foldersList;
        }
        return new ArrayList<>();
    }
}
