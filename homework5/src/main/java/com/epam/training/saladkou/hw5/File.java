package com.epam.training.saladkou.hw5;

public class File {

    private static final String FILE_NAME_PATTERN = "[a-zA-Z0-9]+\\.[a-zA-Z0-9]+";


    private String name;
    private String extension;


    public File(String fullName) {
        if (checkFileNameIsValid(fullName)) {
            int dotIndex = fullName.indexOf(".");
            this.name = fullName.substring(0, dotIndex);
            this.extension = fullName.substring(dotIndex + 1);
        } else {
            throw new WrongFileNameException("File full name must have file"
                    + "name, dot and extension");
        }
    }

    public String getFullName() {
        return name + "." + extension;
    }

    private boolean checkFileNameIsValid(String fileName) {
        if (fileName == null) {
            throw new IllegalArgumentException("File name can't be null");
        }
        return fileName.matches(FILE_NAME_PATTERN);
    }
}
