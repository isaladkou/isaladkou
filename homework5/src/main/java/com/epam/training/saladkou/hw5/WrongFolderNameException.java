package com.epam.training.saladkou.hw5;

public class WrongFolderNameException extends RuntimeException {
    public WrongFolderNameException(String message) {
        super(message);
    }
}
