<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order page</title>
</head>
<body>

<p>Dear, ${user.login} your order is</p>

<c:forEach items="${order.items.keySet()}" var="good">
    <c:set var="amount" value="${order.items.get(good)}"/>
    ${good.title} x${amount}<br>
</c:forEach>
<p>Total: ${order.totalSum}</p>
</body>
</html>
