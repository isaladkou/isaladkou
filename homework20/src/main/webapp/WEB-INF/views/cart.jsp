<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Cart page</title>
</head>
<body>
<p>Hello ${user.login} !</p>


<p>You have already ordered</p>

<c:forEach items="${order.items.keySet()}" var="good">
    <c:set var="amount" value="${order.items.get(good)}"/>
    ${good.title} x${amount}<br>
</c:forEach>

<form name="orderForm" method="post" action="cart">
    <p>Make your order <br></p>
    <select name="orderedItem">
        <c:forEach var="good" items="${goods}">
            <option value="${good.id}">${good}</option>
        </c:forEach>
    </select><br><br>
    <input name="act" type="submit" value="Add Item">
    <input name="act" type="submit" value="Submit">
</form>

<br><a href="history">Your order history</a>

</body>
</html>
