<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="с" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Order history</title>
</head>
<body>
<h3>Here is your order history</h3>


<с:forEach items="${history}" var="order">


    <table border="1">
        <caption>Order № ${order.id}</caption>
        <tr>
            <td>Good</td>
            <td>Price</td>
            <td>Amount</td>
            <td>Sum</td>
        </tr>


        <c:forEach items="${order.items.keySet()}" var="good">
            <c:set var="amount" value="${order.items.get(good)}"/>

            <tr>
                <td>${good.title}</td>
                <td>${good.price}</td>
                <td>${amount}</td>
                <td>${good.price*amount}</td>
            </tr>

        </c:forEach>
        <tr>
            <td>Total price</td>
            <td></td>
            <td></td>
            <td>${order.totalSum}</td>
        </tr>
        <br></table>
</с:forEach>

</body>
</html>
