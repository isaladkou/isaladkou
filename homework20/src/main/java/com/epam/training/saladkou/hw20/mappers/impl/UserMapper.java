package com.epam.training.saladkou.hw20.mappers.impl;

import com.epam.training.saladkou.hw20.entity.User;
import com.epam.training.saladkou.hw20.mappers.Mapper;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapper implements Mapper<User> {

    private static final String ID_COLUMN = "ID";
    private static final String LOGIN_COLUMN = "LOGIN";
    private static final String PASSWORD_COLUMN = "PASSWORD";

    private final Logger logger = Logger.getLogger(UserMapper.class);

    @Override
    public User mapResultSetToEntity(ResultSet resultSet) {
        User user = null;
        try {
            while (resultSet.next()) {
                user = new User(resultSet.getInt(ID_COLUMN),
                        resultSet.getString(LOGIN_COLUMN),
                        resultSet.getString(PASSWORD_COLUMN));
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return user;
    }

    @Override
    public List<User> mapResultSetToEntityList(ResultSet resultSet) {
        List<User> users = new ArrayList<>();
        try {
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt(ID_COLUMN));
                user.setLogin(resultSet.getString(LOGIN_COLUMN));
                user.setPassword(resultSet.getString(PASSWORD_COLUMN));
                users.add(user);
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        users = (users.isEmpty()) ? null : users;
        return users;
    }

}

