package com.epam.training.saladkou.hw20.repository.impl;

import com.epam.training.saladkou.hw20.entity.User;
import com.epam.training.saladkou.hw20.mappers.Mapper;
import com.epam.training.saladkou.hw20.repository.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Component
public class UserRepositoryImpl implements UserRepository {

    private static final String DATABASE_URL =
            "jdbc:h2:mem:shop_database;INIT=RUNSCRIPT FROM "
                    + "'classpath:sql/init.sql';DB_CLOSE_DELAY=-1";


    private static final String SQL_STATEMENT =
            "SELECT id, login, password FROM USER WHERE login=?";

    private static final String ADD_USER_SQL_STATEMENT =
            "INSERT INTO USER(LOGIN, PASSWORD ) VALUES ( ?,? )";

    private final Logger logger = Logger.getLogger(UserRepositoryImpl.class);

    private final Mapper<User> userMapper;

    public UserRepositoryImpl(Mapper<User> userMapper) {
        this.userMapper = userMapper;
    }

    public Optional<User> getUserByLogin(String login) {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQL_STATEMENT)) {
            preparedStatement.setString(1, login);
            logger.info("getting user " + login + " from db...");
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return getUserEntity(resultSet, login);
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return Optional.empty();
    }

    public boolean addUser(User user) {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement =
                     connection.prepareStatement(ADD_USER_SQL_STATEMENT)) {

            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());

            logger.info("adding user " + user + " to db...");

            int rowsAdded = preparedStatement.executeUpdate();
            logger.info("Row added: " + rowsAdded);

            return rowsAdded != 0;

        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return false;
    }

    private Optional<User> getUserEntity(ResultSet resultSet, String login) {
        try {
            if (!resultSet.isBeforeFirst()) {
                logger.info("adding new user...");
                return getUserByLogin(registerNewUser(login).getLogin());
            } else {
                return Optional.ofNullable(userMapper.mapResultSetToEntity(resultSet));
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return Optional.empty();
    }

    private User registerNewUser(String login) {
        User user = new User();
        user.setLogin(login);
        addUser(user);
        return user;
    }
}
