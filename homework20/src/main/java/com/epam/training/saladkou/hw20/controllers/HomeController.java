package com.epam.training.saladkou.hw20.controllers;

import com.epam.training.saladkou.hw20.repository.OrderRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = {"/", "/home"})
public class HomeController {

    private static final String HOME_PAGE = "home";

    public HomeController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    OrderRepository orderRepository;

    @GetMapping()
    public String home() {
        return HOME_PAGE;
    }


}
