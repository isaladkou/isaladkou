package com.epam.training.saladkou.hw20.mappers;

import java.sql.ResultSet;
import java.util.List;

public interface Mapper<T> {

    T mapResultSetToEntity(ResultSet resultSet);

    List<T> mapResultSetToEntityList(ResultSet resultSet);
}
