package com.epam.training.saladkou.hw20.controllers;

import com.epam.training.saladkou.hw20.entity.Order;
import com.epam.training.saladkou.hw20.entity.User;
import com.epam.training.saladkou.hw20.repository.OrderRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/order")
public class OrderController {


    private static final String ORDER_ATTRIBUTE = "order";
    private static final String ORDER_PAGE = "order";
    private static final String USER_ATTRIBUTE = "user";

    public OrderController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    private final OrderRepository orderRepository;

    @GetMapping
    public String order(HttpSession session, Model model) {

        Order order = (Order) session.getAttribute(ORDER_ATTRIBUTE);
        User user = (User) session.getAttribute(USER_ATTRIBUTE);

        orderRepository.addOrderToUser(order, user.getId());
        model.addAttribute(USER_ATTRIBUTE, user);
        model.addAttribute(ORDER_ATTRIBUTE, order);

        return ORDER_PAGE;
    }
}
