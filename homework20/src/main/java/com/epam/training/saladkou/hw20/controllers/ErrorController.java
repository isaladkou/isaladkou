package com.epam.training.saladkou.hw20.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/error")
public class ErrorController {

    private static final String ERROR_PAGE = "error";

    @GetMapping
    public String accessError() {
        return ERROR_PAGE;
    }
}
