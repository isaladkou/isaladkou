package com.epam.training.saladkou.hw20.filters;

import org.apache.log4j.Logger;


import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class AccessFilter implements Filter {

    private final Logger logger = Logger.getLogger(AccessFilter.class);
    private static final String ACCESS_PARAM = "access";
    private static final String ACCESS_ATTRIBUTE = "access";
    private static final String ERROR_PAGE_URL = "error";
    private static final String REFERER_HEADER = "referer";
    private static final String AUTH_PAGE_URL = "auth";


    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String referer = request.getHeader(REFERER_HEADER);
        logger.info(REFERER_HEADER + ": " + referer);
        if (referer != null && referer.endsWith(AUTH_PAGE_URL)) {
            handleRequestFromAuthPage(request);
        }
        Boolean isAccessAllowed = (Boolean) request.getSession().getAttribute(ACCESS_ATTRIBUTE);
        logger.info(ACCESS_ATTRIBUTE + ": " + isAccessAllowed);
        if (isAccessAllowed == null || !isAccessAllowed) {
            sendRedirectToErrorPage(response);
            return;
        }
        putToFilterChain(request, response, filterChain);
    }

    @Override
    public void destroy() {

    }

    private void handleRequestFromAuthPage(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (!session.isNew()) {
            session.invalidate();
            session = request.getSession(true);
        }
        String accessParam = request.getParameter(ACCESS_PARAM);
        boolean isAccessAllowed = (accessParam != null);
        session.setAttribute(ACCESS_ATTRIBUTE, isAccessAllowed);
    }


    private void sendRedirectToErrorPage(HttpServletResponse response) {
        try {
            response.sendRedirect(ERROR_PAGE_URL);
            logger.info("redirecting to " + ERROR_PAGE_URL);
        } catch (IOException exception) {
            logger.warn(exception.getMessage());
        }
    }

    private void putToFilterChain(ServletRequest request, ServletResponse response,
                                  FilterChain filterChain) {
        try {
            filterChain.doFilter(request, response);
        } catch (IOException | ServletException exception) {
            logger.warn(exception.getMessage());
        }
    }

}
