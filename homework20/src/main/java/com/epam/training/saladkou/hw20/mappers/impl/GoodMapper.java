package com.epam.training.saladkou.hw20.mappers.impl;

import com.epam.training.saladkou.hw20.entity.Good;
import com.epam.training.saladkou.hw20.mappers.Mapper;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class GoodMapper implements Mapper<Good> {

    private static final String ID_COLUMN = "ID";
    private static final String TITLE_COLUMN = "TITLE";
    private static final String PRICE_COLUMN = "PRICE";

    private final Logger logger = Logger.getLogger(GoodMapper.class);

    public Good mapResultSetToEntity(ResultSet resultSet) {
        Good good = null;
        try {
            while (resultSet.next()) {
                good = new Good(resultSet.getInt(ID_COLUMN),
                        resultSet.getString(TITLE_COLUMN),
                        resultSet.getDouble(PRICE_COLUMN));
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return good;
    }


    public List<Good> mapResultSetToEntityList(ResultSet resultSet) {
        List<Good> goods = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Good good = new Good(resultSet.getInt(ID_COLUMN),
                        resultSet.getString(TITLE_COLUMN),
                        resultSet.getDouble(PRICE_COLUMN));
                goods.add(good);
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        goods = (goods.isEmpty()) ? null : goods;
        return goods;
    }
}