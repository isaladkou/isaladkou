package com.epam.training.saladkou.hw20.repository;

import com.epam.training.saladkou.hw20.entity.User;

import java.util.Optional;

public interface UserRepository {
    Optional<User> getUserByLogin(String login);

    boolean addUser(User user);
}
