package com.epam.training.saladkou.hw20.controllers;

import com.epam.training.saladkou.hw20.requesthandlers.CartPageRequestHandler;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/cart")
public class CartController {


    private static final String REFERER_PARAM = "referer";
    private static final String LOGIN_PARAM = "login";
    private static final String ORDERED_ITEM_PARAM = "orderedItem";
    private static final String ACTION_PARAM = "act";
    private static final String CART_PAGE = "cart";
    private static final String AUTH_PAGE = "auth";


    public CartController(CartPageRequestHandler requestHandler) {
        this.requestHandler = requestHandler;
    }

    private final CartPageRequestHandler requestHandler;


    private final Logger logger = Logger.getLogger(CartController.class);


    @PostMapping
    public String cart(@RequestHeader(REFERER_PARAM) String referer,
                       @RequestParam(value = LOGIN_PARAM, required = false) String login,
                       @RequestParam(value = ORDERED_ITEM_PARAM, required = false) Integer goodId,
                       @RequestParam(value = ACTION_PARAM, required = false) String action,
                       HttpSession session, Model model) {

        logger.info("referer header: " + referer);

        String viewName = CART_PAGE;

        if (referer.endsWith(AUTH_PAGE)) {
            viewName = requestHandler.handleRequestFromAuthPage(login, session, model);
        }

        if (referer.endsWith(CART_PAGE)) {
            viewName = requestHandler.handleRequestFromCartPage(action, goodId, session, model);
        }
        return viewName;
    }

}
