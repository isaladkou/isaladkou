package com.epam.training.saladkou.hw20.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/auth")
public class AuthController {

    private static final String AUTH_PAGE = "auth";

    @GetMapping
    public String authShowForm() {
        return AUTH_PAGE;
    }


}
