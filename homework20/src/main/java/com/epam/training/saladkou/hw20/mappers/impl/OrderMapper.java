package com.epam.training.saladkou.hw20.mappers.impl;

import com.epam.training.saladkou.hw20.entity.Good;
import com.epam.training.saladkou.hw20.entity.Order;
import com.epam.training.saladkou.hw20.mappers.Mapper;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class OrderMapper implements Mapper<Order> {

    private static final String ORDER_ID_COLUMN = "ORDER_ID";
    private static final String GOOD_ID_COLUMN = "GOOD_ID";
    private static final String TITLE_COLUMN = "TITLE";
    private static final String PRICE_COLUMN = "PRICE";
    private static final String AMOUNT_COLUMN = "AMOUNT";

    private final Logger logger = Logger.getLogger(OrderMapper.class);


    @Override
    public Order mapResultSetToEntity(ResultSet resultSet) {
        Order order = null;
        try {
            while (resultSet.next()) {
                order = new Order(resultSet.getInt(ORDER_ID_COLUMN));
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return order;
    }

    public List<Order> mapResultSetToEntityList(ResultSet resultSet) {
        List<Order> orders = new ArrayList<>();
        try {
            while (resultSet.next()) {
                int currentId = resultSet.getInt(ORDER_ID_COLUMN);

                Good good = new Good();
                good.setId(resultSet.getInt(GOOD_ID_COLUMN));
                good.setTitle(resultSet.getString(TITLE_COLUMN));
                good.setPrice(resultSet.getDouble(PRICE_COLUMN));

                Order order = getOrder(orders, currentId);
                order.addItem(good, resultSet.getInt(AMOUNT_COLUMN));
                orders.add(order);
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return (orders.isEmpty()) ? null : orders;
    }

    private Order getOrder(List<Order> orders, int id) {
        for (Order order : orders) {
            if (order.getId() == id) {
                orders.remove(order);
                return order;
            }
        }
        return new Order(id);
    }
}
