package com.epam.training.saladkou.hw20.repository;

import com.epam.training.saladkou.hw20.entity.Good;

import java.util.Map;

public interface OrderGoodRepository {
    int insertItems(Map<Good, Integer> orderMap, int orderId);

}
