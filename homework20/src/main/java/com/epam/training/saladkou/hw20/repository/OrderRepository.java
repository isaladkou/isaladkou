package com.epam.training.saladkou.hw20.repository;

import com.epam.training.saladkou.hw20.entity.Order;

import java.util.List;
import java.util.Optional;

public interface OrderRepository {
    Optional<List<Order>> getOrdersByUserId(int userId);

    boolean addOrderToUser(Order order, int userId);

    Optional<Integer> getLastOrderId(int userId);
}
