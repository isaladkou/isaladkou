package com.epam.training.saladkou.hw20.entity;

public class Good {

    public Good(int id, String title, double price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }

    public Good() {
        this(0,"",0);
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    private int id;
    private String title;
    private double price;


    @Override
    public String toString() {
        return title + " (" + price + " $)";
    }
}
