package com.epam.training.saladkou.hw20.repository;

import com.epam.training.saladkou.hw20.entity.Good;

import java.util.List;
import java.util.Optional;

public interface GoodRepository {
    Optional<List<Good>> getAll();

    Optional<Good> getGoodById(int id);
}
