package com.epam.training.saladkou.hw20.controllers;

import com.epam.training.saladkou.hw20.entity.Order;
import com.epam.training.saladkou.hw20.entity.User;
import com.epam.training.saladkou.hw20.repository.OrderRepository;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("/history")
public class HistoryController {

    private static final String USER_ATTRIBUTE = "user";
    private static final String HISTORY_ATTRIBUTE = "history";
    private static final String HISTORY_PAGE = "history";


    public HistoryController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    OrderRepository orderRepository;

    private final Logger logger = Logger.getLogger(HistoryController.class);

    @GetMapping
    public String history(HttpSession session, Model model) {
        User user = (User) session.getAttribute(USER_ATTRIBUTE);
        Optional<List<Order>> historyOptional = orderRepository.getOrdersByUserId(user.getId());
        historyOptional.ifPresent(history -> model.addAttribute(HISTORY_ATTRIBUTE, history));
        if (historyOptional.isPresent()) {
            model.addAttribute(HISTORY_ATTRIBUTE, historyOptional.get());
        } else {
            logger.warn("Order history is empty");
        }
        return HISTORY_PAGE;
    }

}
