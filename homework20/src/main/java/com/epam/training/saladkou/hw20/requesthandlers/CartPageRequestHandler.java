package com.epam.training.saladkou.hw20.requesthandlers;

import com.epam.training.saladkou.hw20.entity.Good;
import com.epam.training.saladkou.hw20.entity.Order;
import com.epam.training.saladkou.hw20.entity.User;
import com.epam.training.saladkou.hw20.repository.GoodRepository;
import com.epam.training.saladkou.hw20.repository.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpSession;


@Component
public class CartPageRequestHandler {

    private static final String USER_ATTRIBUTE = "user";
    private static final String GOODS_ATTRIBUTE = "goods";
    private static final String ORDER_ATTRIBUTE = "order";
    private static final String SUBMIT_ACTION = "Submit";
    private static final String CART_PAGE = "cart";
    private static final String REDIRECT_TO_ORDER_PAGE = "redirect:order";


    public CartPageRequestHandler(UserRepository userRepository,
                                  GoodRepository goodRepository) {
        this.userRepository = userRepository;
        this.goodRepository = goodRepository;
    }

    private final UserRepository userRepository;
    private final GoodRepository goodRepository;

    private final Logger logger = Logger.getLogger(CartPageRequestHandler.class);


    public String handleRequestFromAuthPage(String login, HttpSession session, Model model) {
        Optional<User> userOptional = userRepository.getUserByLogin(login);
        if (userOptional.isPresent()) {
            model.addAttribute(USER_ATTRIBUTE, userOptional.get());
            session.setAttribute(USER_ATTRIBUTE, userOptional.get());
        } else {
            logger.warn("User was null");
        }
        Optional<List<Good>> goodsOptional = goodRepository.getAll();
        if (goodsOptional.isPresent()) {
            model.addAttribute(GOODS_ATTRIBUTE, goodsOptional.get());
            session.setAttribute(GOODS_ATTRIBUTE, goodsOptional.get());
        } else {
            logger.warn("Goods wasn't received from db");
        }
        session.setAttribute(ORDER_ATTRIBUTE, new Order());
        return CART_PAGE;
    }

    public String handleRequestFromCartPage(String action, int goodId,
                                            HttpSession session, Model model) {
        if (action.equals(SUBMIT_ACTION)) {
            return REDIRECT_TO_ORDER_PAGE;
        }
        model.addAttribute(USER_ATTRIBUTE, session.getAttribute(USER_ATTRIBUTE));
        model.addAttribute(GOODS_ATTRIBUTE, session.getAttribute(GOODS_ATTRIBUTE));
        addItemToOrder(goodId, session);
        return CART_PAGE;
    }

    private boolean addItemToOrder(int goodId, HttpSession session) {
        Optional<Good> good = goodRepository.getGoodById(goodId);
        if (good.isPresent()) {
            Order order = (Order) session.getAttribute(ORDER_ATTRIBUTE);
            if (order != null) {
                return order.addItem(good.get());
            }
        } else {
            logger.warn("Good wasn't received from db");
        }
        return false;
    }
}
