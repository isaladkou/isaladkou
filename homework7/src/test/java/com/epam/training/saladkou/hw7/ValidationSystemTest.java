package com.epam.training.saladkou.hw7;

import org.junit.Test;

import static org.junit.Assert.*;

public class ValidationSystemTest {


    private final Integer DEFAULT_INTEGER = 5;
    private final Integer BIG_INTEGER = 11;
    private final Integer SMALL_INTEGER = 0;

    private final String DEFAULT_STRING = "Hello";
    private final String WRONG_STRING = "hello";
    private final String EMPTY_STRING = "";

    private final Double WRONG_TYPE_ARGUMENT = 3.2;

    @Test
    public void testValidateInt() throws ValidationFailedException {
        assertTrue(ValidationSystem.validate(DEFAULT_INTEGER));
    }

    @Test(expected = ValidationFailedException.class)
    public void testValidateIntFailBigNUmber() throws ValidationFailedException {
        ValidationSystem.validate(BIG_INTEGER);
    }

    @Test(expected = ValidationFailedException.class)
    public void testValidateIntFailsSmallNumber() throws ValidationFailedException {
        ValidationSystem.validate(SMALL_INTEGER);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateIntNull() throws IllegalArgumentException, ValidationFailedException {
        ValidationSystem.validate(null);
    }

    @Test
    public void testValidateString() throws ValidationFailedException {
        assertTrue(ValidationSystem.validate(DEFAULT_STRING));
    }

    @Test(expected = ValidationFailedException.class)
    public void testValidateStringFails() throws ValidationFailedException {
        ValidationSystem.validate(WRONG_STRING);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateStringEmptyString() throws IllegalArgumentException, ValidationFailedException {
        ValidationSystem.validate(EMPTY_STRING);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateStringNull() throws IllegalArgumentException, ValidationFailedException {
        ValidationSystem.validate(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateWrongType() throws IllegalArgumentException, ValidationFailedException {
        ValidationSystem.validate(WRONG_TYPE_ARGUMENT);
    }
}