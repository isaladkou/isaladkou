package com.epam.training.saladkou.hw7;


public class StringValidator implements Validator<String> {

    private final String VALIDATION_REGEX = "^\\p{Lu}.*";

    @Override
    public boolean validate(String input) throws ValidationFailedException {
        checkInputStringIsNotEmpty(input);
        String firstLetter = input.substring(0, 1);
        if (firstLetter.matches(VALIDATION_REGEX)) {
            return true;
        } else {
            throw new ValidationFailedException("String first character was not upper case!");
        }
    }

    private boolean checkInputStringIsNotEmpty(String input) {
        if (input.equals("")) {
            throw new IllegalArgumentException("Input string can't be empty");
        }
        return true;
    }
}
