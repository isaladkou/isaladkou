package com.epam.training.saladkou.hw7;

public class IntegerValidator implements Validator<Integer> {
    @Override
    public boolean validate(Integer input) throws ValidationFailedException {
        if (input >= 1 && input <= 10) {
            return true;
        } else {
            throw new ValidationFailedException("Number was not in the given range!");
        }
    }
}
