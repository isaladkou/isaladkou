package com.epam.training.saladkou.hw7;

public final class ValidationSystem {

    private ValidationSystem() {

    }

    public static <T> boolean validate(T input) throws ValidationFailedException {
        checkArgumentIsNotNull(input);
        ValidatorFactory validatorFactory = new ValidatorFactory();
        String type = input.getClass().getSimpleName();
        Validator validator = validatorFactory.getValidator(type);
        return validator.validate(input);
    }

    private static <T> boolean checkArgumentIsNotNull(T input) {
        if (input == null) {
            throw new IllegalArgumentException("Input argument can't be null");
        }
        return true;
    }
}
