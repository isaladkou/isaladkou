package com.epam.training.saladkou.hw7;

public class ValidatorFactory {

    public Validator getValidator(String type) {
        if (type.equals("Integer")) {
            return new IntegerValidator();
        } else if (type.equals("String")) {
            return new StringValidator();
        } else {
            throw new IllegalArgumentException("There is no validator for this type");
        }
    }
}
