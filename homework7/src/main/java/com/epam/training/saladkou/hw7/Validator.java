package com.epam.training.saladkou.hw7;

public interface Validator<T> {

    boolean validate(T input) throws ValidationFailedException;
}
