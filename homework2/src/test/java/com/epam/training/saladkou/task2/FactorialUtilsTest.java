package com.epam.training.saladkou.task2;

import org.junit.Assert;
import org.junit.Test;

public class FactorialUtilsTest {

    private final int THREE = 3;
    private final int SIX = 6;
    private final int MINUS_THREE = -3;
    private final int ZERO = 0;
    private final int ONE = 1;

    @Test
    public void calculateUsingWhile() {
        Assert.assertEquals(SIX, FactorialUtils.calculateUsingWhile(THREE));
    }

    @Test
    public void calculateUsingWhileZeroParameter() {
        Assert.assertEquals(ONE, FactorialUtils.calculateUsingWhile(ZERO));
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculateUsingWhileNegativeParameter() {
        FactorialUtils.calculateUsingWhile(MINUS_THREE);
    }

    @Test
    public void calculateUsingDoWhile() {
        Assert.assertEquals(SIX, FactorialUtils.calculateUsingDoWhile(THREE));
    }

    @Test
    public void calculateUsingDoWhileZeroParameter() {
        Assert.assertEquals(ONE, FactorialUtils.calculateUsingDoWhile(ZERO));
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculateUsingDoWhileNegativeParameter() {
        FactorialUtils.calculateUsingDoWhile(MINUS_THREE);
    }

    @Test
    public void calculateUsingFor() {
        Assert.assertEquals(SIX, FactorialUtils.calculateUsingFor(THREE));
    }

    @Test
    public void calculateUsingForZeroParameter() {
        Assert.assertEquals(ONE, FactorialUtils.calculateUsingFor(ZERO));
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculateUsingForNegativeParameter() {
        FactorialUtils.calculateUsingFor(MINUS_THREE);
    }
}