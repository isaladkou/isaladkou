package com.epam.training.saladkou.task1;

import org.junit.Assert;
import org.junit.Test;

public class GFormulaUtilsTest {

    private final int THREE = 3;
    private final int TWO = 2;
    private final int ZERO = 0;
    private final double THREE_POINT_FIVE = 3.5;
    private final double MINUS_THREE_POINT_FIVE = -3.5;
    private final double FIVE_POINT_SIX = 5.6;
    private final double DEFAULT_EXPECTED = 29.283;


    @Test
    public void testCalculate() {
        Assert.assertEquals(DEFAULT_EXPECTED, GFormulaUtils.calculate(THREE, TWO,
                THREE_POINT_FIVE, FIVE_POINT_SIX), 0.001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCalculateZeroPParameter() {
        GFormulaUtils.calculate(THREE, ZERO, THREE_POINT_FIVE, FIVE_POINT_SIX);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCalculateM1M2ZeroSum() {
        GFormulaUtils.calculate(THREE, TWO, THREE_POINT_FIVE, MINUS_THREE_POINT_FIVE);
    }
}