package com.epam.training.saladkou.task2;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class FibonacciNumbersUtilsTest {

    private final int FIVE = 5;
    private final int ONE = 1;
    private final int TWO = 2;
    private final int ZERO = 0;


    private final ArrayList<Integer> FIRST_FIVE_FIBONACCI_NUMBERS =
            new ArrayList<>(Arrays.asList(0, 1, 1, 2, 3));

    private final ArrayList<Integer> FIRST_TWO_FIBONACCI_NUMBERS =
            new ArrayList<>(Arrays.asList(0, 1));

    private final ArrayList<Integer> FIRST_FIBONACCI_NUMBER =
            new ArrayList<>(Arrays.asList(0));

    @Test
    public void getNumbersUsingWhile() {
        Assert.assertEquals(FIRST_FIVE_FIBONACCI_NUMBERS,
                FibonacciNumbersUtils.getNumbersUsingWhile(FIVE));
    }

    @Test
    public void getNumbersUsingWhileFirstTwo() {
        Assert.assertEquals(FIRST_FIBONACCI_NUMBER,
                FibonacciNumbersUtils.getNumbersUsingWhile(ONE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getNumbersUsingWhileZeroAmount() {
        FibonacciNumbersUtils.getNumbersUsingWhile(ZERO);
    }

    @Test
    public void getNumbersUsingDoWhile() {
        Assert.assertEquals(FIRST_FIVE_FIBONACCI_NUMBERS,
                FibonacciNumbersUtils.getNumbersUsingDoWhile(FIVE));
    }

    @Test
    public void getNumbersUsingDoWhileFirstTwo() {
        Assert.assertEquals(FIRST_TWO_FIBONACCI_NUMBERS,
                FibonacciNumbersUtils.getNumbersUsingDoWhile(TWO));
    }

    @Test
    public void getNumbersUsingDoWhileFirst() {
        Assert.assertEquals(FIRST_FIBONACCI_NUMBER,
                FibonacciNumbersUtils.getNumbersUsingDoWhile(ONE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getNumbersUsingDoWhileZeroAmount() {
        FibonacciNumbersUtils.getNumbersUsingDoWhile(ZERO);
    }

    @Test
    public void getNumbersUsingFor() {
        Assert.assertEquals(FIRST_FIVE_FIBONACCI_NUMBERS,
                FibonacciNumbersUtils.getNumbersUsingFor(FIVE));
    }

    @Test
    public void getNumbersUsingForFirst() {
        Assert.assertEquals(FIRST_FIBONACCI_NUMBER,
                FibonacciNumbersUtils.getNumbersUsingFor(ONE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getNumbersUsingForZeroAmount() {
        FibonacciNumbersUtils.getNumbersUsingFor(ZERO);
    }
}