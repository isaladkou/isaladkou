package com.epam.training.saladkou.task1;

import org.junit.Assert;

/**
 * Class for calculating G value.
 *
 * @author Ivan Saladkou
 */
public final class GFormulaUtils {

    /**
     * Private constructor will not allow to create an instance of this class.
     */
    private GFormulaUtils() {

    }

    /**
     * @param a  - Formula parameter.
     * @param p  - Formula parameter.
     * @param m1 -  Formula parameter.
     * @param m2 -  Formula parameter.
     * @return This method returns G value.
     */
    public static double calculate(int a, int p, double m1, double m2) {
        if (p == 0) {
            throw new IllegalArgumentException("p-parameter must be non zero");
        } else if (m1 + m2 == 0) {
            throw new IllegalArgumentException("m1 + m2 must be non zero");
        } else {
            return 4 * Math.pow(Math.PI, 2) * Math.pow(a, 3)
                    / (Math.pow(p, 2) * (m1 + m2));
        }
    }
}
