package com.epam.training.saladkou.task2;

import java.util.ArrayList;

/**
 * Class for calculating fibonacci numbers.
 * Contains 3 methods for calculating.
 * Each method has it's own loop type.
 *
 * @author Ivan Saladkou
 */
public final class FibonacciNumbersUtils {

    /**
     * Private constructor will not allow to create instances of this class.
     */
    private FibonacciNumbersUtils() {

    }

    /**
     * This method calculates fibonacci numbers using "for" loop type.
     *
     * @param parameter - how many fibonacci numbers method should return.
     * @return returns arrayList of fibonacci numbers.
     */
    public static ArrayList<Integer> getNumbersUsingFor(int parameter) {
        checkParameterIsPositive(parameter);
        ArrayList<Integer> fibonacciRow = new ArrayList<>();
        fibonacciRow.add(0);
        if (parameter == 1) {
            return fibonacciRow;
        }
        fibonacciRow.add(1);
        for (int counter = 2; counter < parameter; counter++) {
            int length = fibonacciRow.size();
            int nextNum = fibonacciRow.get(length - 1) + fibonacciRow.get(length - 2);
            fibonacciRow.add(nextNum);
        }
        return fibonacciRow;
    }

    /**
     * This method calculates fibonacci numbers using "while" loop type.
     *
     * @param parameter - how many fibonacci numbers method should return.
     * @return returns arrayList of fibonacci numbers.
     */
    public static ArrayList<Integer> getNumbersUsingWhile(int parameter) {
        checkParameterIsPositive(parameter);
        ArrayList<Integer> fibonacciRow = new ArrayList<>();
        fibonacciRow.add(0);
        if (parameter == 1) {
            return fibonacciRow;
        }
        fibonacciRow.add(1);
        int counter = 2;
        while (counter < parameter) {
            int length = fibonacciRow.size();
            int nextNum = fibonacciRow.get(length - 1) + fibonacciRow.get(length - 2);
            fibonacciRow.add(nextNum);
            counter++;
        }
        return fibonacciRow;
    }

    /**
     * This method calculates fibonacci numbers using "do while" loop type.
     *
     * @param parameter - how many fibonacci numbers method should return.
     * @return returns arrayList of fibonacci numbers.
     */
    public static ArrayList<Integer> getNumbersUsingDoWhile(int parameter) {
        checkParameterIsPositive(parameter);
        ArrayList<Integer> fibonacciRow = new ArrayList<>();
        fibonacciRow.add(0);
        if (parameter == 1) {
            return fibonacciRow;
        }
        fibonacciRow.add(1);
        if (parameter == 2) {
            return fibonacciRow;
        }
        int counter = 2;
        do {
            int length = fibonacciRow.size();
            int nextNum = fibonacciRow.get(length - 1) + fibonacciRow.get(length - 2);
            fibonacciRow.add(nextNum);
            counter++;
        } while (counter < parameter);
        return fibonacciRow;
    }

    /**
     * Method to check positivity of input parameter.
     *
     * @param parameter - number we check positivity.
     * @return Returns true if parameter is positive or throws IllegalArgumentException.
     */
    private static boolean checkParameterIsPositive(int parameter) {
        if (parameter <= 0) {
            throw new IllegalArgumentException("Parameter must be positive");
        } else {
            return true;
        }
    }
}
