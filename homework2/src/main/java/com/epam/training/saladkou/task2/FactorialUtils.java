package com.epam.training.saladkou.task2;

/**
 * Class for calculating Factorial.
 * Contains 3 methods for calculating.
 * Each method has it's own loop type.
 *
 * @author Ivan Saladkou
 */
public final class FactorialUtils {

    /**
     * Private constructor will not allow to create instances of this class.
     */
    private FactorialUtils() {

    }

    /**
     * This method calculates factorial using "while" loop type.
     *
     * @param parameter - factorial of this number method should return.
     * @return returns factorial of input number.
     */
    public static int calculateUsingWhile(int parameter) {
        checkParameterIsNonNegative(parameter);
        int factorial = 1;
        int counter = 1;
        if (parameter == 0) {
            return 1;
        } else {
            while (counter != parameter + 1) {
                factorial *= counter;
                counter++;
            }
            return factorial;
        }
    }

    /**
     * This method calculates factorial using "do while" loop type.
     *
     * @param parameter - factorial of this number method should return.
     * @return returns factorial of input number.
     */
    public static int calculateUsingDoWhile(int parameter) {
        checkParameterIsNonNegative(parameter);
        int factorial = 1;
        int counter = 1;
        if (parameter == 0) {
            return 1;
        } else {
            do {
                factorial *= counter;
                counter++;
            }
            while (counter != parameter + 1);
            return factorial;
        }
    }

    /**
     * This method calculates factorial using "for" loop type.
     *
     * @param parameter - factorial of this number method should return.
     * @return returns factorial of input number.
     */
    public static int calculateUsingFor(int parameter) {
        checkParameterIsNonNegative(parameter);
        int factorial = 1;
        if (parameter == 0) {
            return 1;
        } else {
            for (int counter = 1; counter < parameter + 1; counter++) {
                factorial *= counter;
            }
            return factorial;
        }
    }

    /**
     * Method to check non-negativity of input parameter.
     *
     * @param parameter - number we check non-negativity.
     * @return Returns true if parameter is non-negative or throws IllegalArgumentException.
     */
    private static boolean checkParameterIsNonNegative(int parameter) {
        if (parameter < 0) {
            throw new IllegalArgumentException("Parameter must be greater than 0");
        } else {
            return true;
        }
    }
}
