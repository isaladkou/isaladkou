package com.epam.training.saladkou.task1;

import java.util.Scanner;

/**
 * Class for reading and validation of user's input.
 * Contains methods for console output and validation of input string.
 *
 * @author Ivan Saladkou.
 */
public class GCalculator {

    /**
     * User input instructions constant.
     *
     * @author Ivan Saladkou.
     */
    private static final String INSTRUCTIONS = "You should enter 4 arguments\n"
            + "a integer, p integer, m1 double, m2 double";

    /**
     * Program entry method.
     *
     * @author Ivan Saladkou.
     */
    public static void main(String[] args) {
        System.out.println(INSTRUCTIONS);
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] arguments = input.split(" ");
        if (arguments.length != 4) {
            System.out.println(INSTRUCTIONS);
            return;
        }
        try {
            int a = Integer.parseInt(arguments[0]);
            int p = Integer.parseInt(arguments[1]);
            double m1 = Double.parseDouble(arguments[2]);
            double m2 = Double.parseDouble(arguments[3]);
            System.out.println("G= " + GFormulaUtils.calculate(a, p, m1, m2));
        } catch (IllegalArgumentException e) {
            System.out.println("Exception! " + e.getMessage());
            return;
        }
    }
}
