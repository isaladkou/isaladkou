package com.epam.training.saladkou.task2;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class for reading and validation of user's input.
 * Contains methods for console output and validation of input string.
 * Contains logic for choosing algorithm and loop type.
 *
 * @author Ivan Saladkou.
 */
public class LoopAlgorithms {

    /**
     * User input instructions constant.
     *
     * @author Ivan Saladkou.
     */
    private static final String INSTRUCTIONS = "You should enter 3 arguments\n"
            + "To choose the algorithm enter 1 (Fibonacci) or 2(Factorial\n"
            + "To choose the loop enter 1(while), 2(do-while), 3(for)\n"
            + "input parameter should be positive, and not zero for Fibonacci algorithm";

    /**
     * Program entry method.
     * Here user enter input data using console.
     *
     * @author Ivan Saladkou.
     */
    public static void main(String[] args) {
        System.out.println("Enter algorithm type (1-2), loop type(1-3), input parameter ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] arguments = input.split(" ");
        int algorithm;
        int loop;
        int parameter;
        if (arguments.length != 3) {
            System.out.println(INSTRUCTIONS);
            return;
        }
        try {
            algorithm = Integer.parseInt(arguments[0]);
            loop = Integer.parseInt(arguments[1]);
            parameter = Integer.parseInt(arguments[2]);
        } catch (NumberFormatException e) {
            System.out.println("Exception! " + e.getMessage());
            return;
        }
        if (!checkValidInput(algorithm, loop, parameter)) {
            System.out.println(INSTRUCTIONS);
            return;
        }
        if (algorithm == 1) {
            if (loop == 1) {
                printList(FibonacciNumbersUtils.getNumbersUsingWhile(parameter));
            } else if (loop == 2) {
                printList(FibonacciNumbersUtils.getNumbersUsingDoWhile(parameter));
            } else if (loop == 3) {
                printList(FibonacciNumbersUtils.getNumbersUsingFor(parameter));
            }
        } else if (algorithm == 2) {
            if (loop == 1) {
                System.out.println(FactorialUtils.calculateUsingWhile(parameter));
            } else if (loop == 2) {
                System.out.println(FactorialUtils.calculateUsingDoWhile(parameter));
            } else if (loop == 3) {
                System.out.println(FactorialUtils.calculateUsingFor(parameter));
            }
        }
    }


    /**
     * This method prints arrayList in console without braces.
     *
     * @param list - list you need to print in console.
     */
    private static void printList(ArrayList<Integer> list) {
        StringBuilder printString = new StringBuilder();
        for (int elem : list) {
            printString.append(elem).append(" ");
        }
        System.out.println(printString);
    }

    /**
     * @param algorithm - algorithm type.
     * @param loop      - loop type.
     * @param parameter - parameter for calculating.
     * @return returns true if all of arguments is valid.
     */
    private static boolean checkValidInput(int algorithm, int loop, int parameter) {
        return algorithm >= 1 && algorithm <= 2
                && loop >= 1 && loop <= 3
                && !(algorithm == 1 && parameter == 0)
                && parameter >= 0;
    }
}

