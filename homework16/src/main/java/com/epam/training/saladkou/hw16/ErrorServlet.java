package com.epam.training.saladkou.hw16;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "Error", urlPatterns = "/error")
public class ErrorServlet extends HttpServlet {

    private Logger logger = Logger.getLogger(ErrorServlet.class);

    private final String MARKUP = "<html>"
            + "<head>"
            + "<title>Error page</title>"
            + "</head>"
            + "<body>"
            + "<h1>Oops!</h1>"
            + "<p>You shouldn't be here</p>"
            + "<p>Please, agree with the terms of service first</p>"
            + "<br>"
            + "<a href =\"auth\">Start page</a>"
            + "</body>"
            + "</html>";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse response) {
        try (PrintWriter writer = response.getWriter()) {
            writer.println(MARKUP);
        } catch (IOException exception) {
            logger.warn(exception.getMessage());
        }
    }
}
