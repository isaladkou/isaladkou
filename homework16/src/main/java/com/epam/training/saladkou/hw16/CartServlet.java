package com.epam.training.saladkou.hw16;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "Cart", urlPatterns = "/cart")
public class CartServlet extends HttpServlet {


    private Logger logger = Logger.getLogger(CartServlet.class);

    private final String DEFAULT_USER_NAME = "User";


    private String getOptions() {
        StringBuilder options = new StringBuilder();
        List<Product> catalog = ProductsCatalog.getInstance().getCatalog();
        for (Product product : catalog) {
            options.append("<option value =").append(product.getId()).append(">")
                    .append(product).append("</option>");
        }
        return options.toString();
    }

    private String getUserName(HttpServletRequest request) {
        String name = (String) request.getSession().getAttribute("userName");
        if (name.equals("")) {
            name = DEFAULT_USER_NAME;
        }
        return name;
    }

    private void printMarkup(String name, Map<Product, Integer> order,
                             HttpServletResponse response) {
        OrderPrinter orderPrinter = new OrderPrinter();
        try (PrintWriter writer = response.getWriter()) {
            writer.println("<html>"
                    + "<head>"
                    + "<title>Cart page</title>"
                    + "</head>"
                    + "<body>"
                    + "<p>Hello " + name + "!</p>"
                    + orderPrinter.convertOrderToString(order)
                    + "<form name=\"orderForm\" method=\"post\" action=\"cart\"  >"
                    + "<p>Make your order <br></p>"
                    + "<select size =\"1\" name =\"orderedItem\">"
                    + getOptions()
                    + "</select><br><br>"
                    + "<input name =\"act\" type=\"submit\" value=\"Add Item\">"
                    + "<input name =\"act\"  type=\"submit\" value=\"Submit\">"
                    + "</form>"
                    + "</body>"
                    + "</html>");
        } catch (IOException exception) {
            logger.warn(exception.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String name = getUserName(request);
        Map<Product, Integer> order = (Map<Product, Integer>) request.getSession()
                .getAttribute("orderMap");
        printMarkup(name, order, response);
    }
}