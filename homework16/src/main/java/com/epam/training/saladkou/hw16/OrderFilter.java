package com.epam.training.saladkou.hw16;


import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class OrderFilter implements Filter {

    private final String CART_PAGE_URL = "http://localhost:8888/cart";
    private final String ORDER_ATTRIBUTE_TITLE = "orderMap";
    private final String ACTION_PARAMETER_TITLE = "act";
    private final String ORDERED_ITEM_PARAMETER_TITLE = "orderedItem";
    private final String ADD_ITEM_ACTION = "Add Item";
    private final String REFERER_HEADER = "referer";

    private Logger logger = Logger.getLogger(OrderFilter.class);

    private final ProductsCatalog catalog = ProductsCatalog.getInstance();

    private boolean checkIsAddButtonClicked(HttpServletRequest request) {
        return request.getParameter(ACTION_PARAMETER_TITLE).equals(ADD_ITEM_ACTION);
    }

    private Map<Product, Integer> getOrderMap(HttpSession session) {
        if (session.getAttribute(ORDER_ATTRIBUTE_TITLE) == null) {
            session.setAttribute(ORDER_ATTRIBUTE_TITLE, new HashMap<Product, Integer>());
        }
        return (Map<Product, Integer>) session.getAttribute(ORDER_ATTRIBUTE_TITLE);
    }

    private boolean putItemToOrder(Map<Product, Integer> order, Product item) {
        Product product = getProductFromOrderById(order, item.getId());
        if (product == null) {
            order.put(item, 1);
        } else {
            int amount = order.get(product);
            order.put(product, ++amount);
        }
        return product == null;
    }

    private Product getProductFromOrderById(Map<Product, Integer> order, Integer id) {
        for (Product product : order.keySet()) {
            if (product.getId() == id) {
                return product;
            }
        }
        return null;
    }

    private boolean refreshOrder(HttpServletRequest request) {
        HttpSession session = request.getSession();
        int itemId = Integer.parseInt(request.getParameter(ORDERED_ITEM_PARAMETER_TITLE));
        Product item = catalog.getProductById(itemId);
        Map<Product, Integer> order = getOrderMap(session);
        return putItemToOrder(order, item);
    }

    private void resendToOrderPage(HttpServletRequest request, ServletResponse servletResponse) {
        RequestDispatcher dispatcher = request.getRequestDispatcher("order");
        try {
            dispatcher.forward(request, servletResponse);
        } catch (ServletException | IOException exception) {
            logger.warn(exception.getMessage());
        }
    }

    private void putToFilterChain(HttpServletRequest request,
                                  ServletResponse servletResponse, FilterChain filterChain) {
        try {
            filterChain.doFilter(request, servletResponse);
        } catch (ServletException | IOException exception) {
            logger.warn(exception.getMessage());
        }
    }


    private void handleRequest(HttpServletRequest request,
                               ServletResponse servletResponse, FilterChain filterChain) {
        if (checkIsAddButtonClicked(request)) {
            if (request.getParameter(ORDERED_ITEM_PARAMETER_TITLE) != null) {
                refreshOrder(request);
            }
            putToFilterChain(request, servletResponse, filterChain);
        } else {
            resendToOrderPage(request, servletResponse);
        }
    }


    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        if (request.getHeader(REFERER_HEADER).equals(CART_PAGE_URL)) {
            handleRequest(request, servletResponse, filterChain);
        }
        putToFilterChain(request, servletResponse, filterChain);
    }

    @Override
    public void destroy() {

    }
}
