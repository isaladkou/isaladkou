package com.epam.training.saladkou.hw16;

import java.util.Map;

public class OrderPrinter {
    public String convertOrderToString(Map<Product, Integer> products) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<table>");
        int index = 1;
        if (products != null) {
            for (Product product : products.keySet()) {
                int amount = products.get(product);
                stringBuilder.append("<tr><td>").append(index).append(") </td>");
                stringBuilder.append("<td>").append(product.getTitle()).append("</td>");
                stringBuilder.append("<td> x").append(amount).append("</td>");
                stringBuilder.append("<td>")
                        .append(product.getCost() * amount).append(" $</tr></td>");
                index++;
            }
        }

        stringBuilder.append("</table>");
        return stringBuilder.toString();
    }
}
