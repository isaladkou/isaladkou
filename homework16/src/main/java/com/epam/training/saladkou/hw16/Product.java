package com.epam.training.saladkou.hw16;

public class Product {

    public String getTitle() {
        return title;
    }

    public double getCost() {
        return cost;
    }

    private int id;

    public int getId() {
        return id;
    }

    private String title;
    private double cost;

    public Product(int id, String title, double cost) {
        this.id = id;
        this.title = title;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return title + " (" + cost + " $)";
    }
}
