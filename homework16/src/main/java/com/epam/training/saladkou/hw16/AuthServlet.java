package com.epam.training.saladkou.hw16;


import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "Authorization", urlPatterns = "/auth")
public class AuthServlet extends HttpServlet {

    private Logger logger = Logger.getLogger(AuthServlet.class);

    private final String MARKUP = "<html>"
            + "<head>"
            + "<title>Authorization page</title>"
            + "</head>"
            + "<body>"
            + "<p>Welcome to Online Shop</p>"
            + "<form name=\"test\" method=\"post\" action=\"cart\"  >"
            + "<input type=\"text\" placeholder=\"Enter your name\" name = \"name\">"
            + "<br>"
            + "<input type=\"checkbox\"  name = \"termsCheckbox\">"
            + "<label>I agree with the terms of service</label>"
            + "<br><br>"
            + "<input type=\"submit\" value=\"Enter\">"
            + "</form>"
            + "</body>"
            + "</html>";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        try (PrintWriter writer = resp.getWriter()) {
            writer.println(MARKUP);
        } catch (IOException exception) {
            logger.warn(exception.getMessage());
        }
    }
}
