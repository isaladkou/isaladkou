package com.epam.training.saladkou.hw16;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "Order", urlPatterns = "/order")
public class OrderServlet extends HttpServlet {

    private Logger logger = Logger.getLogger(OrderServlet.class);


    private double getTotalSum(Map<Product, Integer> products) {
        double sum = 0;
        for (Product product : products.keySet()) {
            int amount = products.get(product);
            sum += product.getCost() * amount;
        }
        return sum;
    }

    private void printMarkup(HttpServletResponse response, String name, String table, Double sum) {
        try (PrintWriter writer = response.getWriter()) {
            writer.println("<html>"
                    + "<head>"
                    + "<title>Order page</title>"
                    + "</head>"
                    + "<body>"
                    + "<p>Dear " + name + ", your order:</p>"
                    + table
                    + "<p>Total: $" + sum + " </p>"
                    + "</body>"
                    + "</html>");
        } catch (IOException exception) {
            logger.warn(exception.getMessage());
        }


    }

    private void printEmptyOrderMarkup(HttpServletResponse response, String name) {
        try (PrintWriter writer = response.getWriter()) {
            writer.println("<html>"
                    + "<head>"
                    + "<title>Order page</title>"
                    + "</head>"
                    + "<body>"
                    + "<p>Dear " + name + ", your order is empty</p>"
                    + "</body>"
                    + "</html>");
        } catch (IOException exception) {
            logger.warn(exception.getMessage());
        }

    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        Map<Product, Integer> order = (Map<Product, Integer>) session.getAttribute("orderMap");
        String name = (String) session.getAttribute("userName");
        OrderPrinter orderPrinter = new OrderPrinter();
        if (order != null && !order.isEmpty()) {
            printMarkup(response, name, orderPrinter.convertOrderToString(order),
                    getTotalSum(order));
        } else {
            printEmptyOrderMarkup(response, name);
        }
    }

}
