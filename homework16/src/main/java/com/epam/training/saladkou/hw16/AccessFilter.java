package com.epam.training.saladkou.hw16;

import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class AccessFilter implements Filter {

    private Logger logger = Logger.getLogger(AccessFilter.class);

    private final String AUTH_PAGE_URL = "http://localhost:8888/auth";

    private void handleRequestFromAuthPage(HttpServletRequest request,
                                           HttpServletResponse response) {
        HttpSession session = request.getSession();
        if (!session.isNew()) {
            session.invalidate();
            session = request.getSession(true);
        }
        String accessParam = request.getParameter("termsCheckbox");
        String nameParam = request.getParameter("name");

        boolean isAccessAllowed = (accessParam != null);
        session.setAttribute("isAccessAllowed", isAccessAllowed);
        session.setAttribute("userName", nameParam);
    }

    private void sendRedirectToErrorPage(HttpServletResponse response) {
        try {
            response.sendRedirect("error");
        } catch (IOException exception) {
            logger.warn(exception.getMessage());
        }
    }

    private void putToFilterChain(HttpServletRequest request, HttpServletResponse response,
                                  FilterChain filterChain) {
        try {
            filterChain.doFilter(request, response);
        } catch (IOException | ServletException exception) {
            logger.warn(exception.getMessage());
        }
    }


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        logger.info(request.getHeader("referer"));
        if (request.getHeader("referer").equals(AUTH_PAGE_URL)) {
            handleRequestFromAuthPage(request, response);
        }

        Boolean isAccessAllowed = (Boolean) request.getSession().getAttribute("isAccessAllowed");
        if (isAccessAllowed == null || !isAccessAllowed) {
            sendRedirectToErrorPage(response);
        }
        putToFilterChain(request, response, filterChain);
    }

    @Override
    public void destroy() {

    }
}
