package com.epam.training.saladkou.hw16;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductsCatalog {

    private ProductsCatalog() {
        this.catalog = new ArrayList<>(
                Arrays.asList(new Product(1, "Pants", 5),
                        new Product(2, "Black Hat", 3),
                        new Product(3, "Shoes", 12.3)));
    }

    private static ProductsCatalog instance;

    public static ProductsCatalog getInstance() {
        if (instance == null) {
            instance = new ProductsCatalog();
        }
        return instance;
    }


    public List<Product> getCatalog() {
        return catalog;
    }

    private List<Product> catalog;

    public Product getProductById(int id) {
        Product requested = null;
        for (Product product : catalog) {
            if (product.getId() == id) {
                requested = product;
            }
        }
        return requested;
    }
}
