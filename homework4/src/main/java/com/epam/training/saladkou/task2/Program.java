package com.epam.training.saladkou.task2;

import java.util.Arrays;

public class Program {
    public static void main(String[] args) {

        int[] array = {12, 7, 5, 9, 1, 2, 4, 5};
        SortingContext context = new SortingContext(new BubbleSort());
        System.out.println("Bubble sort");
        context.execute(array);
        System.out.println(Arrays.toString(array));

        int[] array2 = {14, 41, 5, 12, 1, 9, 7, 4};
        context = new SortingContext(new SelectionSort());
        System.out.println("Selection sort");
        context.execute(array2);
        System.out.println(Arrays.toString(array2));

    }
}

