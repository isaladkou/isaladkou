package com.epam.training.saladkou.task1.cards;

import java.math.BigDecimal;

public class CreditCard extends AbstractCard {
    public CreditCard(String ownerName, BigDecimal balance) {
        super(ownerName, balance);
    }

    @Override
    public boolean withdraw(BigDecimal sum) {
        checkSumIsValid(sum);
        setBalance(getBalance().subtract(sum));
        return true;
    }
}
