package com.epam.training.saladkou.task1.exceptions;

public class NoCashException extends RuntimeException {

    public NoCashException(String message) {
        super(message);
    }
}
