package com.epam.training.saladkou.task2;

public class SelectionSort implements Sorter {

    public int[] sort(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Array can't be null");
        } else if (array.length < 2) {
            return array;
        }
        int length = array.length;
        for (int i = 0; i < length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
                int temp = array[minIndex];
                array[minIndex] = array[i];
                array[i] = temp;
            }
        }
        return array;
    }
}
