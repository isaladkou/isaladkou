package com.epam.training.saladkou.task1.exceptions;

public class CrowdedAtmException extends RuntimeException {

    public CrowdedAtmException(String message) {
        super(message);
    }
}
