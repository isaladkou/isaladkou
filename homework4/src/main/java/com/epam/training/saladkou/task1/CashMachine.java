package com.epam.training.saladkou.task1;

import com.epam.training.saladkou.task1.cards.AbstractCard;
import com.epam.training.saladkou.task1.exceptions.CrowdedAtmException;
import com.epam.training.saladkou.task1.exceptions.NoCashException;

import java.math.BigDecimal;

public class CashMachine {

    private BigDecimal capacity;
    private BigDecimal moneyAmount;


    public BigDecimal getCapacity() {
        return capacity;
    }

    public BigDecimal getMoneyAmount() {
        return moneyAmount;
    }

    public CashMachine(BigDecimal capacity, BigDecimal moneyAmount) {
        checkIsCapacityValid(capacity);
        setCapacity(capacity);
        checkIsMoneyAmountValid(moneyAmount);
        setMoneyAmount(moneyAmount);
    }


    private void setMoneyAmount(BigDecimal moneyAmount) {
        checkIsMoneyAmountValid(moneyAmount);
        this.moneyAmount = moneyAmount;
    }

    public void setCapacity(BigDecimal capacity) {
        checkIsCapacityValid(capacity);
        this.capacity = capacity;
    }

    public boolean replenishAccount(AbstractCard card, BigDecimal sum) {
        checkIsOperationParametersValid(card, sum);
        if (getDifference().compareTo(sum) >= 0) {
            card.putMoneyOn(sum);
            setMoneyAmount(getMoneyAmount().add(sum));
            return true;
        } else {
            throw new CrowdedAtmException("ATM is crowded");
        }
    }

    public boolean withdrawMoney(AbstractCard card, BigDecimal sum) {
        checkIsOperationParametersValid(card, sum);
        if (getMoneyAmount().compareTo(sum) >= 0) {
            card.withdraw(sum);
            setMoneyAmount(getMoneyAmount().subtract(sum));
            return true;
        } else {
            throw new NoCashException("No cash in ATM");
        }
    }

    /**
     * Method calculates how much money we can put in ATM.
     *
     * @return Method returns difference
     * between ATM capacity and money amount.
     */
    private BigDecimal getDifference() {
        return getCapacity().subtract(getMoneyAmount());
    }

    private boolean checkIsOperationParametersValid(AbstractCard card, BigDecimal sum) {
        if (card == null) {
            throw new IllegalArgumentException("Card can't be null");
        } else if (sum == null) {
            throw new IllegalArgumentException("Sum can't be null");
        }
        return true;
    }

    private boolean checkIsCapacityValid(BigDecimal capacity) {
        if (capacity.signum() <= 0) {
            throw new IllegalArgumentException("ATM capacity can't be non positive");
        }
        return true;
    }

    private boolean checkIsMoneyAmountValid(BigDecimal moneyAmount) {
        if (moneyAmount.signum() < 0) {
            throw new IllegalArgumentException("Money amount can't be negative");
        } else if (moneyAmount.compareTo(this.capacity) > 0) {
            throw new IllegalArgumentException("Money amount can't bigger than ATM capacity");
        }
        return true;
    }
}