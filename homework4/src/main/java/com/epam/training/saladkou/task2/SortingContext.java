package com.epam.training.saladkou.task2;

public class SortingContext {
    private Sorter sorter;

    public SortingContext(Sorter sorter) {
        if (sorter == null) {
            throw new IllegalArgumentException("Sorter can't be null");
        } else {
            this.sorter = sorter;
        }
    }

    public boolean execute(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Array can't be null");
        } else {
            sorter.sort(array);
            return true;
        }
    }
}
