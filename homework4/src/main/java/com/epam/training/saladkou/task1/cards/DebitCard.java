package com.epam.training.saladkou.task1.cards;

import com.epam.training.saladkou.task1.exceptions.InsufficientFundsException;

import java.math.BigDecimal;

public class DebitCard extends AbstractCard {
    public DebitCard(String ownerName, BigDecimal balance) {
        checkCardParametersIsValid(ownerName, balance);
        checkBalanceIsNotNegative(balance);
        setOwnerName(ownerName);
        setBalance(balance);
    }

    @Override
    public boolean withdraw(BigDecimal sum) {
        checkSumIsValid(sum);
        checkSumIsNotGreatThanBalance(sum);
        setBalance(getBalance().subtract(sum));
        return true;
    }


    private boolean checkBalanceIsNotNegative(BigDecimal balance) {
        if (balance.signum() == -1) {
            throw new IllegalArgumentException("Debit card"
                    + "can't have negative balance");
        }
        return true;
    }

    private boolean checkSumIsNotGreatThanBalance(BigDecimal sum) {
        if (getBalance().compareTo(sum) < 0) {
            throw new InsufficientFundsException("Attention, insufficient funds!");
        }
        return true;
    }

}
