package com.epam.training.saladkou.task2;

public interface Sorter {
    int[] sort(int[] array);
}
