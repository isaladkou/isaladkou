package com.epam.training.saladkou.task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SortingContextTest {

    private int[] defaultArray;
    private BubbleSort defaultSorter;
    private SortingContext sortingContext;

    @Before
    public void initialize() {
        defaultArray = new int[]{5, 3, 7, 9, 55, 2, 1, 12, -4};
        defaultSorter = new BubbleSort();
        sortingContext = new SortingContext(defaultSorter);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testCreateSortingContextWithNullSorter() throws IllegalArgumentException {
        sortingContext = new SortingContext(null);
    }

    @Test
    public void testExecute() {
        Assert.assertTrue(sortingContext.execute(defaultArray));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteNull() throws IllegalArgumentException {
        sortingContext.execute(null);
    }
}