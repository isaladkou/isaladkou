package com.epam.training.saladkou.task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class SelectionSortTest {

    private int[] defaultArray;
    private int[] sortedArray;
    private int[] emptyArray;
    private BubbleSort bubbleSort;


    @Before
    public void initialize() {
        defaultArray = new int[]{5, 3, 7, 9, 55, 2, 1, 12, -4};
        sortedArray = new int[]{-4, 1, 2, 3, 5, 7, 9, 12, 55};
        emptyArray = new int[0];
        bubbleSort = new BubbleSort();
    }

    private SelectionSort selectionSort = new SelectionSort();

    @Test
    public void testSort() {
        assertArrayEquals(selectionSort.sort(defaultArray), sortedArray);
    }

    @Test
    public void testSortEmptyArray() {
        assertArrayEquals(selectionSort.sort(emptyArray), emptyArray);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSortNullArray() throws IllegalArgumentException {
        selectionSort.sort(null);
    }
}