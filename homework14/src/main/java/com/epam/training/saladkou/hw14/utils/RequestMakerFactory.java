package com.epam.training.saladkou.hw14.utils;

import com.epam.training.saladkou.hw14.Program;
import com.epam.training.saladkou.hw14.requestmakers.ApacheRequestMaker;
import com.epam.training.saladkou.hw14.requestmakers.RequestMaker;
import com.epam.training.saladkou.hw14.requestmakers.StandardRequestMaker;
import org.apache.log4j.Logger;

public class RequestMakerFactory {

    private static Logger logger = Logger.getLogger(Program.class);


    public RequestMaker getRequestMaker(String parameter) {
        int parameterInt = Integer.parseInt(parameter);
        if (parameterInt == 1) {
            return new StandardRequestMaker();
        } else if (parameterInt == 2) {
            return new ApacheRequestMaker();
        } else {
            logger.warn("Request maker was null");
            return null;
        }
    }
}
