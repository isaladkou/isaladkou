package com.epam.training.saladkou.hw14.utils;

import com.epam.training.saladkou.hw14.entity.Post;

public class ResponsePrinter {

    private final String POST_METHOD = "POST";
    private final String GET_METHOD = "GET";


    public void printMessage(String method, Post post) {
        if (method.equals(GET_METHOD)) {
            printSuccessGetMessage(post);
        } else if (method.equals(POST_METHOD)) {
            printSuccessPostMessage(post);
        }
    }

    private void printSuccessPostMessage(Post post) {
        checkIsPostNull(post);
        System.out.println("Article [" + post.getId() + "] has been created:"
                + "User [" + post.getUserId() + "] Title [" + post.getTitle()
                + "] Message [" + post.getBody() + "]");
    }

    private void printSuccessGetMessage(Post post) {
        checkIsPostNull(post);
        System.out.println(post);
    }

    private boolean checkIsPostNull(Post post) {
        if (post == null) {
            throw new IllegalArgumentException("Response was null");
        }
        return true;
    }
}
