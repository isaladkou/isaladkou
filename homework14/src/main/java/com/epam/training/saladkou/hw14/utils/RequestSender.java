package com.epam.training.saladkou.hw14.utils;

import com.epam.training.saladkou.hw14.entity.Post;
import com.epam.training.saladkou.hw14.requestmakers.RequestMaker;

public class RequestSender {

    public Post sendRequest(RequestMaker requestMaker, String methodName, int id) {
        Post response = null;
        if (methodName.equals("GET")) {
            response = requestMaker.sendGet(id);
        } else if (methodName.equals("POST")) {
            response = requestMaker.sendPost(new Post(id));
        }
        return response;
    }
}
