package com.epam.training.saladkou.hw14.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InputParser {

    private final String SEPARATOR = " ";

    private boolean checkInputValid(String input) {
        if (input == null || !(input.contains(SEPARATOR))) {
            throw new IllegalArgumentException("Illegal input line");
        }
        return true;
    }

    public Map<String, String> getParametersMap(String input) {
        checkInputValid(input);
        List<String> parametersList = Arrays.asList(input.split(SEPARATOR));
        Map<String, String> parameters = new HashMap<>();
        parameters.put("solution", parametersList.get(0));
        parameters.put("method", parametersList.get(1));
        parameters.put("id", parametersList.get(2));
        return parameters;
    }

}
