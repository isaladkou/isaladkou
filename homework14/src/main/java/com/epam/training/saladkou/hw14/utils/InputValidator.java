package com.epam.training.saladkou.hw14.utils;


import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class InputValidator {

    private Logger logger = Logger.getLogger(InputValidator.class);

    private final int PARAMETERS_AMOUNT = 3;
    private final List<String> SOLUTION_PARAMETER_RANGE =
            new ArrayList<>(Arrays.asList("1", "2"));
    private final List<String> METHOD_PARAMETER_RANGE =
            new ArrayList<>(Arrays.asList("GET", "POST"));

    public boolean validate(Map<String, String> parameters) {
        checkIsAnyParameterNull(parameters);
        checkArgumentsAmount(new ArrayList<>(parameters.values()));
        checkIsMethodParameterValid(parameters.get("method"));
        checkIsSolutionParameterValid(parameters.get("solution"));
        checkIsIdParameterValid(parameters.get("id"));
        return true;
    }

    private boolean checkArgumentsAmount(List<String> arguments) {
        if (PARAMETERS_AMOUNT != arguments.size()) {
            logger.warn("Illegal arguments amount");
            throw new IllegalArgumentException("Illegal arguments amount");
        }
        return true;
    }

    private boolean checkIsSolutionParameterValid(String parameter) {
        if (!SOLUTION_PARAMETER_RANGE.contains(parameter)) {
            logger.warn("Illegal solution parameter");
            throw new IllegalArgumentException("Illegal solution parameter");
        }
        return true;
    }

    private boolean checkIsMethodParameterValid(String method) {
        if (!METHOD_PARAMETER_RANGE.contains(method)) {
            logger.warn("Illegal method parameter");
            throw new IllegalArgumentException("Illegal method parameter");
        }
        return true;
    }

    private boolean checkIsIdParameterValid(String idParameter) {
        int id = Integer.parseInt(idParameter);
        if (id < 0) {
            logger.warn("Illegal id parameter");
            throw new IllegalArgumentException("Illegal id parameter");
        }
        return true;
    }

    private boolean checkIsAnyParameterNull(Map<String, String> parameters) {
        if (parameters.containsValue(null)) {
            logger.warn("Some parameter was null");
            throw new IllegalArgumentException("No one parameter could be null");
        }
        return true;
    }
}
