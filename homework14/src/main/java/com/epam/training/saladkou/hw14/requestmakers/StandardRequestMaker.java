package com.epam.training.saladkou.hw14.requestmakers;

import com.epam.training.saladkou.hw14.entity.Post;
import com.epam.training.saladkou.hw14.exceptions.ConnectionException;
import com.epam.training.saladkou.hw14.exceptions.RequestException;
import com.epam.training.saladkou.hw14.exceptions.StreamException;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class StandardRequestMaker implements RequestMaker {

    @Override
    public Post sendGet(int postId) {
        try {
            URL url = new URL(ADDRESS + "/" + postId);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.disconnect();
            return handleResponse(connection);
        } catch (MalformedURLException exception) {
            throw new IllegalArgumentException("Illegal URL");
        } catch (ProtocolException exception) {
            throw new RequestException(exception);
        } catch (IOException exception) {
            throw new ConnectionException("Exception while opening connection", exception);
        }
    }

    @Override
    public Post sendPost(Post post) {
        try {
            URL url = new URL(ADDRESS);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            writeObjectToStream(post, connection.getOutputStream());
            connection.disconnect();
            checkStatusCodeIsSuccess(connection.getResponseCode());
            return mapStreamToObject(connection.getInputStream());
        } catch (MalformedURLException exception) {
            throw new IllegalArgumentException("Illegal URL");
        } catch (ProtocolException exception) {
            throw new RequestException(exception);
        } catch (IOException exception) {
            throw new ConnectionException("Exception while referring to connection", exception);
        }
    }

    private OutputStream writeObjectToStream(Post post, OutputStream stream) {
        DataOutputStream outputStream = new DataOutputStream(stream);
        try {
            outputStream.writeBytes(getParametersString(post));
            outputStream.flush();
            outputStream.close();
        } catch (IOException exception) {
            throw new StreamException("Exception while referring to output stream", exception);
        }
        return outputStream;
    }

    private Post handleResponse(HttpURLConnection connection) {
        try {
            checkStatusCodeIsSuccess(connection.getResponseCode());
            return mapStreamToObject(connection.getInputStream());
        } catch (IOException exception) {
            throw new ConnectionException("Exception while referring to connection", exception);
        }
    }
}
