package com.epam.training.saladkou.hw14;

import com.epam.training.saladkou.hw14.entity.Post;
import com.epam.training.saladkou.hw14.requestmakers.RequestMaker;
import com.epam.training.saladkou.hw14.utils.ParametersReceiver;
import com.epam.training.saladkou.hw14.utils.RequestMakerFactory;
import com.epam.training.saladkou.hw14.utils.RequestSender;
import com.epam.training.saladkou.hw14.utils.ResponsePrinter;
import org.apache.log4j.Logger;

import java.util.Map;

public class Program {

    private static Logger logger = Logger.getLogger(Program.class);

    public static void main(String[] args) {
        Map<String, String> parameters;
        RequestMaker requestMaker;
        try {
            parameters = new ParametersReceiver().getRequestParameters();
            requestMaker = new RequestMakerFactory().getRequestMaker(parameters.get("solution"));
            Post response = new RequestSender().sendRequest(requestMaker, parameters.get("method"),
                    Integer.parseInt(parameters.get("id")));
            new ResponsePrinter().printMessage(parameters.get("method"), response);
        } catch (RuntimeException exception) {
            logger.warn(exception.getMessage());
        }
    }
}
