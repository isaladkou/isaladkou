package com.epam.training.saladkou.hw14.exceptions;

public class MappingFailedException extends RuntimeException {
    public MappingFailedException(Exception cause) {
        super("Exception while mapping stream to object", cause);
    }

}
