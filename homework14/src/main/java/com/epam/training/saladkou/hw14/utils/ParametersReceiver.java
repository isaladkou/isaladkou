package com.epam.training.saladkou.hw14.utils;

import java.util.Map;
import java.util.Scanner;

public class ParametersReceiver {
    public Map<String, String> getRequestParameters() {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        scanner.close();
        InputParser inputParser = new InputParser();
        Map<String, String> parameters = inputParser.getParametersMap(input);
        InputValidator inputValidator = new InputValidator();
        inputValidator.validate(parameters);
        return parameters;
    }
}
