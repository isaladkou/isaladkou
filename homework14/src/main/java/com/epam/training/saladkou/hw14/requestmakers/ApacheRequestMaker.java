package com.epam.training.saladkou.hw14.requestmakers;

import com.epam.training.saladkou.hw14.entity.Post;
import com.epam.training.saladkou.hw14.exceptions.RequestException;
import com.epam.training.saladkou.hw14.exceptions.ResponseException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class ApacheRequestMaker implements RequestMaker {

    public ApacheRequestMaker(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public ApacheRequestMaker() {
        this.httpClient = HttpClients.createDefault();
    }

    private HttpClient httpClient;

    @Override
    public Post sendGet(int postId) {
        HttpGet getRequest = new HttpGet(ADDRESS + "/" + postId);
        HttpResponse response;
        try {
            response = httpClient.execute(getRequest);
        } catch (ClientProtocolException exception) {
            throw new ResponseException(exception);
        } catch (IOException exception) {
            throw new ResponseException(exception);
        }
        return handleResponse(response);
    }

    @Override
    public Post sendPost(Post post) {
        HttpPost postRequest = new HttpPost(ADDRESS);
        HttpResponse response;
        try {
            postRequest.setEntity(new StringEntity(getParametersString(post)));
            response = httpClient.execute(postRequest);
            return handleResponse(response);
        } catch (UnsupportedEncodingException exception) {
            throw new RequestException(exception);
        } catch (ClientProtocolException exception) {
            throw new ResponseException(exception);
        } catch (IOException exception) {
            throw new ResponseException(exception);
        }
    }

    private Post handleResponse(HttpResponse response) {
        checkStatusCodeIsSuccess(response.getStatusLine().getStatusCode());
        try {
            return mapStreamToObject(response.getEntity().getContent());
        } catch (IOException exception) {
            throw new ResponseException("Exception while getting content from response");
        }
    }
}
