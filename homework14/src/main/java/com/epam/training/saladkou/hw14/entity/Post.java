package com.epam.training.saladkou.hw14.entity;

import java.util.Objects;

public class Post {

    private final int DEFAULT_USER_ID = 1;
    private final int DEFAULT_ID = 1;
    private final String DEFAULT_TITLE = "default title";
    private final String DEFAULT_BODY = "default body";

    private int userId;
    private int id;
    private String title;
    private String body;

    public Post() {
        this.id = DEFAULT_ID;
        this.userId = DEFAULT_USER_ID;
        this.title = DEFAULT_TITLE;
        this.body = DEFAULT_BODY;
    }

    public Post(int id) {
        this.id = id;
        this.userId = DEFAULT_USER_ID;
        this.title = DEFAULT_TITLE;
        this.body = DEFAULT_BODY;
    }

    public Post(int userId, int id, String title, String body) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
    }

    public int getUserId() {
        return userId;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Post post = (Post) other;
        return this.userId == post.userId
                && this.id == post.id
                && title.equals(post.title)
                && body.equals(post.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, id, title, body);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Article [").append(this.id).append("]: ");
        stringBuilder.append("User [").append(this.userId).append("]: ");
        stringBuilder.append("Title [").append(this.title).append("]: ");
        stringBuilder.append("Message [").append(this.body).append("]:");
        return stringBuilder.toString();
    }
}
