package com.epam.training.saladkou.hw14.exceptions;

public class RequestException extends RuntimeException {
    public RequestException(Throwable cause) {
        super(cause);
    }

}
