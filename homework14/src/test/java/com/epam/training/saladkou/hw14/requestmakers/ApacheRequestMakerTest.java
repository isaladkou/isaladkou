package com.epam.training.saladkou.hw14.requestmakers;

import com.epam.training.saladkou.hw14.entity.Post;
import com.epam.training.saladkou.hw14.exceptions.ResponseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static java.net.HttpURLConnection.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApacheRequestMakerTest {

    private final int ID = 1;
    private final int TO_SEND_ID = 101;
    private final int USER_ID = 1;
    private final String TITLE = "sunt aut facere repellat provident "
            + "occaecati excepturi optio reprehenderit";
    private final String BODY = "quia et suscipit\n"
            + "suscipit recusandae consequuntur expedita et cum\n"
            + "reprehenderit molestiae ut ut quas totam\n"
            + "nostrum rerum est autem sunt rem eveniet architecto";

    @Mock
    private HttpClient httpClient;

    @Mock
    private HttpResponse httpResponse;

    @Mock
    private StatusLine statusLine;

    @Mock
    private HttpEntity httpEntity;

    @InjectMocks
    private ApacheRequestMaker requestMaker;


    private Post defaultPost;
    private Post postToSend;


    @Before
    public void initialize() {
        defaultPost = new Post(USER_ID, ID, TITLE, BODY);
        postToSend = new Post(TO_SEND_ID);
    }

    @Test
    public void testSendGet() throws IOException {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(HTTP_OK);
        when(httpResponse.getEntity()).thenReturn(httpEntity);
        String jsonPost = new ObjectMapper().writeValueAsString(defaultPost);
        InputStream inputStream = new ByteArrayInputStream(jsonPost.getBytes());
        when(httpEntity.getContent()).thenReturn(inputStream);

        assertEquals(defaultPost, requestMaker.sendGet(ID));

        verify(httpClient).execute(any(HttpGet.class));
        verify(httpResponse).getStatusLine();
        verify(statusLine).getStatusCode();
        verify(httpResponse).getEntity();
        verify(httpEntity).getContent();
    }

    @Test(expected = ResponseException.class)
    public void testSendGetWrongStatusCode() throws IOException {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(HTTP_BAD_REQUEST);

        requestMaker.sendGet(ID);

        verify(httpClient).execute(any(HttpGet.class));
        verify(httpResponse).getStatusLine();
        verify(statusLine).getStatusCode();
    }

    @Test(expected = ResponseException.class)
    public void testSendGetWithClientProtocolException() throws IOException {
        when(httpClient.execute(any(HttpGet.class))).thenThrow(new ClientProtocolException());
        requestMaker.sendGet(ID);
        verify(httpClient).execute(any(HttpGet.class));
    }

    @Test(expected = ResponseException.class)
    public void testSendGetWithIOException() throws IOException {
        when(httpClient.execute(any(HttpGet.class))).thenThrow(new IOException());
        requestMaker.sendGet(ID);
        verify(httpClient).execute(any(HttpGet.class));
    }

    @Test(expected = ResponseException.class)
    public void testSendGetWithExceptionWhileGetContent() throws IOException {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(HTTP_OK);
        when(httpResponse.getEntity()).thenReturn(httpEntity);
        when(httpEntity.getContent()).thenThrow(new IOException());

        assertEquals(defaultPost, requestMaker.sendGet(ID));

        verify(httpClient).execute(any(HttpGet.class));
        verify(httpResponse).getStatusLine();
        verify(statusLine).getStatusCode();
        verify(httpResponse).getEntity();
        verify(httpEntity).getContent();
    }


    @Test
    public void testSendPost() throws IOException {
        when(httpClient.execute(any(HttpPost.class))).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(HTTP_CREATED);
        when(httpResponse.getEntity()).thenReturn(httpEntity);
        String jsonPostToSend = new ObjectMapper().writeValueAsString(postToSend);
        InputStream inputStream = new ByteArrayInputStream(jsonPostToSend.getBytes());
        when(httpEntity.getContent()).thenReturn(inputStream);

        assertEquals(postToSend, requestMaker.sendPost(postToSend));

        verify(httpClient).execute(any(HttpGet.class));
        verify(httpResponse).getStatusLine();
        verify(statusLine).getStatusCode();
        verify(httpResponse).getEntity();
        verify(httpEntity).getContent();
    }

    @Test(expected = ResponseException.class)
    public void testSendPostWithClientProtocolException() throws IOException {
        when(httpClient.execute(any(HttpPost.class))).thenThrow(new ClientProtocolException());
        requestMaker.sendPost(postToSend);
        verify(httpClient).execute(any(HttpGet.class));
    }

    @Test(expected = ResponseException.class)
    public void testSendPostWithIOException() throws IOException {
        when(httpClient.execute(any(HttpPost.class))).thenThrow(new IOException());
        requestMaker.sendPost(postToSend);
        verify(httpClient).execute(any(HttpGet.class));
    }
}