package com.epam.training.saladkou.hw14.utils;

import com.epam.training.saladkou.hw14.utils.InputValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class InputValidatorTest {

    private InputValidator inputValidator;
    private Map<String, String> validParametersMap;
    private Map<String, String> nullParametersMap;
    private Map<String, String> wrongAmountParametersMap;
    private Map<String, String> wrongSolutionParametersMap;
    private Map<String, String> wrongMethodParametersMap;
    private Map<String, String> wrongIdParametersMap;


    @Before
    public void initialize() {
        inputValidator = new InputValidator();
        validParametersMap = new HashMap<>();
        validParametersMap.put("id", "1");
        validParametersMap.put("method", "GET");
        validParametersMap.put("solution", "1");

        nullParametersMap = new HashMap<>(validParametersMap);
        nullParametersMap.put("id", null);

        wrongAmountParametersMap = new HashMap<>(validParametersMap);
        wrongAmountParametersMap.put("someKey", "someValue");

        wrongSolutionParametersMap = new HashMap<>(validParametersMap);
        wrongSolutionParametersMap.put("solution", "-2");

        wrongMethodParametersMap = new HashMap<>(validParametersMap);
        wrongMethodParametersMap.put("method", "PUT");

        wrongIdParametersMap = new HashMap<>(validParametersMap);
        wrongIdParametersMap.put("id", "-2");
    }

    @Test
    public void validate() {
        Assert.assertTrue(inputValidator.validate(validParametersMap));
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateWrongParametersAmount() {
        inputValidator.validate(wrongAmountParametersMap);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateNullParameter() {
        inputValidator.validate(nullParametersMap);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateWrongSolutionParameter() {
        inputValidator.validate(wrongSolutionParametersMap);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateWrongMethodParameter() {
        inputValidator.validate(wrongMethodParametersMap);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateWrongIdParameter() {
        inputValidator.validate(wrongIdParametersMap);
    }
}