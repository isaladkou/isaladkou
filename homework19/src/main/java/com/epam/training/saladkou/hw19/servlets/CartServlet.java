package com.epam.training.saladkou.hw19.servlets;

import com.epam.training.saladkou.hw19.config.AppConfig;
import com.epam.training.saladkou.hw19.entity.Good;
import com.epam.training.saladkou.hw19.repository.GoodRepository;
import com.epam.training.saladkou.hw19.utils.impl.RequestResenderImpl;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;


@WebServlet(urlPatterns = "/cart")
public class CartServlet extends HttpServlet {

    private final Logger logger = Logger.getLogger(CartServlet.class);

    private static final String GOODS_ATTRIBUTE = "goods";
    private static final String CART_PAGE_URL = "cart.jsp";

    private static final ApplicationContext CONTEXT =
            new AnnotationConfigApplicationContext(AppConfig.class);


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        logger.info("in CartServlet doPost method");
        GoodRepository goodRepository = CONTEXT.getBean(GoodRepository.class);

        List<Good> goods = goodRepository.getAll();
        HttpSession session = request.getSession();
        session.setAttribute(GOODS_ATTRIBUTE, goods);
        logger.info("resending request to cart.jsp");

        RequestResenderImpl requestResender = CONTEXT.getBean(RequestResenderImpl.class);
        requestResender.forwardRequest(CART_PAGE_URL, request, response);

    }
}
