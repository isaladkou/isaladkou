package com.epam.training.saladkou.hw19.utils;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public interface RequestResender {
    void forwardRequest(String url, ServletRequest request,
                        ServletResponse response);

    void putToFilterChain(ServletRequest request, ServletResponse response,
                          FilterChain filterChain);
}
