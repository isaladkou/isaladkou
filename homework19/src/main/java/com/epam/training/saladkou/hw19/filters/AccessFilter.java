package com.epam.training.saladkou.hw19.filters;

import com.epam.training.saladkou.hw19.config.AppConfig;
import com.epam.training.saladkou.hw19.entity.User;
import com.epam.training.saladkou.hw19.repository.UserRepository;
import com.epam.training.saladkou.hw19.utils.impl.RequestResenderImpl;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class AccessFilter implements Filter {

    private final Logger logger = Logger.getLogger(AccessFilter.class);
    private static final String ACCESS_PARAM = "termsCheckbox";
    private static final String USERNAME_PARAM = "name";
    private static final String ACCESS_ATTRIBUTE = "isAccessAllowed";
    private static final String ERROR_PAGE_URL = "error.jsp";
    private static final String REFERER_HEADER = "referer";
    private static final String AUTH_PAGE_URL = "auth.jsp";
    private static final String USER_ATTRIBUTE = "user";

    private static final ApplicationContext CONTEXT =
            new AnnotationConfigApplicationContext(AppConfig.class);


    private void handleRequestFromAuthPage(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (!session.isNew()) {
            session.invalidate();
            session = request.getSession(true);
        }
        String accessParam = request.getParameter(ACCESS_PARAM);
        String nameParam = request.getParameter(USERNAME_PARAM);

        UserRepository userRepository = CONTEXT.getBean(UserRepository.class);

        User user = userRepository.getUserByLogin(nameParam);
        boolean isAccessAllowed = (accessParam != null);
        session.setAttribute(ACCESS_ATTRIBUTE, isAccessAllowed);
        session.setAttribute(USER_ATTRIBUTE, user);
    }


    private void sendRedirectToErrorPage(HttpServletResponse response) {
        try {
            response.sendRedirect(ERROR_PAGE_URL);
        } catch (IOException exception) {
            logger.warn(exception.getMessage());
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String referer = request.getHeader(REFERER_HEADER);
        logger.info(REFERER_HEADER + ": " + referer);
        if (referer != null && referer.endsWith(AUTH_PAGE_URL)) {
            handleRequestFromAuthPage(request);
        }
        Boolean isAccessAllowed = (Boolean) request.getSession().getAttribute(ACCESS_ATTRIBUTE);
        logger.info(ACCESS_ATTRIBUTE + ": " + isAccessAllowed);
        if (isAccessAllowed == null || !isAccessAllowed) {
            sendRedirectToErrorPage(response);
            return;
        }
        RequestResenderImpl requestResender = CONTEXT.getBean(RequestResenderImpl.class);
        requestResender.putToFilterChain(request, response, filterChain);
    }

    @Override
    public void destroy() {

    }

}
