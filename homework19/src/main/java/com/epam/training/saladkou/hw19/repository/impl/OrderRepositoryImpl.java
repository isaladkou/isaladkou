package com.epam.training.saladkou.hw19.repository.impl;

import com.epam.training.saladkou.hw19.entity.Order;
import com.epam.training.saladkou.hw19.mappers.Mapper;
import com.epam.training.saladkou.hw19.repository.OrderGoodRepository;
import com.epam.training.saladkou.hw19.repository.OrderRepository;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderRepositoryImpl implements OrderRepository {

    private static final String GET_ORDERS_BY_USER_ID_SQL_STATEMENT =
            "SELECT \"ORDER\".ID AS \"ORDER_ID\",\n"
                    + "       GOOD_ID    AS \"GOOD_ID\",\n"
                    + "       GOOD.title,\n"
                    + "       GOOD.PRICE,\n"
                    + "       ORDER_GOOD.amount\n"
                    + "FROM \"ORDER\",\n"
                    + "     ORDER_GOOD,\n"
                    + "     GOOD\n"
                    + "WHERE \"ORDER\".ID IN (\n"
                    + "    SELECT id FROM \"ORDER\" WHERE USER_ID = ?\n"
                    + "    )\n"
                    + "  AND ORDER_GOOD.ORDER_ID = \"ORDER\".ID\n"
                    + "  AND GOOD.ID = ORDER_GOOD.GOOD_ID\n"
                    + "ORDER BY \"ORDER\".ID;";


    private static final String GET_ALL_ORDERS_ID_SQL_STATEMENT =
            "SELECT \"ORDER\".ID FROM \"ORDER\"";

    private static final String DATABASE_URL =
            "jdbc:h2:mem:shop_database;INIT=RUNSCRIPT FROM "
                    + "'classpath:sql/init.sql';DB_CLOSE_DELAY=-1";

    private static final String ADD_ORDER_SQL_STATEMENT =
            "INSERT INTO \"ORDER\"(USER_ID, TOTAL_PRICE) VALUES (?,?)";

    private static final String ID_COLUMN = "ID";

    private Logger logger = Logger.getLogger(OrderRepositoryImpl.class);


    public OrderRepositoryImpl(Mapper<Order> orderMapper, OrderGoodRepository orderGoodRepository) {
        this.orderMapper = orderMapper;
        this.orderGoodRepository = orderGoodRepository;
    }


    private Mapper<Order> orderMapper;
    private OrderGoodRepository orderGoodRepository;


    public List<Order> getOrdersByUserId(int userId) {
        List<Order> orders = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement =
                     connection.prepareStatement(GET_ORDERS_BY_USER_ID_SQL_STATEMENT)) {

            preparedStatement.setInt(1, userId);
            logger.info("getting user " + userId + " orders...");

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                orders = orderMapper.mapResultSetToEntityList(resultSet);
                logger.info("user " + userId + " orders :" + orders);
                return orders;
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return orders;
    }

    public boolean addOrderToUser(Order order, int userId) {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement =
                     connection.prepareStatement(ADD_ORDER_SQL_STATEMENT)) {

            preparedStatement.setInt(1, userId);
            preparedStatement.setDouble(2, order.getTotalSum());

            logger.info("adding order to user " + userId);

            int rowsAdded = preparedStatement.executeUpdate();

            orderGoodRepository.insertItems(order.getItems(), order.getId());
            return rowsAdded != 0;

        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return false;
    }


    public int getLastOrderId() {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement statement =
                     connection.prepareStatement(GET_ALL_ORDERS_ID_SQL_STATEMENT);
             ResultSet resultSet = statement.executeQuery()) {
            int lastId = 0;
            while (resultSet.next()) {
                lastId = resultSet.getInt(ID_COLUMN);
            }
            logger.info("LAST ORDER ID: " + lastId);
            return lastId;
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return 0;
    }

}
