package com.epam.training.saladkou.hw19.mappers;

import java.sql.ResultSet;
import java.util.List;

public interface Mapper<T> {

    String NULL_MESSAGE = "Resultset can't be null";

    T mapResultSetToEntity(ResultSet resultSet) ;
    List<T> mapResultSetToEntityList(ResultSet resultSet);
}
