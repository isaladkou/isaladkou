package com.epam.training.saladkou.hw19.repository.impl;

import com.epam.training.saladkou.hw19.entity.Good;
import com.epam.training.saladkou.hw19.mappers.Mapper;
import com.epam.training.saladkou.hw19.repository.GoodRepository;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GoodRepositoryImpl implements GoodRepository {

    private static final String DATABASE_URL =
            "jdbc:h2:mem:shop_database;INIT=RUNSCRIPT FROM "
                    + "'classpath:sql/init.sql';DB_CLOSE_DELAY=-1";

    private static final String GET_GOOD_BY_ID_SQL_STATEMENT =
            "SELECT id, title, price FROM GOOD WHERE id=?";

    private static final String GET_ALL_GOODS_SQL_STATEMENT =
            "SELECT * FROM GOOD";

    private Logger logger = Logger.getLogger(GoodRepositoryImpl.class);

    public GoodRepositoryImpl(Mapper<Good> mapper) {
        this.goodMapper = mapper;
    }

    private Mapper<Good> goodMapper;


    public Good getGoodById(int id) {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement statement =
                     connection.prepareStatement(GET_GOOD_BY_ID_SQL_STATEMENT)) {

            statement.setInt(1, id);
            logger.info("getting good by id =" + id);
            try (ResultSet resultSet = statement.executeQuery()) {
                Good good = goodMapper.mapResultSetToEntity(resultSet);
                logger.info("GOOD :" + good);
                return good;
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return null;
    }

    public List<Good> getAll() {
        List<Good> goods = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement statement =
                     connection.prepareStatement(GET_ALL_GOODS_SQL_STATEMENT);
             ResultSet resultSet = statement.executeQuery()) {
            logger.info("getting all goods...");
            goods = goodMapper.mapResultSetToEntityList(resultSet);
            logger.info("Goods:" + goods);
            return goods;
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return goods;
    }
}
