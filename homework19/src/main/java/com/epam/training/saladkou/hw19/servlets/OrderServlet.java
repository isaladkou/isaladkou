package com.epam.training.saladkou.hw19.servlets;

import com.epam.training.saladkou.hw19.config.AppConfig;
import com.epam.training.saladkou.hw19.entity.Order;
import com.epam.training.saladkou.hw19.entity.User;
import com.epam.training.saladkou.hw19.repository.OrderRepository;
import com.epam.training.saladkou.hw19.utils.impl.RequestResenderImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "Order", urlPatterns = "/order")
public class OrderServlet extends HttpServlet {

    private static final String ORDER_PAGE_URL = "order.jsp";
    private static final String ORDER_ATTRIBUTE = "order";
    private static final String USER_ATTRIBUTE = "user";

    private static final ApplicationContext CONTEXT =
            new AnnotationConfigApplicationContext(AppConfig.class);


    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        Order order = (Order) session.getAttribute(ORDER_ATTRIBUTE);
        User user = (User) session.getAttribute(USER_ATTRIBUTE);

        OrderRepository orderRepository = CONTEXT.getBean(OrderRepository.class);
        orderRepository.addOrderToUser(order, user.getId());

        RequestResenderImpl requestResender = CONTEXT.getBean(RequestResenderImpl.class);
        requestResender.forwardRequest(ORDER_PAGE_URL, request, response);
    }
}
