package com.epam.training.saladkou.hw19.servlets;

import com.epam.training.saladkou.hw19.config.AppConfig;
import com.epam.training.saladkou.hw19.entity.Order;
import com.epam.training.saladkou.hw19.entity.User;
import com.epam.training.saladkou.hw19.repository.OrderRepository;
import com.epam.training.saladkou.hw19.utils.impl.RequestResenderImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@WebServlet(urlPatterns = "/history")
public class HistoryServlet extends HttpServlet {

    private static final String HISTORY_ATTRIBUTE = "history";
    private static final String USER_ATTRIBUTE = "user";
    private static final String HISTORY_PAGE_URL = "order_history.jsp";
    private static final ApplicationContext CONTEXT =
            new AnnotationConfigApplicationContext(AppConfig.class);


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        OrderRepository orderRepository = CONTEXT.getBean(OrderRepository.class);
        User user = (User) session.getAttribute(USER_ATTRIBUTE);
        List<Order> orders = orderRepository.getOrdersByUserId(user.getId());
        session.setAttribute(HISTORY_ATTRIBUTE, orders);
        RequestResenderImpl requestResender = CONTEXT.getBean(RequestResenderImpl.class);
        requestResender.forwardRequest(HISTORY_PAGE_URL, request, response);
    }
}
