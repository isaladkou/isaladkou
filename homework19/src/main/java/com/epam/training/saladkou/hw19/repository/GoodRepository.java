package com.epam.training.saladkou.hw19.repository;

import com.epam.training.saladkou.hw19.entity.Good;

import java.util.List;

public interface GoodRepository {
    List<Good> getAll();

    Good getGoodById(int id);
}
