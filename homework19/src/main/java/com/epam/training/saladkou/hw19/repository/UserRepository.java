package com.epam.training.saladkou.hw19.repository;

import com.epam.training.saladkou.hw19.entity.User;

public interface UserRepository {
    User getUserByLogin(String login);

    boolean addUser(User user);
}
