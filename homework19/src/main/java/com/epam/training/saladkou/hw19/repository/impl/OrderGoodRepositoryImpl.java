package com.epam.training.saladkou.hw19.repository.impl;

import com.epam.training.saladkou.hw19.entity.Good;
import com.epam.training.saladkou.hw19.repository.OrderGoodRepository;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

public class OrderGoodRepositoryImpl implements OrderGoodRepository {
    private static final String DATABASE_URL =
            "jdbc:h2:mem:shop_database;INIT=RUNSCRIPT FROM "
                    + "'classpath:sql/init.sql';DB_CLOSE_DELAY=-1";

    private static final String SQL_STATEMENT =
            "INSERT INTO ORDER_GOOD (ORDER_ID,GOOD_ID, AMOUNT ) VALUES(?,?,?) ";

    private Logger logger = Logger.getLogger(OrderGoodRepositoryImpl.class);


    public int insertItems(Map<Good, Integer> orderMap, int orderId) {
        int counter = 0;
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement statement = connection.prepareStatement(SQL_STATEMENT)) {
            logger.info("inserting rows to ORDER_GODD table");
            for (Good good : orderMap.keySet()) {
                statement.setInt(1, orderId);
                statement.setInt(2, good.getId());
                statement.setInt(3, orderMap.get(good));
                counter += statement.executeUpdate();
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        logger.info("ITEMS ADDED: " + counter);
        return counter;
    }


}
