package com.epam.training.saladkou.hw19.repository;

import com.epam.training.saladkou.hw19.entity.Order;

import java.util.List;

public interface OrderRepository {
    List<Order> getOrdersByUserId(int userId);

    boolean addOrderToUser(Order order, int userId);

    int getLastOrderId();
}
