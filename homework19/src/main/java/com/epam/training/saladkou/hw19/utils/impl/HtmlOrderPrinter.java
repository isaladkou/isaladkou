package com.epam.training.saladkou.hw19.utils.impl;

import com.epam.training.saladkou.hw19.entity.Good;
import com.epam.training.saladkou.hw19.entity.Order;
import com.epam.training.saladkou.hw19.utils.OrderPrinter;

public class HtmlOrderPrinter implements OrderPrinter {
    public String wrapOrderToTable(Order order) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("<table border=\"1\">");
        stringBuilder.append("<caption>Order #").append(order.getId()).append("</caption>");
        stringBuilder.append("<tr><td>Good</td><td>Price</td><td>Amount</td><td>Sum</td></tr>");
        for (Good good : order.getItems().keySet()) {
            int amount = order.getItems().get(good);
            stringBuilder.append("<tr><td>").append(good.getTitle()).append("</td><td>")
                    .append(good.getPrice()).append("</td><td>")
                    .append(amount).append("</td><td>")
                    .append(good.getPrice() * amount).append("</td></tr>");
        }
        stringBuilder.append("<tr><td>Total sum:</td><td></td><td></td><td>")
                .append(order.getTotalSum()).append("</td></tr>");
        stringBuilder.append("<br></table>");
        return stringBuilder.toString();
    }
}
