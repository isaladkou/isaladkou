package com.epam.training.saladkou.hw19.entity;

import java.util.HashMap;
import java.util.Map;

public class Order {

    public Order(int id) {
        this.id = id;
        items = new HashMap<>();
    }

    public Order() {
        this(0);
    }

    public int getId() {
        return id;
    }

    public Map<Good, Integer> getItems() {
        return items;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setItems(Map<Good, Integer> items) {
        this.items = items;
    }

    public boolean addItem(Good good, int amount) {
        items.put(good, amount);
        return true;
    }

    public boolean addItem(Good item) {
        Good good = getProductFromOrderById(item.getId());
        if (good == null) {
            items.put(item, 1);
        } else {
            int amount = items.get(good);
            items.put(good, ++amount);
        }
        return good == null;
    }

    private Good getProductFromOrderById(Integer goodId) {
        for (Good good : items.keySet()) {
            if (good.getId() == goodId) {
                return good;
            }
        }
        return null;
    }


    private Map<Good, Integer> items;
    private int id;


    public double getTotalSum() {
        double sum = 0;
        for (Good good : items.keySet()) {
            int amount = items.get(good);
            sum += good.getPrice() * amount;
        }
        return sum;
    }

    @Override
    public String toString() {
        return "Order{"
                + "id=" + id
                + "items=" + items + '}';
    }
}
