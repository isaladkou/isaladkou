package com.epam.training.saladkou.hw19.filters;


import com.epam.training.saladkou.hw19.config.AppConfig;
import com.epam.training.saladkou.hw19.entity.Good;
import com.epam.training.saladkou.hw19.entity.Order;
import com.epam.training.saladkou.hw19.repository.GoodRepository;
import com.epam.training.saladkou.hw19.repository.OrderRepository;
import com.epam.training.saladkou.hw19.utils.impl.RequestResenderImpl;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class OrderFilter implements Filter {

    private static final String CART_PAGE_URL = "/cart";
    private static final String ORDER_ATTRIBUTE = "order";

    private static final String ACTION_PARAMETER = "act";
    private static final String ORDERED_ITEM_PARAMETER = "orderedItem";
    private static final String ADD_ITEM_ACTION = "Add Item";
    private static final String REFERER_HEADER = "referer";
    private static final String ORDER_PAGE_URL = "order";

    private final Logger logger = Logger.getLogger(OrderFilter.class);
    private static final ApplicationContext CONTEXT =
            new AnnotationConfigApplicationContext(AppConfig.class);


    private boolean checkIsAddButtonClicked(HttpServletRequest request) {
        return request.getParameter(ACTION_PARAMETER).equals(ADD_ITEM_ACTION);
    }

    private Order getOrder(HttpSession session) {
        if (session.getAttribute(ORDER_ATTRIBUTE) == null) {

            OrderRepository orderRepository = CONTEXT.getBean(OrderRepository.class);
            int lastOrderId = orderRepository.getLastOrderId();
            session.setAttribute(ORDER_ATTRIBUTE, new Order(lastOrderId + 1));
        }
        return (Order) session.getAttribute(ORDER_ATTRIBUTE);
    }

    private boolean refreshOrder(HttpServletRequest request) {
        HttpSession session = request.getSession();
        int itemId = Integer.parseInt(request.getParameter(ORDERED_ITEM_PARAMETER));

        GoodRepository goodRepository = CONTEXT.getBean(GoodRepository.class);

        Good item = goodRepository.getGoodById(itemId);
        Order order = getOrder(session);
        return order.addItem(item);
    }


    private void handleRequest(HttpServletRequest request,
                               ServletResponse servletResponse, FilterChain filterChain) {
        RequestResenderImpl requestResender = CONTEXT.getBean(RequestResenderImpl.class);
        if (checkIsAddButtonClicked(request)) {
            if (request.getParameter(ORDERED_ITEM_PARAMETER) != null) {
                refreshOrder(request);
            }
            requestResender.putToFilterChain(request, servletResponse, filterChain);
        } else {
            requestResender.forwardRequest(ORDER_PAGE_URL, request, servletResponse);
        }
    }


    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) {
        logger.info("in OrderFilter");

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (request.getHeader(REFERER_HEADER).endsWith(CART_PAGE_URL)) {
            logger.info(REFERER_HEADER + " :" + request.getHeader(REFERER_HEADER));
            handleRequest(request, servletResponse, filterChain);
            return;
        }
        RequestResenderImpl requestResender = CONTEXT.getBean(RequestResenderImpl.class);
        requestResender.putToFilterChain(request, servletResponse, filterChain);
    }


    @Override
    public void destroy() {

    }
}
