package com.epam.training.saladkou.hw19.utils;

public interface NullChecker {
    boolean checkIsArgumentNull(Object argument);

    boolean checkIsArgumentNull(Object argument, String message);

}
