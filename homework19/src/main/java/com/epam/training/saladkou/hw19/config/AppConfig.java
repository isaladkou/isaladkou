package com.epam.training.saladkou.hw19.config;


import com.epam.training.saladkou.hw19.entity.Good;
import com.epam.training.saladkou.hw19.entity.Order;
import com.epam.training.saladkou.hw19.entity.User;
import com.epam.training.saladkou.hw19.mappers.Mapper;
import com.epam.training.saladkou.hw19.mappers.impl.GoodMapper;
import com.epam.training.saladkou.hw19.mappers.impl.OrderMapper;
import com.epam.training.saladkou.hw19.mappers.impl.UserMapper;
import com.epam.training.saladkou.hw19.repository.GoodRepository;
import com.epam.training.saladkou.hw19.repository.OrderGoodRepository;
import com.epam.training.saladkou.hw19.repository.OrderRepository;
import com.epam.training.saladkou.hw19.repository.UserRepository;
import com.epam.training.saladkou.hw19.repository.impl.GoodRepositoryImpl;
import com.epam.training.saladkou.hw19.repository.impl.OrderGoodRepositoryImpl;
import com.epam.training.saladkou.hw19.repository.impl.OrderRepositoryImpl;
import com.epam.training.saladkou.hw19.repository.impl.UserRepositoryImpl;
import com.epam.training.saladkou.hw19.utils.NullChecker;
import com.epam.training.saladkou.hw19.utils.OrderPrinter;
import com.epam.training.saladkou.hw19.utils.RequestResender;
import com.epam.training.saladkou.hw19.utils.impl.HtmlOrderPrinter;
import com.epam.training.saladkou.hw19.utils.impl.NullCheckerImpl;
import com.epam.training.saladkou.hw19.utils.impl.RequestResenderImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public OrderRepository getOrderRepository() {
        return new OrderRepositoryImpl(getOrderMapper(), getOrderGoodRepository());
    }

    @Bean
    public GoodRepository getGoodRepository() {
        return new GoodRepositoryImpl(getGoodMapper());
    }

    @Bean
    public OrderGoodRepository getOrderGoodRepository() {
        return new OrderGoodRepositoryImpl();
    }

    @Bean
    public UserRepository getUserRepository() {
        return new UserRepositoryImpl(getUserMapper());
    }

    @Bean
    public OrderPrinter getOrderPrinter() {
        return new HtmlOrderPrinter();
    }

    @Bean
    public NullChecker getNullChecker() {
        return new NullCheckerImpl();
    }

    @Bean
    public RequestResender getRequestResender() {
        return new RequestResenderImpl();
    }

    @Bean
    public Mapper<Good> getGoodMapper() {
        return new GoodMapper(getNullChecker());
    }

    @Bean
    public Mapper<Order> getOrderMapper() {
        return new OrderMapper(getNullChecker());
    }

    @Bean
    public Mapper<User> getUserMapper() {
        return new UserMapper(getNullChecker());
    }

}
