package com.epam.training.saladkou.hw19.utils;

import com.epam.training.saladkou.hw19.entity.Order;

public interface OrderPrinter {
    String wrapOrderToTable(Order order);
}
