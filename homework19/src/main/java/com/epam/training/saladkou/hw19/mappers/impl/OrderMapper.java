package com.epam.training.saladkou.hw19.mappers.impl;

import com.epam.training.saladkou.hw19.entity.Good;
import com.epam.training.saladkou.hw19.entity.Order;
import com.epam.training.saladkou.hw19.mappers.Mapper;
import com.epam.training.saladkou.hw19.utils.NullChecker;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderMapper implements Mapper<Order> {

    private static final String ORDER_ID_COLUMN = "ORDER_ID";
    private static final String GOOD_ID_COLUMN = "GOOD_ID";
    private static final String TITLE_COLUMN = "TITLE";
    private static final String PRICE_COLUMN = "PRICE";
    private static final String AMOUNT_COLUMN = "AMOUNT";

    private final NullChecker nullChecker;

    private final Logger logger = Logger.getLogger(OrderMapper.class);

    public OrderMapper(NullChecker nullChecker) {
        this.nullChecker = nullChecker;
    }

    @Override
    public Order mapResultSetToEntity(ResultSet resultSet) {
        nullChecker.checkIsArgumentNull(resultSet);
        Order order = new Order();
        try {
            while (resultSet.next()) {
                order.setId(resultSet.getInt(ORDER_ID_COLUMN));
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return order;
    }

    public List<Order> mapResultSetToEntityList(ResultSet resultSet) {
        List<Order> orders = new ArrayList<>();
        nullChecker.checkIsArgumentNull(resultSet);
        try {
            while (resultSet.next()) {
                int currentId = resultSet.getInt(ORDER_ID_COLUMN);

                Good good = new Good();
                good.setId(resultSet.getInt(GOOD_ID_COLUMN));
                good.setTitle(resultSet.getString(TITLE_COLUMN));
                good.setPrice(resultSet.getDouble(PRICE_COLUMN));

                Order order = getOrder(orders, currentId);
                order.addItem(good, resultSet.getInt(AMOUNT_COLUMN));
                orders.add(order);
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return orders;
    }

    private Order getOrder(List<Order> orders, int id) {
        for (Order order : orders) {
            if (order.getId() == id) {
                orders.remove(order);
                return order;
            }
        }
        return new Order(id);
    }


}
