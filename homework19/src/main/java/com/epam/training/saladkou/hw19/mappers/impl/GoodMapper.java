package com.epam.training.saladkou.hw19.mappers.impl;

import com.epam.training.saladkou.hw19.entity.Good;
import com.epam.training.saladkou.hw19.mappers.Mapper;
import com.epam.training.saladkou.hw19.utils.NullChecker;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GoodMapper implements Mapper<Good> {

    private static final String NULL_MESSAGE = "Resultset can't be null";
    private static final String ID_COLUMN = "ID";
    private static final String TITLE_COLUMN = "TITLE";
    private static final String PRICE_COLUMN = "PRICE";

    private final Logger logger = Logger.getLogger(GoodMapper.class);

    private final NullChecker nullChecker;

    public GoodMapper(NullChecker nullChecker) {
        this.nullChecker = nullChecker;
    }

    public Good mapResultSetToEntity(ResultSet resultSet) {
        nullChecker.checkIsArgumentNull(resultSet, NULL_MESSAGE);
        Good good = new Good();
        try {
            while (resultSet.next()) {
                good.setId(resultSet.getInt(ID_COLUMN));
                good.setTitle(resultSet.getString(TITLE_COLUMN));
                good.setPrice(resultSet.getDouble(PRICE_COLUMN));
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return good;
    }


    public List<Good> mapResultSetToEntityList(ResultSet resultSet) {
        nullChecker.checkIsArgumentNull(resultSet, NULL_MESSAGE);
        List<Good> goods = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Good good = new Good();
                good.setId(resultSet.getInt(ID_COLUMN));
                good.setTitle(resultSet.getString(TITLE_COLUMN));
                good.setPrice(resultSet.getDouble(PRICE_COLUMN));
                goods.add(good);
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return goods;
    }
}
