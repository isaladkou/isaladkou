package com.epam.training.saladkou.hw19.mappers.impl;

import com.epam.training.saladkou.hw19.entity.User;
import com.epam.training.saladkou.hw19.mappers.Mapper;
import com.epam.training.saladkou.hw19.utils.NullChecker;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserMapper implements Mapper<User> {

    private static final String ID_COLUMN = "ID";
    private static final String LOGIN_COLUMN = "LOGIN";
    private static final String PASSWORD_COLUMN = "PASSWORD";

    private final Logger logger = Logger.getLogger(UserMapper.class);

    private NullChecker nullChecker;

    public UserMapper(NullChecker nullChecker) {
        this.nullChecker = nullChecker;
    }

    @Override
    public User mapResultSetToEntity(ResultSet resultSet) {
        nullChecker.checkIsArgumentNull(resultSet, NULL_MESSAGE);
        User user = new User();
        try {
            while (resultSet.next()) {
                user.setId(resultSet.getInt(ID_COLUMN));
                user.setLogin(resultSet.getString(LOGIN_COLUMN));
                user.setPassword(resultSet.getString(PASSWORD_COLUMN));
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }

        return user;
    }

    @Override
    public List<User> mapResultSetToEntityList(ResultSet resultSet) {
        nullChecker.checkIsArgumentNull(resultSet, NULL_MESSAGE);
        List<User> users = new ArrayList<>();
        try {
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt(ID_COLUMN));
                user.setLogin(resultSet.getString(LOGIN_COLUMN));
                user.setPassword(resultSet.getString(PASSWORD_COLUMN));
                users.add(user);
            }
        } catch (SQLException exception) {
            logger.warn(exception.getMessage());
        }
        return users;
    }

}

