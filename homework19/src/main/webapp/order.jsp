<%@page import="com.epam.training.saladkou.hw19.entity.Order,
                com.epam.training.saladkou.hw19.entity.User"
%>
<%@ page import="com.epam.training.saladkou.hw19.config.AppConfig" %>
<%@ page import="org.springframework.context.annotation.AnnotationConfigApplicationContext" %>
<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page import="com.epam.training.saladkou.hw19.utils.OrderPrinter" %>
<html>
<head>
    <title>Order page</title>
</head>
<body>

<p>Dear,&nbsp<%
    User user = (User) session.getAttribute("user");
    out.println(user.getLogin());
%>
    your order is</p>
<%
    Order order = (Order)session.getAttribute("order");
    ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
    OrderPrinter printer = context.getBean(OrderPrinter.class);
    out.println(printer.wrapOrderToTable(order));
%>
</body>
</html>