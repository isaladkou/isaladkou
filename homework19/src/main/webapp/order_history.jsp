<%@ page import="com.epam.training.saladkou.hw19.entity.Order" %>
<%@ page import="java.util.List" %>
<%@ page import="org.springframework.context.annotation.AnnotationConfigApplicationContext" %>
<%@ page import="com.epam.training.saladkou.hw19.config.AppConfig" %>
<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page import="com.epam.training.saladkou.hw19.utils.OrderPrinter" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order history</title>
</head>
<body>
<h3>Here is your order history</h3>
<%
    List<Order> history = (List<Order>) session.getAttribute("history");
    ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
    OrderPrinter printer = context.getBean(OrderPrinter.class);
    for (Order order : history) {
        out.println(printer.wrapOrderToTable(order));
    }
%>

</body>
</html>
